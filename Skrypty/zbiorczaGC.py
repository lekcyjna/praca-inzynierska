import readSequenceFunctions as rsf
import argparse
import matplotlib.pyplot as plt
import numpy as np

rsf.usedCoding=rsf.Coding()

parser=argparse.ArgumentParser()
parser.add_argument("fastaFile",
        help="Path to fasta file in which there are sequences whose histogram should be plotted.")
parser.add_argument("przynaleznosc",
        help="Path to file with result of grouping.")
parser.add_argument("groupNr", type=int,
        help="Number of group to check.")
args=parser.parse_args()

seq, _ = rsf.prepareDictionary([args.fastaFile])
d={}
for k in seq:
    if len(seq[k])//3>40:
        d[k]=seq[k]
print(len(seq))
print(len(d))

przynaleznosc=np.loadtxt(args.przynaleznosc)

sekwencjeWGrupie=np.array(list(d.values()))[przynaleznosc==args.groupNr]

gc=[]
for gen in sekwencjeWGrupie:
    gen=gen.lower()
    C=gen.count("c")
    G=gen.count("g")
    gc.append((G+C)/len(gen))


plt.hist(gc, bins=50,rwidth=0.85, range=(0.2, 0.8))
plt.grid(axis="y")
plt.ylabel("Liczba sekwencji")
plt.xlabel("Względna zawartość GC")
#plt.savefig("Obrazy/gcHist.png")
plt.show()

gc=np.array(gc)
print(gc.mean())
print(gc.std())
print("Więcej niż 0,5:", np.sum(gc>=0.5)/len(seq))
