import readSequenceFunctions as rsf
import argparse
import matplotlib.pyplot as plt
import numpy as np

parser=argparse.ArgumentParser()
parser.add_argument("fastaFile",
        help="Path to fasta file in which there are sequences whose histogram should be plotted.")
args=parser.parse_args()

seq={}
with open(args.fastaFile, "r") as file:
    rsf.loadSequencesIntoDict(seq,file)

dl=[]
for key in seq:
    gen : str=seq[key]
    gen=gen.lower()
    C=gen.count("c")
    G=gen.count("g")
    dl.append((G+C)/len(seq[key]))

plt.hist(dl, bins=50,rwidth=0.85, range=(0.2, 0.8))
plt.grid(axis="y")
plt.ylabel("Liczba sekwencji")
plt.xlabel("Względna zawartość GC")
plt.savefig("Obrazy/gcHist.png")
plt.show()

dl=np.array(dl)
print(dl.mean())
print(dl.std())
print("Więcej niż 0,5:", np.sum(dl>=0.5)/len(dl))
