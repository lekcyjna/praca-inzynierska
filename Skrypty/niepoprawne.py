import readSequenceFunctions as rsf
import argparse
import numpy as np

parser=argparse.ArgumentParser()
parser.add_argument("fastaFile",
        help="Path to fasta file in which there are sequences whose histogram should be plotted.")
args=parser.parse_args()

seq={}
with open(args.fastaFile, "r") as file:
    rsf.loadSequencesIntoDict(seq,file)

blNuk=[]
blDl=[]
for key in seq:
    gen : str=seq[key]
    gen=gen.lower()
    C=gen.count("c")
    G=gen.count("g")
    A=gen.count("a")
    T=gen.count("t")
    if A+T+G+C!=len(gen):
        blNuk.append(key)
    if len(gen)%3!=0:
        blDl.append(key)

print(len(blNuk))
print(len(blDl))

