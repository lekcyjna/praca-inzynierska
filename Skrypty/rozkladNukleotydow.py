import readSequenceFunctions as rsf
import argparse
import matplotlib.pyplot as plt
import numpy as np

parser=argparse.ArgumentParser()
parser.add_argument("fastaFile",
        help="Path to fasta file in which there are sequences whose histogram should be plotted.")
args=parser.parse_args()

seq={}
with open(args.fastaFile, "r") as file:
    rsf.loadSequencesIntoDict(seq,file)

przebiegi=[]
keys=list(seq.keys())
np.random.seed(49)
for i in range(24):
    gen : str = seq[keys[np.random.randint(0,len(keys))]]
    gen=np.array(list(gen.lower()))
    przebiegi.append({})
    kumSumCalosci=np.cumsum(np.ones(len(gen)))
    for n in {'a','t','c','g'}:
        x=np.cumsum(gen==n)
        przebiegi[-1][n]=x/kumSumCalosci


plt.gcf().set_size_inches((10,5))
plt.gcf().set_constrained_layout(True)
Wiersze=3
Kolumny=7
for i in range(Wiersze):
    for j in range(Kolumny):
        if i==Wiersze-1 and j==Kolumny-1:
            continue
        plt.subplot(Wiersze,Kolumny,i*Kolumny+j+1)
        for n in {'a','t','c','g'}:
            plt.ylim(0.1, 0.5)
            plt.plot(przebiegi[i*Kolumny+j][n], label=n.upper())

h,l=plt.gca().get_legend_handles_labels()
plt.gcf().legend(h, l, loc=(0.9,0.08))
plt.gcf().supxlabel("Długość sekwencji w nukleotydach")
plt.gcf().supylabel("Frakcja nukleotydów danego typu")

plt.savefig("Obrazy/rozkladNukleotydow.png")
plt.show()
