import readSequenceFunctions as rsf
import argparse
import matplotlib.pyplot as plt
import numpy as np

parser=argparse.ArgumentParser()
parser.add_argument("fastaFile",
        help="Path to fasta file in which there are sequences whose histogram should be plotted.")
args=parser.parse_args()

seq={}
with open(args.fastaFile, "r") as file:
    rsf.loadSequencesIntoDict(seq,file)

dl=[]
for key in seq:
    dl.append(len(seq[key])/3)

plt.hist(dl, bins=50,rwidth=0.85, range=(0,1600))
plt.grid(axis="y")
plt.ylabel("Liczba sekwencji")
plt.xlabel("Długość sekwencji w kodonach")
plt.savefig("Obrazy/dlHist.png")
plt.show()

dl=np.array(dl)
print(dl.mean())
print(dl.std())
