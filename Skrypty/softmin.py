import numpy as np
import matplotlib.pyplot as plt

def softMinDzielenie(X: np.ndarray, alpha: float) -> float:
    E=np.exp(X*alpha)
    licznik=np.sum(X*E)
    mianownik=np.sum(E)
    return licznik/mianownik;


def softMinLogSumExp(X: np.ndarray, alpha: float) -> float:
    E=np.exp(X*alpha)
    suma=np.sum(E)
    return (1/alpha)*np.log(suma);

def przezPierwiastek(X: np.ndarray, alpha:float) -> float:
    a=np.sum(X)
    b=np.sqrt((X[0]-X[1])**2+alpha)
    return (a-b)/2

N=1000
alpha=-1
X=np.linspace(-5,5,N)
xsy=np.array([X, np.zeros(N)]).T
yDziel=np.array([softMinDzielenie(xsy[i,:], alpha) for i in range(N)])
yLSE=np.array([softMinLogSumExp(xsy[i,:], alpha) for i in range(N)])
yPierw=np.array([przezPierwiastek(xsy[i,:], 0.1) for i in range(N)])
ymin=np.array([np.min(xsy[i,:]) for i in range(N)])

plt.gcf().set_size_inches(10,5)
plt.subplot(1,3,1);
plt.ylim(-5,5)
plt.plot(X, yDziel, label="Smooth minimum z użyciem dzielenia")
plt.plot(X, ymin, label="Hard minimum")
plt.legend();
plt.subplot(1,3,2);
plt.ylim(-5,5)
plt.plot(X, yLSE, label="Smooth minimum LSE");
plt.plot(X, ymin, label="Hard minimum")
plt.legend();
plt.subplot(1,3,3);
plt.ylim(-5,5)
plt.plot(X, yPierw, label="Smooth minimum przez pierwiastek");
plt.plot(X, ymin, label="Hard minimum")
plt.legend();
plt.savefig("smoothMin.png")
plt.show();
