/* -O2
real	0m0,272s
user	0m0,174s
sys	0m0,097s
*/
#include "adept.h"
#include "adept_arrays.h"
#include <iostream>
#include <cstdlib>

#define N 10000

int main()
{
  std::srand(14);
  adept::Matrix M (N,N);
  M(1,1)=std::rand();
  double suma=0;
  for(int i=0;i<N;i++)
    for(int j=0;j<N;j++)
      suma+=M(i,j);
  std::cerr<<suma<<"\n";;
}
