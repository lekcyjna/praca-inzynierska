/* -O2
real	0m0,457s
user	0m0,193s
sys	0m0,263s
*/
#include <iostream>
#include <cstdlib>
#include <valarray>

#define N 10000

int main()
{
  std::srand(14);
  std::valarray<double> M(N*N);
  M[1]=std::rand();
  double suma=0;
  for(int i=0;i<N;i++)
    for(int j=0;j<N;j++)
      suma+=M[i*N+j];
  std::cerr<<suma<<"\n";;
}
