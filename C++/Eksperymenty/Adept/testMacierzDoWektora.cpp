#include <adept_arrays.h>
#include <iostream>

int main()
{
  adept::Matrix a{{1,2,3},{6,5,4}};
  adept::Vector v(a.data_pointer(), 6);
  std::cerr<<a<<"\n";
  std::cerr<<v<<"\n";
  a(1,2)=14;
  std::cerr<<a<<"\n";
  std::cerr<<v<<"\n";

  adept::Vector w(6);
  w<<a;
  std::cerr<<w<<"\n";
}

