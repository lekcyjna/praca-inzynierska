#include <adept.h>
#include <adept_arrays.h>
#include <iostream>

int main()
{
  adept::Stack astack;
  adept::aMatrix m{{1,2,3},{6,5,4}};
  astack.new_recording();

  auto x = m*3;
  adept::aVector v=adept::minval(x,1);
  adept::adouble w = adept::norm2(v);
  w.set_gradient(1.0);
  astack.reverse();
  std::cerr<<w<<" "<<w.get_gradient()<<"\n";
  std::cerr<<v.get_gradient()<<"\n";
  std::cerr<<m.get_gradient()<<"\n";
}
