#include <iostream>

template<typename T, int I>
class Klasa
{
  public:
  void wypisz();
};

template<typename T, int i> 
void Klasa<T, i>::wypisz()
{
  if constexpr (i!=0)
  {
  std::cerr<<"Otrzymano inta "<<i<<".\n";
  }
  else
  {
    std::cerr<<"Przekazano zero.\n";
  }
}


int main()
{
//  Klasa<float, 1> k1;
//  k1.wypisz();

  Klasa<float, 0> k2;
  k2.wypisz();
}
