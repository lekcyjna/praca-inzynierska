#pragma once
#include "src/miary/DTWBase.tpp"

template <typename T>
  requires wiezy::Ciag<T>
class DTWEuklides : public DTWBase<T>
{
  double funkcjaOdleglosci(const typename T::value_type&, const typename T::value_type&) const override;
};
