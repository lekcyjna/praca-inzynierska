#pragma once
#include "DTWEuklides.hpp"

template<typename T>
  requires wiezy::Ciag<T>
double DTWEuklides<T>::funkcjaOdleglosci(const typename T::value_type& a, const typename T::value_type& b) const
{
  double x=a-b;
  return x*x;
}
