/// Wymagania dla DTW:
/// - musi zwracać długość najlepszego przyrównania
/// - musi zwracać najlepsze przyrównanie
/// - operuje na dwóch ciągach
/// - ciągi są niemodyfikowane
/// - domyślnie używa odległości euklidesowej
/// - łatwo się rozszerza o zmienę miary odległości
/// - ma możliwość dodawania ograniczeń na macierz przyrównania
/// - powinno być thread-safe
/// - wsparcie dla ciągów wielowymiarowych
//
//
/// Rozwiązanie wymagań:
/// - funkcja odległości przekazywana jako lambda
/// - ograniczenia na macierz przyrównań dodawane poprzez dziedziczenie
/// - w alg dynamicznym wywoływana funkcja nadpisywana, która sprawdza czy dane pole wziąć pod uwagę
/// - implementacja obiektowa
/// - implementacja na szablonach
/// - użyć konceptów

#pragma once
#include <concepts>
#include<functional>
#include<ranges>
#include<vector>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

typedef std::vector<std::vector<int>> Przyrownanie;

namespace wiezy {
template <typename T>
concept Ciag = 
  requires (T x)
{
  typename T::value_type;
  requires std::ranges::range<T>;
  x.size();// -> std::convertible_to<int>;
};

template <typename T, typename F>
concept KlasaZOpakowaniemFunkcji =
requires (T x1, T x2)
{
  F::f(x1,x2);
  requires std::floating_point<decltype(F::f(x1,x2))>;
};

}

template <typename T>
  requires wiezy::Ciag<T>
class DTWBase 
{
  protected:
    /// Sprawdza czy należy wyliczyć pole x,y z macierzy dynamicznej
    /// zwraca true jeśli tak, false wpp. dlugoscX i dlugoscY to
    /// dlugosci macierzy przyrownania wzdłuż odpowiednich osi
    virtual bool sprawdzWiezyMacierzyDynamicznej(int x, int y, int dlugoscX, int dlugoscY) const;

    virtual double funkcjaOdleglosci(const typename T::value_type&, const typename T::value_type&) const =0;

    enum class Kierunek : std::uint8_t
    {
      poczatek = 0,
      zGory,
      zLewej,
      zeSkosu,
      bledny = 255
    };
  public:
    /// Wylicza DTW między dwoma ciągami i zwraca koszt oraz przyrównanie drugiego ciągu do pierwszego
    std::tuple<double,Przyrownanie> operator() (const T&, const T&) const;
    static void setLogger();

  private:
    double INF=1e307;

    std::tuple<double, Kierunek> znajdzMinimum(int i, int j,
        const std::vector<std::vector<double>>& koszty, const std::vector<std::vector<Kierunek>>& droga) const;
    void wypelnijMacierzPrzyrownania(const T&, const T&, std::vector<std::vector<double>>&, std::vector<std::vector<Kierunek>>&) const;
    Przyrownanie wyznaczPrzyrownanie(const std::vector<std::vector<Kierunek>>&) const;
};


