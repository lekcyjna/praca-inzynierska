#pragma once
#include "DTWLambda.hpp"

template <typename T>
  requires wiezy::Ciag<T>
DTWLambda<T>::DTWLambda(std::function<double(const typename T::value_type&, const typename T::value_type&)> f)
  : lambdaOdleglosci{f}
{}


template <typename T>
  requires wiezy::Ciag<T>
double DTWLambda<T>::funkcjaOdleglosci(const typename T::value_type& arg1, const typename T::value_type& arg2) const
{
  return lambdaOdleglosci(arg1, arg2);
}
