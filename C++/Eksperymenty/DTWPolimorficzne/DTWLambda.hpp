#pragma once
#include "src/miary/DTWBase.tpp"

template <typename T>
  requires wiezy::Ciag<T>
class DTWLambda : public DTWBase<T>
{
  virtual double funkcjaOdleglosci(const typename T::value_type&, const typename T::value_type&) const override;
  std::function<double(const typename T::value_type&, const typename T::value_type&)> lambdaOdleglosci;
  public:
    DTWLambda(std::function<double(const typename T::value_type&, const typename T::value_type&)>);
};
