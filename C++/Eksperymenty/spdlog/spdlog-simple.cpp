#define SPDLOG_FMT_EXTERNAL
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <chrono>

#define X 1000000000

int a=2;
int b=4;

int main()
{
  auto t1 = std::chrono::system_clock::now(); 
  spdlog::set_level(spdlog::level::warn);
  for(int i=0;i<X;i++)
  {
    spdlog::info("Zimnokrwisty smok pustynny {} {}",a,b); 
  }
  auto t2 = std::chrono::system_clock::now(); 
  spdlog::warn("Bactrace {}",spdlog::default_logger()->should_backtrace());
  spdlog::warn("Czas: {}", std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count());
}

