#define SPDLOG_FMT_EXTERNAL
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <chrono>
#include <thread>

#define X 1000000000
#define WATKI 4

int a=2;
int b=4;

void loguj()
{
  for(int i=0;i<X/WATKI;i++)
  {
    spdlog::info("Zimnokrwisty smok pustynny {} {}",a,b); 
  }
}

int main()
{
  auto t1 = std::chrono::system_clock::now(); 
  std::vector<std::thread>  wektWatk {WATKI};
  spdlog::set_level(spdlog::level::warn);
  for (std::uint8_t i=0;i<4;i++)
  {
    wektWatk[i]=std::thread(loguj);
  }
  for (std::uint8_t i=0;i<4;i++)
  {
    wektWatk[i].join();
  }
  auto t2 = std::chrono::system_clock::now(); 
  spdlog::warn("Bactrace {}",spdlog::default_logger()->should_backtrace());
  spdlog::warn("Czas: {}", std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count());
}

