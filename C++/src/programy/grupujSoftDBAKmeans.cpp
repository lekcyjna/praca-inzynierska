//TODO dodać możliwość zmiany liczby iteracji do kodu SoftDBAKmeans
//TODO dodać do SoftDTW normalizację przez długość
#include "src/grupowanie/SoftDBAKmeans.tcc"
#include <boost/program_options.hpp>
#include "src/utils/wczytajSzeregi.hpp"
#include "src/miary/ZmienneFunkcjeDTW.tpp"

namespace po = boost::program_options;

template <PSmoothMinType pSmoothMinType>
void uczModel(po::variables_map& vm, std::vector<adept::Matrix>& zbior)
{
  std::mt19937 rnd;
  rnd.seed(vm["seed"].as<int>());

  double gamma=vm["gamma"].as<double>();
  int liczbaGrup=vm["liczba-grup"].as<int>();
  int iter=vm["iter"].as<int>();

  SoftDBAKmeans<miary::EuklidesProstyBezWiezowMacierzy, pSmoothMinType> model (gamma, liczbaGrup,rnd, iter);
  model.ustawTypNormalizacji((typename decltype(model)::TypNormalizacji)vm["normalizacja"].as<int>());
  model.ustawLiczbeReinicjalizacji(vm["reinicjalizacje"].as<uint32_t>());
  model.ustawMinimalnaLiczbeElementowWGrupie(vm["minElem"].as<uint32_t>());
  model.ustawLiczbeIterNaWczesnymEtapie(vm["poczIter"].as<uint32_t>());

  model.fit(zbior);
  auto centroidy = wektorAdeptaDoWektoraWektorow(model.zwrocCentroidy());
  zapiszWektorWektorow(centroidy, vm["out-dir"].as<std::filesystem::path>()/"centroidy.txt");
  auto przynaleznosc = model.predict(zbior);
  std::vector<int> wektorPrzyn;
  for (const auto p : przynaleznosc)
    wektorPrzyn.push_back(p);
  zapiszWektor(wektorPrzyn, vm["out-dir"].as<std::filesystem::path>()/"przynaleznosc.txt");
  std::cerr<<"Koszt: "<<model.zwrocKoszt(zbior)<<"\n";
}

int main(int argc, char** argv)
{
  po::options_description opcje;
  opcje.add_options()
    ("plik-wejsciowy", po::value<std::filesystem::path>()->required(), "Ściezka do pliku z ciagami do zgrupowania.")
    ("liczba-grup,n", po::value<int>()->required(), "Liczba grup do utworzenia.")
    ("seed", po::value<int>()->default_value(14), "Ziarno do generatora liczb losowych.")
    ("iter", po::value<int>()->default_value(50), "Liczba iteracji algorytmu kmenoids.")
    ("gamma", po::value<double>()->default_value(1.0), "Gamma używana w SoftDTW.")
    ("out-dir", po::value<std::filesystem::path>()->required(), "Katalog do zapisu plików wyjściowych.")
    ("normalizacja", po::value<int>()->default_value(0), "Typ normalizacji: 0 - brak, 1 - normalizacja przez dl. centrum.")
    ("reinicjalizacje", po::value<uint32_t>()->default_value(3), "Liczba reinicjalizacji grup, w przypadku zapadania się.")
    ("poczIter", po::value<uint32_t>()->default_value(2), "Liczba iteracji do przeprowadzenia na wstępnym etapie wyboru centroidu.")
    ("minElem", po::value<uint32_t>()->default_value(10), "Minimalna liczba elementów, która powinna być w grupie po zakończeniu wstępnej inicjalizacji centroidów.")
    ("smoothMin", po::value<std::string>()->default_value("LSE"), "Typ smoothMin jaki powinien zostać użyty.")
    ("help", "Wypisuje pomoc.");
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, opcje), vm);
  po::notify(vm);

  std::vector<adept::Matrix> zbior = wektorWektorowDoWektoraAdepta(wczytajSzeregi1D(vm["plik-wejsciowy"].as<std::filesystem::path>()));
  std::filesystem::create_directories(vm["out-dir"].as<std::filesystem::path>());


  PSmoothMinType psMinType;
  if(vm["smoothMin"].as<std::string>() == "LSE")
    psMinType = PSmoothMinType::LSE;
  else if(vm["smoothMin"].as<std::string>() == "ExpDiv")
    psMinType = PSmoothMinType::ExpDiv;
  else
    throw std::runtime_error("Błędny argument smoothMin: " + vm["smoothMin"].as<std::string>());

  if(psMinType == PSmoothMinType::LSE)
  {
    uczModel<PSmoothMinType::LSE>(vm, zbior);
  }
  else if ( psMinType == PSmoothMinType::ExpDiv)
  {
    uczModel<PSmoothMinType::ExpDiv>(vm, zbior);
  }
}

