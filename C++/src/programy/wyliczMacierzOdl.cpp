#include <boost/program_options.hpp>
#include "src/miary/DTW.tpp"
#include "src/miary/SoftDTW.tpp"
#include "src/miary/ZmienneFunkcjeDTW.tpp"
#include "src/miary/macierzOdleglosci.tpp"
#include "src/utils/wczytajSzeregi.hpp"
#include <fenv.h>
//int _feenableexcept_status = feenableexcept(FE_INVALID|FE_DIVBYZERO|FE_OVERFLOW);

namespace po = boost::program_options;

namespace {
  std::vector<adept::Matrix> konwertujDoAdepta(const std::vector<std::vector<double>>& zbior)
  {
    std::vector<adept::Matrix> wyn;
    wyn.reserve(zbior.size());
    for (const auto& ciag : zbior)
    {
      adept::Matrix a(ciag.size(), 1);
      for(uint32_t i=0;i<ciag.size();i++)
        a(i,0)=ciag[i];
      wyn.emplace_back(std::move(a));
    }
    return wyn;
  }
}

int main(int argc, char** argv)
{
  po::options_description opcje;
  opcje.add_options()
    ("plik-wejsciowy", po::value<std::filesystem::path>()->required(), "Ściezka do pliku z ciagami do zgrupowania.")
    ("out-dir", po::value<std::filesystem::path>()->required(), "Katalog do zapisu plików wyjściowych.")
    ("log", po::value<std::string>()->default_value("error"), "Poziom logowania.")
    ("typ-odl", po::value<std::string>()->default_value("euklides"), "Typ odległości jaki ma zostać wyliczony.")
    ("gamma", po::value<double>(), "Parametr gamma dla softdtw.")
    ("help", "Wypisuje pomoc.");
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, opcje), vm);
  po::notify(vm);

  std::string poziomLogowania=vm["log"].as<std::string>();
  if(poziomLogowania=="error")
    spdlog::set_level(spdlog::level::err);
  else if (poziomLogowania=="warn")
    spdlog::set_level(spdlog::level::warn);
  else if (poziomLogowania=="info")
    spdlog::set_level(spdlog::level::info);
  else if (poziomLogowania=="debug")
    spdlog::set_level(spdlog::level::debug);
  else if (poziomLogowania=="trace")
    spdlog::set_level(spdlog::level::trace);
  else
    throw std::runtime_error("Nierozpoznany poziom logowania.");

  std::vector<std::vector<double>> zbior = wczytajSzeregi1D(vm["plik-wejsciowy"].as<std::filesystem::path>());
  std::filesystem::create_directories(vm["out-dir"].as<std::filesystem::path>());

  std::string typOdl=vm["typ-odl"].as<std::string>();
  std::vector<std::vector<double>> macierzOdl;
  if(typOdl=="euklides")
  {
    std::cerr<<"Wyliczanie DTW z użyciem odległości euklidesowej.\n";
    auto funOdl = DTW<std::vector<double>, miary::EuklidesProstyBezWiezowMacierzy<double>> 
      {miary::EuklidesProstyBezWiezowMacierzy<double>()};
    auto fabryka = FabrykaMacierzOdleglosci<decltype(zbior)>
    {[&funOdl](const std::vector<double>& a, const std::vector<double>&b)
      {
        auto [odl, przyrownanie] {funOdl(a,b)};
        return odl;
      }};
    macierzOdl=fabryka(zbior);
  }
  else if (typOdl=="softdtw")
  {
    std::cerr<<"Wyliczanie SoftDTW z użyciem LSE.\n";
    miary::EuklidesProstyBezWiezowMacierzy<adept::Vector> euk{};
    auto funOdl = SoftDTW<adept::Matrix, decltype(euk)> {euk, vm["gamma"].as<double>()};
    auto adeptZbior=konwertujDoAdepta(zbior);
    macierzOdl.reserve(zbior.size());
    for(unsigned int i=0;i<zbior.size();i++)
    {
      std::vector<double> wiersz;
      wiersz.reserve(zbior.size());
      for(unsigned int j=0;j<i;j++)
      {
        wiersz.push_back(macierzOdl[j][i]);
      }
      for(unsigned int j=i;j<zbior.size();j++)
      {
        wiersz.push_back(funOdl(adeptZbior[i], adeptZbior[j]));
      }
      macierzOdl.push_back(wiersz);
    }
  }
  else if (typOdl=="softdtwExpDiv")
  {
    std::cerr<<"Wyliczanie SoftDTW z użyciem ExpDiv.\n";
    miary::EuklidesProstyBezWiezowMacierzy<adept::Vector> euk{};
    auto funOdl = SoftDTW<adept::Matrix, decltype(euk), PSmoothMinType::ExpDiv> {euk, vm["gamma"].as<double>()};
    auto adeptZbior=konwertujDoAdepta(zbior);
    macierzOdl.reserve(zbior.size());
    for(unsigned int i=0;i<zbior.size();i++)
    {
      std::vector<double> wiersz;
      wiersz.reserve(zbior.size());
      for(unsigned int j=0;j<i;j++)
      {
        wiersz.push_back(macierzOdl[j][i]);
      }
      for(unsigned int j=i;j<zbior.size();j++)
      {
        wiersz.push_back(funOdl(adeptZbior[i], adeptZbior[j]));
      }
      macierzOdl.push_back(wiersz);
    }
  }
  else
  {
    std::cerr<<"Wyliczanie DTW z użyciem funkcji lambda.\n";
    auto funOdl = DTW<std::vector<double>, miary::FunkcjaLambdaBezWiezowMacierzy<double>> 
      {miary::FunkcjaLambdaBezWiezowMacierzy<double>{[](const double& a, const double& b) -> double{double x = a-b; return x*x;}}};

    auto fabryka = FabrykaMacierzOdleglosci<decltype(zbior)>
    {[&funOdl](const std::vector<double>& a, const std::vector<double>&b)
      {
        auto [odl, przyrownanie] {funOdl(a,b)};
        return odl;
      }};
    macierzOdl=fabryka(zbior);
  }

  zapiszWektorWektorow(macierzOdl, vm["out-dir"].as<std::filesystem::path>()/"macierzOdl.txt");
  return 0;
}
