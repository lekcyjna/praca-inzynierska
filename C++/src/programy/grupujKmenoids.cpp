//W historii gita powinno dać się odszukać uwagi do pracy na temat efektywności (dziś jest 2021-10-05)
// Przeanalizować uzyskane centroidy - sprawdzić ile rzeczywiście centroidów otrzymaliśmy - TODO
//    - Sprawdzić liczbę elementów należących do poszczególnych centroidów - TODO

#include "src/grupowanie/Kmenoids.tpp"
#include "src/grupowanie/FuzzyKmenoids.tpp"
#include <boost/program_options.hpp>
#include "src/utils/wczytajSzeregi.hpp"
#include "src/miary/ZmienneFunkcjeDTW.tpp"

namespace po = boost::program_options;

int main(int argc, char** argv)
{
  po::options_description opcje;
  opcje.add_options()
    ("plik-wejsciowy", po::value<std::filesystem::path>()->required(), "Ściezka do pliku z ciagami do zgrupowania.")
    ("liczba-grup,n", po::value<int>()->required(), "Liczba grup do utworzenia.")
    ("seed", po::value<int>()->default_value(14), "Ziarno do generatora liczb losowych.")
    ("iter", po::value<int>()->default_value(50), "Liczba iteracji algorytmu kmenoids.")
    ("out-dir", po::value<std::filesystem::path>()->required(), "Katalog do zapisu plików wyjściowych.")
    ("log", po::value<std::string>()->default_value("error"), "Poziom logowania")
    ("zaladuj-macierz", po::value<std::filesystem::path>(), "Scieżka do macierzy odległości do załadowania.")
    ("zapisz-macierz", po::value<bool>()->default_value(false), "Czy należy zapisać wyliczoną macierz odległości.")
    ("fuzzy", po::value<bool>()->default_value(false), "Przeprowadź grupowanie z użyciem FuzzyKmenoids.")
    ("help", "Wypisuje pomoc.");
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, opcje), vm);
  po::notify(vm);

  std::string poziomLogowania=vm["log"].as<std::string>();
  if(poziomLogowania=="error")
    spdlog::set_level(spdlog::level::err);
  else if (poziomLogowania=="warn")
    spdlog::set_level(spdlog::level::warn);
  else if (poziomLogowania=="info")
    spdlog::set_level(spdlog::level::info);
  else if (poziomLogowania=="debug")
    spdlog::set_level(spdlog::level::debug);
  else if (poziomLogowania=="trace")
    spdlog::set_level(spdlog::level::trace);
  else
    throw std::runtime_error("Nierozpoznany poziom logowania.");

  std::vector<std::vector<double>> zbior = wczytajSzeregi1D(vm["plik-wejsciowy"].as<std::filesystem::path>());
  std::filesystem::create_directories(vm["out-dir"].as<std::filesystem::path>());

  DTW<std::vector<double>, miary::EuklidesProstyBezWiezowMacierzy<double>> 
    dtw{miary::EuklidesProstyBezWiezowMacierzy<double>()};

  std::mt19937 rnd;
  rnd.seed(vm["seed"].as<int>());

  std::shared_ptr<Kmenoids<decltype(zbior)>> model;
  //normalizacja przez maksymalną długość faworyzuje przyrównania bardziej wuklidesowe,
  //czyli takie w których przyrównywanych par jest mniej (jest mniej przesunięć)
  auto lambdaDTW = 
        [&dtw](const std::vector<double>& a, const std::vector<double>& b) -> double 
        {
          auto [wynik, przyrownianie] {dtw(a,b)};
          int normalizacja= std::max(a.size(), b.size());
          return wynik/normalizacja;
        };
  if (vm["fuzzy"].as<bool>())
  {
    model = std::make_shared<FuzzyKmenoids<decltype(zbior)>> (
        lambdaDTW, rnd, vm["liczba-grup"].as<int>(), vm["iter"].as<int>());
  }
  else
  {
    model = std::make_shared<Kmenoids<decltype(zbior)>> ( lambdaDTW, 
        rnd, vm["liczba-grup"].as<int>(), vm["iter"].as<int>());
  }

  if (vm.count("zaladuj-macierz") >= 1)
  {
    MacierzOdleglosci macierzOdl = wczytajSzeregi1D(vm["zaladuj-macierz"].as<std::filesystem::path>());
    model->ustawMacierzOdleglosci(macierzOdl);
  }
  model->fit(zbior);
  auto centroidy = model->zwrocCentroidy();
  zapiszWektorWektorow(centroidy, vm["out-dir"].as<std::filesystem::path>()/"centroidy.txt");
  auto przynaleznosc = model->predict(zbior);
  zapiszWektorWektorow(przynaleznosc, vm["out-dir"].as<std::filesystem::path>()/"przynaleznosc.txt");
  if (vm["zapisz-macierz"].as<bool>())
    zapiszWektorWektorow(model->zwrocMacierzOdleglosci(), vm["out-dir"].as<std::filesystem::path>()/"macierzOdl.txt");
  return 0;
}
