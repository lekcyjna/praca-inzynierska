#pragma once
#include <vector>
#include <filesystem>
#include <fstream>
#include <adept_arrays.h>

std::vector<std::vector<double>> wczytajSzeregi1D(const std::filesystem::path& sciezka);

inline std::vector<adept::Matrix> wektorWektorowDoWektoraAdepta(const std::vector<std::vector<double>>& zbior)
{
  std::vector<adept::Matrix> wynik;
  wynik.reserve(zbior.size());
  for (const auto& ciag : zbior)
  {
    adept::Matrix m(ciag.size(),1);
    for(unsigned int i=0;i<ciag.size();i++)
    {
      m(i,0)=ciag[i];
    }
    wynik.push_back(m);
  }
  return wynik;
}

inline std::vector<std::vector<double>> wektorAdeptaDoWektoraWektorow(const std::vector<adept::Matrix>& wektor)
{
  std::vector<std::vector<double>> wynik;
  wynik.reserve(wektor.size());
  for(const auto & m: wektor)
  {
    std::vector<double> ciag;
    ciag.reserve(m.dimension(0));
    for (int i=0;i<m.dimension(0);i++)
      ciag.push_back(m(i,0));
    wynik.push_back(ciag);
  }
  return wynik;
}

template <typename T>
void zapiszWektor(const std::vector<T>& wektor, const std::filesystem::path& sciezka)
{
  std::ofstream plik;
  plik.open(sciezka);

  plik<<wektor<<"\n";

  plik.close();
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
  for (const auto& elem : vec)
  {
    os<<elem<<"\t";
  }
  return os;
}


template <typename T>
  requires std::integral<T> or std::floating_point<T>
void zapiszWektorWektorow(const std::vector<std::vector<T>>& szeregi, const std::filesystem::path& sciezka)
{
  std::ofstream plik;
  plik.open(sciezka);

  for (const auto& wektor : szeregi)
  {
    plik<<wektor<<"\n";
  }

  plik.close();
}
