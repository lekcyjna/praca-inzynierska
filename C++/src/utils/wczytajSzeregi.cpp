#include <concepts>
#include "wczytajSzeregi.hpp"

std::vector<std::vector<double>> wczytajSzeregi1D(const std::filesystem::path& sciezka)
{
  std::ifstream plik;
  plik.exceptions(std::ifstream::failbit);
  plik.open(sciezka);
  std::vector<std::vector<double>> szeregi;

  while(!plik.eof())
  {
    std::string linia;
    std::getline(plik, linia);
    plik>>std::ws;
    std::vector<double> ciag;
    std::stringstream liniaStream{linia};
    double a;
    liniaStream >> a;
    do
    {
      ciag.push_back(a);
      liniaStream >> a;
    } while(!liniaStream.eof());
    szeregi.push_back(ciag);
  }
  plik.close();
  return szeregi;
}
