#pragma once
#include <adept.h>
#include <concepts>
#include <ranges>

namespace wiezy {

template <typename T>
concept Ciag = 
  requires (T x)
{
  typename T::value_type;
  requires std::ranges::range<T>;
  x.size();// -> std::convertible_to<int>;
};

template <typename T>
concept kolekcja = Ciag<T>;

template <typename T>
concept KolekcjaCiagow = 
  requires (T x)
{
  requires kolekcja<T>;
  requires Ciag<typename T::value_type>;
};

template <typename T>
concept Xdouble = std::same_as<T, double> || std::same_as<T, adept::adouble>;

template <typename T, typename F>
concept ZmienneFunkcjeDTW =
requires (typename T::value_type t, F f, int i)
{
  {f.funkcjaOdleglosci(t,t)} -> Xdouble;
  {f.wiezyMacierzyDynamicznej(i,i,i,i)} -> std::same_as<bool>;
};


}
