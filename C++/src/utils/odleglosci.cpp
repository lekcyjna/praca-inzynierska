#include "odleglosci.hpp"

namespace src{
namespace utils{
  std::function<double(std::vector<int>,std::vector<int>)> odlEuklidesowa=
    [](std::vector<int> a, std::vector<int> b) -> double {
      long long odl=0;
      int size=a.size();
      for (int i=0;i<size;i++)
        odl+=(a[i]-b[i])*(a[i]-b[i]);
      return odl;
    };
}
}
