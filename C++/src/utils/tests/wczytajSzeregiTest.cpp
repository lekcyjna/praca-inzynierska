#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "src/utils/wczytajSzeregi.hpp"

TEST(wczytajSzeregiTest, nieistniejacyPlikPowinnienRzucicWyjatek)
{
  EXPECT_THROW(wczytajSzeregi1D("dfghfgbsidfuhnsdfnfdjgbfdfkghnsldsbfgjkfglsdhgljbsdljfbdskgsdlfihepfihpehiohiqpweihri4ehoedhfldfj"),
      std::ios_base::failure);
}
