/// Wymagania:
/// - Przyjmuje kolekcję (lub dwie) elementów
/// - Przyjmuje funkcję odległości między elementami
/// - Wylicza odległość między elementami z kolekcji pierszej i drugiej
/// - Wylicza odległości między elementami w tej samej kolekcji
/// - Wyliczanie odbywa się wielowątkowo
//
//
/// Pomysł na implementację:
/// - W pierwszej fazie zostanie przygotowana kolejka:
///   - Będzie zabezpieczona muteksem
///   - Będą znajdowały się w niej wszystkie pary dla których należy wyliczyć odległość, wraz z współrzędnymi w macierzy do zapisania wyniku
/// - W drugiej fazie zostanie utworzony zbiór wątków:
///   - Będą one pobierały kolejne pary z kolejki
///   - Wyliczały dla nich odległość
///   - I zapisywały do macierzy wynikowej zabezpieczonej muteksem

#pragma once
#include <mutex>
#include <vector>
#include <ranges>
#include <functional>
#include <queue>
#include <thread>
#include "src/utils/wiezy.hpp"

typedef std::vector<std::vector<double>> MacierzOdleglosci;


template <typename T>
  requires wiezy::kolekcja<T>
class FabrykaMacierzOdleglosci
{
  public:
    FabrykaMacierzOdleglosci(std::function<double(const typename T::value_type&, const typename T::value_type&)>);
    MacierzOdleglosci operator() (const T&) const;
    MacierzOdleglosci operator() (const T&, const T&) const;

  private:
    std::function<double(const typename T::value_type&, const typename T::value_type&)> funkcjaOdleglosci;
    MacierzOdleglosci wyliczMacierz(std::queue<std::tuple<std::reference_wrapper<const typename T::value_type>,
        std::reference_wrapper<const typename T::value_type>, int, int>>&&
        kolejkaParDoObliczenia, int dlugoscX, int dlugoscY) const;
    void logikaWatkuLiczacego(
        std::queue<std::tuple<std::reference_wrapper<const typename T::value_type>, 
        std::reference_wrapper<const typename T::value_type>, int, int>>&, std::mutex&,
        MacierzOdleglosci&) const;
//    std::mutex kolejkaParDoObliczeniaMutex;
};
