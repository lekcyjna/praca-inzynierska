#pragma once
#include "src/utils/wiezy.hpp"
#include <adept_arrays.h>
#include <functional>
namespace miary{

template <typename T>
  requires requires (T t) {t-t;}
class EuklidesProstyBezWiezowMacierzy
{
  public:
    static double funkcjaOdleglosci(const T& a, const T& b)
    {
      double x = a-b;
      return x*x;
    }
    
    /// Sprawdza czy należy wyliczyć pole x,y z macierzy dynamicznej
    /// zwraca true jeśli tak, false wpp. dlugoscX i dlugoscY to
    /// dlugosci macierzy przyrownania wzdłuż odpowiednich osi
    /// x,y,dlugoscX,dlugoscY
    static bool wiezyMacierzyDynamicznej(int, int, int, int)
    {
      return true;
    }
};

template <>
class  EuklidesProstyBezWiezowMacierzy<adept::Vector>
{
  public:
    static double funkcjaOdleglosci(const adept::Vector& a, const adept::Vector& b)
    {
      auto x = a-b;
      return adept::sum(x*x);
    }
    
    static bool wiezyMacierzyDynamicznej(int, int, int, int)
    {
      return true;
    }

    /// Zwraca pochodną funkcji kosztu ze względu na zmienną "a"
    /// a dokładnie to zwraca gradient, ponieważ zmienna "a" może być wielowymiarowa
    static adept::Vector pochodna(const adept::Vector& a, const adept::Vector& b)
    {
      return 2*(a-b);
    }
};

template<>
class  EuklidesProstyBezWiezowMacierzy<adept::aVector>
{
  public:
    static adept::adouble funkcjaOdleglosci(const adept::aVector& a, const adept::Vector& b)
    {
      auto x = a-b;
      return adept::sum(x*x);
    }
    
    static bool wiezyMacierzyDynamicznej(int, int, int, int)
    {
      return true;
    }
};

template <typename T>
requires wiezy::Ciag<T>
class EuklidesKontenerLiczbBezWiezowMacierzy
{
  public:
    static double funkcjaOdleglosci(const T& a, const T& b)
    {
      double suma=0;
      for(unsigned int i=0;i<a.size();i++)
        suma+=(a[i]-b[i])*(a[i]-b[i]);
      return suma;
    }
    
    static bool wiezyMacierzyDynamicznej(int, int, int, int)
    {
      return true;
    }
};

template <typename T>
class FunkcjaLambdaBezWiezowMacierzy
{
  private:
    std::function<double(const T&, const T&)> fun;
  public:
    FunkcjaLambdaBezWiezowMacierzy(std::function<double(const T&, const T&)> f) : fun{f} {};

    double funkcjaOdleglosci(const T& a, const T& b) const
    {
      return fun(a,b);
    }
    
    static bool wiezyMacierzyDynamicznej(int, int, int, int)
    {
      return true;
    }
};




}
