/// Wymagania dla DTW:
/// - musi zwracać długość najlepszego przyrównania
/// - musi zwracać najlepsze przyrównanie
/// - operuje na dwóch ciągach
/// - ciągi są niemodyfikowane
/// - domyślnie używa odległości euklidesowej
/// - łatwo się rozszerza o zmienę miary odległości
/// - ma możliwość dodawania ograniczeń na macierz przyrównania
/// - powinno być thread-safe
/// - wsparcie dla ciągów wielowymiarowych
//
//
/// Rozwiązanie wymagań:
/// - funkcja odległości przekazywana jako lambda
/// - ograniczenia na macierz przyrównań dodawane poprzez dziedziczenie
/// - w alg dynamicznym wywoływana funkcja nadpisywana, która sprawdza czy dane pole wziąć pod uwagę
/// - implementacja obiektowa
/// - implementacja na szablonach
/// - użyć konceptów

#pragma once
#include<functional>
#include<vector>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include "src/utils/wiezy.hpp"

typedef std::vector<std::vector<int>> Przyrownanie;

template <typename T, typename F>
  requires wiezy::Ciag<T> and wiezy::ZmienneFunkcjeDTW<T,F>
class DTW 
{
  protected:

    enum class Kierunek : std::uint8_t
    {
      poczatek = 0,
      zGory,
      zLewej,
      zeSkosu,
      bledny = 255
    };
  public:
    DTW(F);

    /// Wylicza DTW między dwoma ciągami i zwraca koszt oraz przyrównanie drugiego ciągu do pierwszego
    std::tuple<double,Przyrownanie> operator() (const T&, const T&) const;
    static void setLogger();

  private:
    double INF=1e307;
    F funkcje;

    std::tuple<double, Kierunek> znajdzMinimum(int i, int j,
        const std::vector<std::vector<double>>& koszty, const std::vector<std::vector<Kierunek>>& droga) const;
    void wypelnijMacierzPrzyrownania(const T&, const T&, std::vector<std::vector<double>>&, std::vector<std::vector<Kierunek>>&) const;
    Przyrownanie wyznaczPrzyrownanie(const std::vector<std::vector<Kierunek>>&) const;
};


