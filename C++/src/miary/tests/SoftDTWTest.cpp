#include "src/miary/SoftDTW.hpp"
#include "src/miary/SoftDTW.tpp"
#include <gtest/gtest.h>
#include "src/miary/ZmienneFunkcjeDTW.tpp"

TEST(SoftDTWTest, 01testNaIntachDlugieCiagi)
{
  adept::Matrix M1(10,1);
  adept::Matrix M2(14,1);
  M1 << 1, 12, 19, 21, 19, 23, 29, 23, 16, 24;
  M2 << 22, 22, 26, 11,  4,  0, 13, 16, 24, 15,  4, 19, 11,  3;
  miary::EuklidesProstyBezWiezowMacierzy<adept::Vector> euk;
  SoftDTW<adept::Matrix, decltype(euk)> softDtw {euk};
  double wynik=softDtw(M1,M2);
  EXPECT_DOUBLE_EQ(wynik, 1636.306852650613);
}

TEST(SoftDTWTest, 02testDodatnichDoubli)
{
  adept::Matrix M1(5,1);
  adept::Matrix M2(3,1);
  M1 << 1.877645  , 9.22869519, 7.12875878, 8.78412798, 6.35216643;
  M2 << 0.13884815, 9.21035995, 2.51637103;
  miary::EuklidesProstyBezWiezowMacierzy<adept::Vector> euk;
  SoftDTW<adept::Matrix, decltype(euk)> softDtw {euk};
  double wynik=softDtw(M1,M2);
  EXPECT_NEAR(wynik, 22.251531045544453, 0.000001);
}

TEST(SoftDTWTest, 03krotkiTestNaDoublach)
{
  adept::Matrix M1(3,1);
  adept::Matrix M2(2,1);
  M1 <<0.26953826,  0.24890706, -0.84596861;
  M2 <<-1.02806895, -0.92769885;
  miary::EuklidesProstyBezWiezowMacierzy<adept::Vector> euk;
  SoftDTW<adept::Matrix, decltype(euk)> softDtw {euk};
  double wynik=softDtw(M1,M2);
  EXPECT_NEAR(wynik, 1.9855525367297413, 0.000001);
}

TEST(SoftDTWTest, 04testNaDoublachOdchStd1)
{
  adept::Matrix M1(15,1);
  adept::Matrix M2(5,1);
  M1 << 0.78851803,  1.29676084, -0.83033854, -0.64199902, -0.30130679,
       -0.33225751, -1.04267286, -0.39074706,  1.1174393 , -1.2024189 ,
        2.24242859,  0.16788374, -0.90690311,  0.07662095, -0.08839654;
  M2 << -0.19995783, -0.72793114,  0.07829569,  0.20847645,  2.43765637;
  miary::EuklidesProstyBezWiezowMacierzy<adept::Vector> euk;
  SoftDTW<adept::Matrix, decltype(euk)> softDtw {euk};
  double wynik=softDtw(M1,M2);
  EXPECT_NEAR(wynik, 12.062736236003731, 0.000001);
  double wynik2=softDtw(M2,M1);
  EXPECT_NEAR(wynik, wynik2, 0.000001);
}

TEST(SoftDTWTest, 05testNaAdoublachOdchStd1)
{
  adept::Stack ast;
  adept::aMatrix M1(15,1);
  adept::Matrix M2(5,1);
  M1 << 0.78851803,  1.29676084, -0.83033854, -0.64199902, -0.30130679,
       -0.33225751, -1.04267286, -0.39074706,  1.1174393 , -1.2024189 ,
        2.24242859,  0.16788374, -0.90690311,  0.07662095, -0.08839654;
  M2  << -0.19995783, -0.72793114,  0.07829569,  0.20847645,  2.43765637;
  miary::EuklidesProstyBezWiezowMacierzy<adept::aVector> euk;
  SoftDTW<adept::aMatrix, decltype(euk)> softDtw {euk};
  adept::adouble wynik=softDtw(M1,M2);
  EXPECT_NEAR(wynik.value(), 12.062736236003731, 0.000001);
}

TEST(SoftDTWTest, 06testPochodnej)
{
  adept::Stack ast;
  adept::aMatrix M1 {{2}};
  adept::Matrix M2 {{10}};
  miary::EuklidesProstyBezWiezowMacierzy<adept::aVector> euk;
  SoftDTW<adept::aMatrix, decltype(euk)> softDtw {euk};
  ast.new_recording();
  adept::adouble wynik=softDtw(M1,M2);
  wynik.set_gradient(1.0);
  ast.compute_adjoint();
  EXPECT_DOUBLE_EQ(-16, M1[0][0].get_gradient());
}

TEST(SoftDTWTest, 07testPochodnej)
{
  adept::Stack ast;
  adept::aMatrix M1 {{2},{3},{4},{5}};
  adept::Matrix M2 {{10},{10},{1},{2},{15},{10}};
  miary::EuklidesProstyBezWiezowMacierzy<adept::aVector> euk;
  SoftDTW<adept::aMatrix, decltype(euk)> softDtw {euk};
  ast.new_recording();
  adept::adouble wynik=softDtw(M1,M2);
  wynik.set_gradient(1.0);
  ast.compute_adjoint();
  auto g = M1.get_gradient();
  for(int i=0;i<4;i++)
    EXPECT_NE(0, g(i,0));
}


TEST(SoftDTWTest, 08testNaAdoublachOdchStd1SmoothMinExpDiv)
{
  adept::Stack ast;
  adept::aMatrix M1(15,1);
  adept::Matrix M2(5,1);
  M1 << 0.78851803,  1.29676084, -0.83033854, -0.64199902, -0.30130679,
       -0.33225751, -1.04267286, -0.39074706,  1.1174393 , -1.2024189 ,
        2.24242859,  0.16788374, -0.90690311,  0.07662095, -0.08839654;
  M2  << -0.19995783, -0.72793114,  0.07829569,  0.20847645,  2.43765637;
  miary::EuklidesProstyBezWiezowMacierzy<adept::aVector> euk;
  SoftDTW<adept::aMatrix, decltype(euk), PSmoothMinType::ExpDiv> softDtw {euk};
  adept::adouble wynik=softDtw(M1,M2);
  EXPECT_NEAR(wynik.value(), 20.157067818416195, 0.000001);
}

TEST(SoftDTWTest, 09testPochodnejExpDiv)
{
  adept::Stack ast;
  adept::aMatrix M1 {{2},{3},{4},{5}};
  adept::Matrix M2 {{10},{10},{1},{2},{15},{10}};
  miary::EuklidesProstyBezWiezowMacierzy<adept::aVector> euk;
  SoftDTW<adept::aMatrix, decltype(euk), PSmoothMinType::ExpDiv> softDtw {euk};
  ast.new_recording();
  adept::adouble wynik=softDtw(M1,M2);
  wynik.set_gradient(1.0);
  ast.compute_adjoint();
  auto g = M1.get_gradient();
  for(int i=0;i<4;i++)
  {
    EXPECT_LT(-1000000, g(i,0));
    EXPECT_GT(1000000, g(i,0));
    EXPECT_NE(0, g(i,0));
  }
}

/*
TEST(SoftDTWTest, NOT_TEST_wypisanieDoPracy)
{
  adept::Stack ast;
  adept::Matrix M1(3,1);
  adept::Matrix M2(2,1);
  adept::Matrix M3(3,1);
  M1 << 0, 0, 0;
  M2 << 0, 10;
  M3 << 10, 10, 10;
  miary::EuklidesProstyBezWiezowMacierzy<adept::Vector> euk;
  SoftDTW<adept::Matrix, decltype(euk), PSmoothMinType::ExpDiv> softDtw {euk};
  std::cerr << softDtw(M1,M2) <<" "<<softDtw(M1, M3)<<" "<<softDtw(M2,M3)<<"\n";
}
*/
