#include "src/miary/DTW.tpp"
#include "src/miary/ZmienneFunkcjeDTW.tpp"
#include <gtest/gtest.h>
#include <vector>

template <typename T>
class parametry
{
  public:
    T argument1;
    T argument2;
    double wynik;
};

class DTWTestVectorIntow : public testing::TestWithParam<parametry<std::vector<int>>>
{
};

TEST_P(DTWTestVectorIntow, testyWektoraIntow)
{
  parametry p = GetParam();
  DTW<std::vector<int>, miary::EuklidesProstyBezWiezowMacierzy<int>> 
    dtw{miary::EuklidesProstyBezWiezowMacierzy<int>()};
  auto [koszt, przyrownanie] {dtw(p.argument1, p.argument2)};
  EXPECT_DOUBLE_EQ(p.wynik, koszt);
}

INSTANTIATE_TEST_SUITE_P(PierwszeTesty, DTWTestVectorIntow, testing::Values(
      parametry<std::vector<int>>{std::vector<int>{1,2,3},std::vector<int>{1,2,3},0},
      parametry<std::vector<int>>{std::vector<int>{0,0,0},std::vector<int>{3,3,3},27},
      parametry<std::vector<int>>{std::vector<int>{1,2,2,2,2,3},std::vector<int>{1,2,3},0},
      parametry<std::vector<int>>{std::vector<int>{1,2,3},std::vector<int>{1,2,2,2,2,3},0},
      parametry<std::vector<int>>{std::vector<int>{1,2,3},std::vector<int>{0,0,2,3},2},
      parametry<std::vector<int>>{std::vector<int>{1,6,3},std::vector<int>{1,4,8,4,3},9}
      ));


class DTWTestVectorVectorowDoubli : public testing::TestWithParam<parametry<std::vector<std::vector<double>>>>
{
};

TEST_P(DTWTestVectorVectorowDoubli, testy)
{
  parametry p = GetParam();
  DTW<std::vector<std::vector<double>>, miary::EuklidesKontenerLiczbBezWiezowMacierzy<std::vector<double>>> 
    dtw{miary::EuklidesKontenerLiczbBezWiezowMacierzy<std::vector<double>>()};
  auto [koszt, przyrownanie] {dtw(p.argument1, p.argument2)};
  EXPECT_DOUBLE_EQ(p.wynik, koszt);
}


INSTANTIATE_TEST_SUITE_P(TestyDoubli, DTWTestVectorVectorowDoubli, testing::Values(
      parametry<std::vector<std::vector<double>>>
      {std::vector<std::vector<double>>{{1.0,1.5}, {3.0,2.0}, {1.6,2.0}},
      std::vector<std::vector<double>>{{2.0, 5.5}, {0,-1}, {1.6,12}}, 135},
      parametry<std::vector<std::vector<double>>>
      {std::vector<std::vector<double>>{{1.0,1.5}, {3.0,2.0}, {1,2.0}, {1.6, 12}, {1.6,12},{1.6,12}},
      std::vector<std::vector<double>>{{2.0, 5.5}, {0,-1}, {1.6,12}}, 40.25}
      ));
