#include <gtest/gtest.h>
#include "src/miary/DTW.tpp"
#include "src/miary/macierzOdleglosci.tpp"
#include "src/miary/ZmienneFunkcjeDTW.tpp"

TEST(MacierzOdleglosciTest, odlegloscEuklidesowa)
{
  std::vector<int> zbior = {1,2,3,6, -1};
  std::function<double(int,int)> odlEuklidesowa=[](int a, int b) -> double {long long roznica=a-b; return roznica*roznica;};
  FabrykaMacierzOdleglosci<std::vector<int>> fabryka {odlEuklidesowa};
  MacierzOdleglosci macierz = fabryka(zbior);
  MacierzOdleglosci poprawna = {{0,1,4,25,4},{1,0,1,16,9},{4,1,0, 9, 16},{25,16,9,0,49},{4,9,16,49,0}};
  EXPECT_EQ(poprawna, macierz);
}

TEST(MacierzOdleglosciTest, odlegloscEuklidesowaDwaZbiory)
{
  std::vector<int> zbior1 = {1,2,1};
  std::vector<int> zbior2 = {2,3};
  std::function<double(int,int)> odlEuklidesowa=[](int a, int b) -> double {long long roznica=a-b; return roznica*roznica;};
  FabrykaMacierzOdleglosci<std::vector<int>> fabryka {odlEuklidesowa};
  MacierzOdleglosci macierz = fabryka(zbior1, zbior2);
  MacierzOdleglosci poprawna = {{1,4},{0,1}, {1,4}};
  EXPECT_EQ(poprawna, macierz);
}


TEST(MacierzOdleglosciTest, DTWDwaZbiory)
{
  std::vector<std::vector<int>> zbior1 = {{1,0,0},{1,1,2,3},{4,5}};
  std::vector<std::vector<int>> zbior2 = {{1,1,1}};
  DTW<std::vector<int>, miary::EuklidesProstyBezWiezowMacierzy<int>> 
    dtw{miary::EuklidesProstyBezWiezowMacierzy<int>()};
  std::function<double(std::vector<int>,std::vector<int>)> odlDtw=[&dtw](auto a, auto b) -> double {
    auto [koszt, przyrownanie] {dtw(a,b)};
    return koszt;
  };
  FabrykaMacierzOdleglosci<std::vector<std::vector<int>>> fabryka {odlDtw};
  MacierzOdleglosci macierz = fabryka(zbior1, zbior2);
  MacierzOdleglosci poprawna = {{2}, {5}, {34}};
  EXPECT_EQ(poprawna, macierz);
}
