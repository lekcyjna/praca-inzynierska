#pragma once
#include "DTW.hpp"

template <typename T, typename F>
  requires wiezy::Ciag<T> and wiezy::ZmienneFunkcjeDTW<T,F>
DTW<T,F>::DTW(F f)
  : funkcje{f}
{}

template <typename T, typename F>
  requires wiezy::Ciag<T> and wiezy::ZmienneFunkcjeDTW<T,F>
std::tuple<double, Przyrownanie> DTW<T,F>::operator()(const T& ciagWzorcowy, const T& ciagPrzyrownywany) const
{
  int dlWzorca=ciagWzorcowy.size();
  int dlPrzyrownywanego=ciagPrzyrownywany.size();

  std::vector<std::vector<double>> tablicaKosztow;
  /// Opisuje drogę jaką trzeba wrócić by uzyskać przyrównanie
  std::vector<std::vector<Kierunek>> tablicaPowrotow;

  tablicaKosztow.resize(dlWzorca);
  tablicaPowrotow.resize(dlWzorca);
  for (std::vector<double> & v : tablicaKosztow)
    v.resize(dlPrzyrownywanego, 0);

  for (std::vector<Kierunek>& v : tablicaPowrotow)
    v.resize(dlPrzyrownywanego,Kierunek::bledny);

  wypelnijMacierzPrzyrownania(ciagWzorcowy, ciagPrzyrownywany, tablicaKosztow, tablicaPowrotow);
  Przyrownanie przyrownanie = wyznaczPrzyrownanie(tablicaPowrotow);
  return std::make_tuple(tablicaKosztow[dlWzorca-1][dlPrzyrownywanego-1], przyrownanie);
}


template <typename T, typename F>
  requires wiezy::Ciag<T> and wiezy::ZmienneFunkcjeDTW<T,F>
std::tuple<double, typename DTW<T,F>::Kierunek> DTW<T,F>::znajdzMinimum(
    int i, int j,
    const std::vector<std::vector<double>>& koszty,
    const std::vector<std::vector<Kierunek>>& droga) const
{
  constexpr std::int8_t zmianaWX[3]= {-1,-1,0};
  constexpr std::int8_t zmianaWY[3]= {-1,0,-1};
  constexpr Kierunek kierunekZmiany[3] = {Kierunek::zeSkosu, Kierunek::zLewej, Kierunek::zGory};

  if(i==0) [[unlikely]]
  {
    return std::make_tuple(koszty[i][j-1], Kierunek::zGory);
  }
  if(j==0) [[unlikely]]
  {
    return std::make_tuple(koszty[i-1][j], Kierunek::zLewej);
  }

  double minimum = INF;
  Kierunek kierunekPrzyjscia = Kierunek::bledny;

  for (int k=0;k<3;k++)
  {
    if (minimum>koszty[i+zmianaWX[k]][j+zmianaWY[k]] and droga[i+zmianaWX[k]][j+zmianaWY[k]]!=Kierunek::bledny)
    {
      minimum=koszty[i+zmianaWX[k]][j+zmianaWY[k]];
      kierunekPrzyjscia=kierunekZmiany[k];
    }
  }
//  spdlog::debug("Wyliczono minimum: {} dla pola {},{} z kierunku: {}", minimum, i, j, (std::int8_t)kierunekPrzyjscia);
  return std::make_tuple(minimum, kierunekPrzyjscia);
}


template <typename T, typename F>
  requires wiezy::Ciag<T> and wiezy::ZmienneFunkcjeDTW<T,F>
void DTW<T,F>::wypelnijMacierzPrzyrownania(const T& ciagWzorcowy, const T& ciagPrzyrownywany, 
    std::vector<std::vector<double>>& tablicaKosztow, std::vector<std::vector<Kierunek>>& tablicaPowrotow) const
{
  int dlWzorca=ciagWzorcowy.size();
  int dlPrzyrownywanego=ciagPrzyrownywany.size();
  tablicaPowrotow[0][0]=Kierunek::poczatek;
  tablicaKosztow[0][0]=funkcje.funkcjaOdleglosci(ciagWzorcowy[0], ciagPrzyrownywany[0]);

  for (int k=1;k<dlPrzyrownywanego;k++)
  {
    if (not funkcje.wiezyMacierzyDynamicznej(0,k, dlWzorca, dlPrzyrownywanego))
    {
      tablicaKosztow[0][k]=INF;
      continue;
    }
    tablicaPowrotow[0][k]=Kierunek::zGory;
    tablicaKosztow[0][k]=tablicaKosztow[0][k-1]+funkcje.funkcjaOdleglosci(ciagWzorcowy[0], ciagPrzyrownywany[k]);
  }
  for (int k=1;k<dlWzorca;k++)
  {
    if (not funkcje.wiezyMacierzyDynamicznej(k,0, dlWzorca, dlPrzyrownywanego))
    {
      tablicaKosztow[k][0]=INF;
      continue;
    }
    tablicaPowrotow[k][0]=Kierunek::zLewej;
    tablicaKosztow[k][0]=tablicaKosztow[k-1][0]+funkcje.funkcjaOdleglosci(ciagWzorcowy[k], ciagPrzyrownywany[0]);
  }

  for (int i=1;i<dlWzorca;i++)
  {
    const typename T::value_type& elementWzorca = ciagWzorcowy[i];
    for (int j=1;j<dlPrzyrownywanego;j++)
    {
      const typename T::value_type& elementPrzyrownywany = ciagPrzyrownywany[j];
      if (not funkcje.wiezyMacierzyDynamicznej(i,j, dlWzorca, dlPrzyrownywanego))
      {
        tablicaKosztow[i][j]=INF;
        continue;
      }
      double odl = funkcje.funkcjaOdleglosci(elementWzorca, elementPrzyrownywany);
      auto [minimalnyKoszt, kierunek]{znajdzMinimum(i,j,tablicaKosztow, tablicaPowrotow)};
      tablicaKosztow[i][j]=odl+minimalnyKoszt;
      tablicaPowrotow[i][j]=kierunek;
    }
  }
  return;
}

template <typename T, typename F>
  requires wiezy::Ciag<T> and wiezy::ZmienneFunkcjeDTW<T,F>
Przyrownanie DTW<T,F>::wyznaczPrzyrownanie(const std::vector<std::vector<Kierunek>>& tablicaPowrotow) const
{
  int dlWzorca=tablicaPowrotow.size();
  int dlPrzyrownywanego=tablicaPowrotow[0].size();
  Przyrownanie przyrownanie;
  przyrownanie.resize(dlWzorca);
  int i=dlWzorca-1, j=dlPrzyrownywanego-1;
  while (i!=0 or j!=0)
  {
    przyrownanie[i].push_back(j);
    switch (tablicaPowrotow[i][j])
    {
      case Kierunek::zeSkosu:
        i--;
        j--;
        break;
      case Kierunek::zLewej:
        i--;
        break;
      case Kierunek::zGory:
        j--;
        break;
      default:
        throw std::runtime_error("Bladny kierunek przyjscia: "+std::to_string((std::int8_t)tablicaPowrotow[i][j])+" i: "
            +std::to_string(i)+" j: "+std::to_string(j));
    }
  }
  return przyrownanie;
}
