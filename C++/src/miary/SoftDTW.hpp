// Uwaga do pracy: ważna jest normalizacja danych, jeśli nie są bliskie 1 to powstają duże błędy numeryczne
#pragma once
#include <adept_arrays.h>
#include <vector>
#include "src/utils/wiezy.hpp"

enum class PSmoothMinType
{
  LSE = 0,
  ExpDiv
};

template <typename T1, typename F, PSmoothMinType pSmoothMinType = PSmoothMinType::LSE>
requires (std::same_as<T1, adept::aMatrix> or std::same_as<T1, adept::Matrix>) and
  wiezy::ZmienneFunkcjeDTW<T1,F>
class SoftDTW 
{
  public:
    SoftDTW(F, double gamma = 1);

    typedef typename std::conditional<std::is_same_v<T1, adept::Matrix>, double, adept::adouble>::type xdouble;

    /// Wylicza SoftDTW między dwoma ciągami
    xdouble operator() (const T1&, const adept::Matrix&) const;
    T1 wyliczMacierzDynamiczna (const T1&, const adept::Matrix&) const;
    adept::Matrix backward(const adept::Matrix&, const adept::Matrix&, const adept::Matrix&) const;

    const F funkcje;

  private:
    double INF=1e307;
    double gamma;
    double gammaOdw;

    typedef T1 xMatrix;

    xdouble softMinimum(xdouble, xdouble, xdouble) const;
    void wypelnijMacierzDynamiczna(const T1& ciagWzorcowy, const adept::Matrix& ciagPrzyrownywany, 
        xMatrix& macierzDynamiczna) const;

    inline double wyliczWspolczynnikPochodnej(const adept::Matrix& koszty, const adept::Matrix& ciagWzorcowy,
        const adept::Matrix& ciagPrzyrownywany, int i, int j, uint8_t di, uint8_t dj) const;
};


