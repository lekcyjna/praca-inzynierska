#pragma once
#include "SoftDTW.hpp"
#include <cmath>
#include <adept_arrays.h>
#include <adept.h>

namespace {

template<typename T>
T LSE(T a1, T a2, T a3)
{
  T m = std::max(std::max(a1,a2),a3);
  T wynik = m + log(exp(a1-m)+exp(a2-m)+exp(a3-m));
  return wynik;
}
}

template <typename T1, typename F, PSmoothMinType pSmoothMinType>
requires (std::same_as<T1, adept::aMatrix> or std::same_as<T1, adept::Matrix>) and
  wiezy::ZmienneFunkcjeDTW<T1,F>
SoftDTW<T1,F, pSmoothMinType>::SoftDTW(F f, double gamma)
  : funkcje{f}
  , gamma {gamma}
  , gammaOdw {1/gamma}
{}

template <typename T1, typename F, PSmoothMinType pSmoothMinType>
requires (std::same_as<T1, adept::aMatrix> or std::same_as<T1, adept::Matrix>) and
  wiezy::ZmienneFunkcjeDTW<T1,F>
typename SoftDTW<T1,F,pSmoothMinType>::xdouble SoftDTW<T1, F,pSmoothMinType>::operator()
  (const T1& ciagWzorcowy, const adept::Matrix& ciagPrzyrownywany) const
{
  const unsigned int dlWzorca = ciagWzorcowy.size(0);
  const unsigned int dlPrzyrownywanego = ciagPrzyrownywany.size(0);

  xMatrix macierzDynamiczna(dlWzorca, dlPrzyrownywanego);

  wypelnijMacierzDynamiczna(ciagWzorcowy, ciagPrzyrownywany, macierzDynamiczna);

  return macierzDynamiczna(dlWzorca-1, dlPrzyrownywanego-1);
}

template <typename T1, typename F, PSmoothMinType pSmoothMinType>
requires (std::same_as<T1, adept::aMatrix> or std::same_as<T1, adept::Matrix>) and
  wiezy::ZmienneFunkcjeDTW<T1,F>
T1 SoftDTW<T1, F,pSmoothMinType>::wyliczMacierzDynamiczna(const T1& ciagWzorcowy, const adept::Matrix& ciagPrzyrownywany) const
{
  const unsigned int dlWzorca = ciagWzorcowy.size(0);
  const unsigned int dlPrzyrownywanego = ciagPrzyrownywany.size(0);

  xMatrix macierzDynamiczna(dlWzorca, dlPrzyrownywanego);

  wypelnijMacierzDynamiczna(ciagWzorcowy, ciagPrzyrownywany, macierzDynamiczna);

  return macierzDynamiczna;
}

template <typename T1, typename F, PSmoothMinType pSmoothMinType>
requires (std::same_as<T1, adept::aMatrix> or std::same_as<T1, adept::Matrix>) and
  wiezy::ZmienneFunkcjeDTW<T1,F>
void SoftDTW<T1, F,pSmoothMinType>::wypelnijMacierzDynamiczna(const T1& ciagWzorcowy, const adept::Matrix& ciagPrzyrownywany, 
    xMatrix& macierzDynamiczna) const
{
  using adept::__;
  const unsigned int dlWzorca = ciagWzorcowy.size(0);
  const unsigned int dlPrzyrownywanego = ciagPrzyrownywany.size(0);

  macierzDynamiczna(0,0)=funkcje.funkcjaOdleglosci(ciagWzorcowy[0], ciagPrzyrownywany[0]);

  for (unsigned int k=1;k<dlPrzyrownywanego;k++)
  {
    if (not funkcje.wiezyMacierzyDynamicznej(0,k, dlWzorca, dlPrzyrownywanego))
    {
      if constexpr (std::is_same_v<xdouble, adept::adouble>)
        macierzDynamiczna(0,k).set_value(INF);
      else
        macierzDynamiczna(0,k)=INF;
      continue;
    }
    macierzDynamiczna(0,k)=macierzDynamiczna(0,k-1)+funkcje.funkcjaOdleglosci(ciagWzorcowy[0], ciagPrzyrownywany[k]);
  }
  for (unsigned int k=1;k<dlWzorca;k++)
  {
    if (not funkcje.wiezyMacierzyDynamicznej(k,0, dlWzorca, dlPrzyrownywanego))
    {
      if constexpr (std::is_same_v<xdouble, adept::adouble>)
        macierzDynamiczna(k,0).set_value(INF);
      else
        macierzDynamiczna(k,0)=INF;
      continue;
    }
    macierzDynamiczna(k,0)=macierzDynamiczna(k-1,0)+funkcje.funkcjaOdleglosci(ciagWzorcowy[k], ciagPrzyrownywany[0]);
  }

  for (unsigned int i=1; i<dlWzorca; i++)
  {
    for (unsigned int j=1; j<dlPrzyrownywanego; j++)
    {
      if (not funkcje.wiezyMacierzyDynamicznej(i, j, dlWzorca, dlPrzyrownywanego))
      {
        if constexpr (std::is_same_v<xdouble, adept::adouble>)
          macierzDynamiczna(i,j).set_value(INF);
        else
          macierzDynamiczna(i,j)=INF;
        continue;
      }
      macierzDynamiczna(i,j)=softMinimum(macierzDynamiczna(i-1,j-1), macierzDynamiczna(i-1,j),
          macierzDynamiczna(i,j-1)) + funkcje.funkcjaOdleglosci(ciagWzorcowy[i], ciagPrzyrownywany[j]);
    }
  }
  return;
}


template <typename T1, typename F, PSmoothMinType pSmoothMinType>
requires (std::same_as<T1, adept::aMatrix> or std::same_as<T1, adept::Matrix>) and
  wiezy::ZmienneFunkcjeDTW<T1,F>
typename SoftDTW<T1,F,pSmoothMinType>::xdouble SoftDTW<T1,F,pSmoothMinType>::softMinimum(xdouble a1, xdouble a2, xdouble a3) const
{
  /*
  #include <experimental/simd>
  namespace stdx = std::experimental;
  alignas(stdx::memory_alignment_v<stdx::fixed_size_simd<double,3>>) double A[3]={a1,a2,a3};
  auto t = stdx::fixed_size_simd<double,3> (A,stdx::vector_aligned);
  t/=-gamma;
  double m=stdx::hmax(t);
  t=stdx::exp(t-m);
  double s=stdx::reduce(t);
  return (log(s)+m)*(-gamma);
  */
  
  if constexpr (pSmoothMinType==PSmoothMinType::LSE)
  {
    a1*=-gammaOdw;
    a2*=-gammaOdw;
    a3*=-gammaOdw;
    xdouble logarytm = LSE(a1,a2,a3);
    xdouble wynik = (logarytm)*(-gamma);
    return wynik;
  }
  else
  {
    double eps = 1e-100;
    xdouble la1 = a1*-gammaOdw+log(a1+eps);
    xdouble la2 = a2*-gammaOdw+log(a2+eps);
    xdouble la3 = a3*-gammaOdw+log(a3+eps);
    xdouble logLicznik = LSE(la1,la2,la3);

    xdouble ma1 = a1*-gammaOdw;
    xdouble ma2 = a2*-gammaOdw;
    xdouble ma3 = a3*-gammaOdw;
    xdouble logMianownik = LSE(ma1,ma2,ma3);

    return exp(logLicznik-logMianownik);
  }
}

template <typename T1, typename F, PSmoothMinType pSmoothMinType>
requires (std::same_as<T1, adept::aMatrix> or std::same_as<T1, adept::Matrix>) and
  wiezy::ZmienneFunkcjeDTW<T1,F>
inline double SoftDTW<T1,F,pSmoothMinType>::wyliczWspolczynnikPochodnej(const adept::Matrix& koszty, const adept::Matrix& ciagWzorcowy,
    const adept::Matrix& ciagPrzyrownywany, int i, int j, uint8_t di, uint8_t dj) const
{
  double wsp;
  if constexpr (pSmoothMinType==PSmoothMinType::LSE)
  {
    wsp=exp((koszty(i+di, j+dj)-koszty(i, j)-
        funkcje.funkcjaOdleglosci(ciagWzorcowy[i+di], ciagPrzyrownywany[j+dj]))*gammaOdw);
  }
  else
  {
    auto a1= (i+di-1>=0 and j+dj-1 >= 0) ? koszty(i+di-1, j+dj-1) : 0;
    auto a2= j+dj-1 >= 0 ? koszty(i+di, j+dj-1) : 0;
    auto a3= i+di-1 >= 0 ? koszty(i+di-1, j+dj) : 0;
    auto b1=a1*-gammaOdw;
    auto b2=a2*-gammaOdw;
    auto b3=a3*-gammaOdw;
    auto logEsuma=LSE(b1,b2,b3);

    auto nawias=1-gammaOdw*(koszty(i,j)-koszty(i+di,j+dj)+funkcje.funkcjaOdleglosci(ciagWzorcowy[i+di], ciagPrzyrownywany[j+dj]));
    wsp = exp(-gammaOdw*koszty(i,j)-logEsuma)*nawias;
  }
  return wsp;
}

template <typename T1, typename F, PSmoothMinType pSmoothMinType>
requires (std::same_as<T1, adept::aMatrix> or std::same_as<T1, adept::Matrix>) and
  wiezy::ZmienneFunkcjeDTW<T1,F>
adept::Matrix SoftDTW<T1,F,pSmoothMinType>::backward(const adept::Matrix& koszty, const adept::Matrix& ciagWzorcowy,
    const adept::Matrix& ciagPrzyrownywany) const
{
  adept::Matrix pochodne(koszty.dimensions());

  int dlWzorca = koszty.dimension(0);
  int dlPrzyrownywanego = koszty.dimension(1);

  pochodne(dlWzorca-1, dlPrzyrownywanego-1)=1;

  for(int i=dlWzorca-2; i>=0;i--)
  {
    pochodne(i, adept::end) = wyliczWspolczynnikPochodnej(koszty, ciagWzorcowy, ciagPrzyrownywany, i, dlPrzyrownywanego-1, 1,0)*
      pochodne(i+1, adept::end);
  }

  for(int j=dlPrzyrownywanego-2; j>=0;j--)
  {
    pochodne(adept::end,j) = wyliczWspolczynnikPochodnej(koszty, ciagWzorcowy, ciagPrzyrownywany, dlWzorca-1, j, 0, 1)*
      pochodne(adept::end, j+1);
  }

  for(int i=dlWzorca-2;i>=0;i--)
  {
    for(int j=dlPrzyrownywanego-2; j>=0;j--)
    {
      pochodne(i,j)=
       wyliczWspolczynnikPochodnej(koszty, ciagWzorcowy, ciagPrzyrownywany, i, j, 0, 1)*pochodne(i, j+1) +
       wyliczWspolczynnikPochodnej(koszty, ciagWzorcowy, ciagPrzyrownywany, i, j, 1, 0)*pochodne(i+1, j) +
       wyliczWspolczynnikPochodnej(koszty, ciagWzorcowy, ciagPrzyrownywany, i, j, 1, 1)*pochodne(i+1, j+1);
    }
  }
  return pochodne;
}


//#include "src/miary/ZmienneFunkcjeDTW.tpp"
//template class SoftDTW<adept::aMatrix, miary::EuklidesProstyBezWiezowMacierzy<adept::aVector, adept::adouble>>;
//template class SoftDTW<adept::Matrix, miary::EuklidesProstyBezWiezowMacierzy<adept::Vector, double>>;
