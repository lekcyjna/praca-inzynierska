#pragma once
#include "macierzOdleglosci.hpp"
#include <spdlog/spdlog.h>


template <typename T>
  requires wiezy::kolekcja<T>
FabrykaMacierzOdleglosci<T>::FabrykaMacierzOdleglosci(
    std::function<double(const typename T::value_type&, const typename T::value_type&)> funkcja)
  : funkcjaOdleglosci{funkcja}
{}

template <typename T>
  requires wiezy::kolekcja<T>
MacierzOdleglosci FabrykaMacierzOdleglosci<T>::operator()(const T& kolekcja) const
{
  std::queue<std::tuple<std::reference_wrapper<const typename T::value_type>,
    std::reference_wrapper<const typename T::value_type>, int, int>> kolejkaParDoObliczenia;
  for(unsigned int i=0; i<kolekcja.size();i++)
  {
    const typename T::value_type& element1 = kolekcja[i];
    for(unsigned int j=i; j<kolekcja.size(); j++)
    {
      const typename T::value_type& element2 = kolekcja[j]; 
      kolejkaParDoObliczenia.push(std::make_tuple(std::ref(element1), std::ref(element2), i, j));
    }
  }
  spdlog::info("{} Liczba par odleglosci do wyliczenia: {}", __func__, kolejkaParDoObliczenia.size());
  MacierzOdleglosci macierz = wyliczMacierz(std::move(kolejkaParDoObliczenia), kolekcja.size(), kolekcja.size());

  for(unsigned int i=0; i<kolekcja.size();i++)
  {
    for(unsigned int j=0; j<i; j++)
    {
      macierz[i][j]=macierz[j][i];
    }
  }
  return macierz;
}


template <typename T>
  requires wiezy::kolekcja<T>
MacierzOdleglosci FabrykaMacierzOdleglosci<T>::operator()(const T& kolekcja1, const T& kolekcja2) const
{
  std::queue<std::tuple<std::reference_wrapper<const typename T::value_type>,
    std::reference_wrapper<const typename T::value_type>, int, int>> kolejkaParDoObliczenia;
  for(unsigned int i=0; i<kolekcja1.size();i++)
  {
    const typename T::value_type& element1 = kolekcja1[i];
    for(unsigned int j=0; j<kolekcja2.size(); j++)
    {
      const typename T::value_type& element2 = kolekcja2[j]; 
      kolejkaParDoObliczenia.push(std::make_tuple(std::ref(element1), std::ref(element2), i, j));
    }
  }
  spdlog::info("{} Liczba par odleglosci do wyliczenia: {}", __func__, kolejkaParDoObliczenia.size());
  return wyliczMacierz(std::move(kolejkaParDoObliczenia), kolekcja1.size(), kolekcja2.size());
}


template <typename T>
  requires wiezy::kolekcja<T>
void FabrykaMacierzOdleglosci<T>::logikaWatkuLiczacego(
  std::queue<std::tuple<std::reference_wrapper<const typename T::value_type>,
    std::reference_wrapper<const typename T::value_type>, int, int>>& kolejkaParDoObliczenia,
  std::mutex& kolejkaParDoObliczeniaMutex, MacierzOdleglosci& macierz) const 
{
  while(true)
  {
    int rozmiarKolejki;

    kolejkaParDoObliczeniaMutex.lock();
    rozmiarKolejki=kolejkaParDoObliczenia.size();
    if (rozmiarKolejki==0) [[unlikely]]
    {
      kolejkaParDoObliczeniaMutex.unlock();
      break;
    }
    auto [element1, element2, wspX, wspY]{kolejkaParDoObliczenia.front()};
    kolejkaParDoObliczenia.pop();
    kolejkaParDoObliczeniaMutex.unlock();

    if(rozmiarKolejki%(1<<16)==0) [[unlikely]]
      spdlog::info("Pozostały rozmiar kolejki: {}", rozmiarKolejki);
    double wynik=funkcjaOdleglosci(element1, element2);
    macierz[wspX][wspY]=wynik;
  }
}

template <typename T>
  requires wiezy::kolekcja<T>
MacierzOdleglosci FabrykaMacierzOdleglosci<T>::wyliczMacierz(
  std::queue<std::tuple<std::reference_wrapper<const typename T::value_type>,
    std::reference_wrapper<const typename T::value_type>, int, int>>&& kolejkaParDoObliczenia,
  int dlugoscX, int dlugoscY) const
{
  MacierzOdleglosci macierz;
  macierz.resize(dlugoscX);
  for (auto& v : macierz)
    v.resize(dlugoscY,0);

  std::mutex kolejkaParDoObliczeniaMutex;

  std::vector<std::thread> listaWatkow;

  for(unsigned int i=0;i<std::thread::hardware_concurrency(); i++)
  {
    listaWatkow.push_back(std::thread(&FabrykaMacierzOdleglosci<T>::logikaWatkuLiczacego, this, 
          std::ref(kolejkaParDoObliczenia), std::ref(kolejkaParDoObliczeniaMutex),
          std::ref(macierz)));
  }
  for(unsigned int i=0;i<std::thread::hardware_concurrency(); i++)
  {
    listaWatkow[i].join();
  }
  return macierz;
}
