#include <gtest/gtest.h>
#include <spdlog/spdlog.h>

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  spdlog::set_level(spdlog::level::warn);
  for(int i=0;i<argc;i++)
  {
    if(std::string(argv[i])=="-d")
      spdlog::set_level(spdlog::level::debug);
    if(std::string(argv[i])=="-e")
      spdlog::set_level(spdlog::level::err);
    if(std::string(argv[i])=="-i")
      spdlog::set_level(spdlog::level::info);
  }  
  return RUN_ALL_TESTS();
}
