#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "src/grupowanie/Kmenoids.tpp"
#include "src/miary/ZmienneFunkcjeDTW.tpp"
#include "src/utils/odleglosci.hpp"
#include "src/miary/DTW.tpp"
#include "src/miary/macierzOdleglosci.tpp"

TEST(KmenoidsTest, prosteGrupowanieEuklidesem)
{
  std::vector<std::vector<int>> zbior = {
    {1,1,1}, {1,1,2}, {10,10,10}
  };
  std::mt19937 gen;
  gen.seed(14);

  Kmenoids<std::vector<std::vector<int>>> kmenoids{src::utils::odlEuklidesowa, gen, 2, 20};
  kmenoids.fit(zbior);
  auto centra = kmenoids.zwrocCentroidy();
  EXPECT_EQ(2, centra.size());
  EXPECT_THAT(zbior, testing::Contains(centra[0]));
  EXPECT_THAT(zbior, testing::Contains(centra[1]));
  if (centra[0][0]==1)
  {
    EXPECT_THAT((std::vector<std::vector<int>>{zbior[0], zbior[1]}), testing::Contains(centra[0]));
    EXPECT_EQ(zbior[2], centra[1]);
  }
  else
  {
    EXPECT_EQ(zbior[2], centra[0]);
    EXPECT_THAT((std::vector<std::vector<int>>{zbior[0], zbior[1]}), testing::Contains(centra[1]));
  }
}

TEST(KmenoidsTest, grupowanieEuklidesemZPrzesunieciemMiedzyGrupami)
{
  std::vector<std::vector<int>> zbior = {
    {1,1,-2}, {1,1,2}, {1,1,3}, {1,1,4}, {1,1,5}, {1,1,6}
  };
  std::mt19937 gen;

  Kmenoids<std::vector<std::vector<int>>> kmenoids{src::utils::odlEuklidesowa, gen, 2, 10};
  kmenoids.fit(zbior);
  kmenoids.ustawCentroidy(
      {{{1,1,3},2}, {{1,1,6},5}}
      );
  auto wynik = kmenoids.predict(zbior);

  for(int i=0;i<6;i++)
  {
    EXPECT_EQ(2, wynik[i].size());
    if (i<4)
    {
      EXPECT_EQ(1, wynik[i][0]);
      EXPECT_EQ(0, wynik[i][1]);
    }
    else
    {
      EXPECT_EQ(0, wynik[i][0]);
      EXPECT_EQ(1, wynik[i][1]);
    }
  }

  kmenoids.fit(zbior);
  wynik=kmenoids.predict(zbior);
  auto centra = kmenoids.zwrocCentroidy();
  EXPECT_EQ(2, centra.size());
  EXPECT_THAT((std::vector<std::vector<int>>{{1,1,2}, {1,1,5}}), testing::Contains(centra[0]));
  EXPECT_THAT((std::vector<std::vector<int>>{{1,1,2}, {1,1,5}}), testing::Contains(centra[1]));
  if (centra[0][2]==2)
  {
    for(int i=0;i<6;i++)
    {
      EXPECT_EQ(2, wynik[i].size());
      if (i<3)
      {
        EXPECT_EQ(1, wynik[i][0]);
        EXPECT_EQ(0, wynik[i][1]);
      }
      else
      {
        EXPECT_EQ(0, wynik[i][0]);
        EXPECT_EQ(1, wynik[i][1]);
      }
    }
    EXPECT_EQ(zbior[1], centra[0]);
    EXPECT_EQ(zbior[4], centra[1]);
  }
  else
  {
    for(int i=0;i<6;i++)
    {
      EXPECT_EQ(2, wynik[i].size());
      if (i<3)
      {
        EXPECT_EQ(0, wynik[i][0]);
        EXPECT_EQ(1, wynik[i][1]);
      }
      else
      {
        EXPECT_EQ(1, wynik[i][0]);
        EXPECT_EQ(0, wynik[i][1]);
      }
    }
    EXPECT_EQ(zbior[1], centra[1]);
    EXPECT_EQ(zbior[4], centra[0]);
  }
}


TEST(KmenoidsTest, prosteGrupowanieDTW)
{
  std::vector<std::vector<int>> zbior = {
    {1,1,1,1}, {1,1,2}, {10,10,10}, {10,11,10,10}, {10,7}, {100,100,100,100}
  };
  std::mt19937 gen;
  gen.seed(12);

  DTW<std::vector<int>, miary::EuklidesProstyBezWiezowMacierzy<int>> 
    dtw{miary::EuklidesProstyBezWiezowMacierzy<int>()};

  Kmenoids<std::vector<std::vector<int>>> kmenoids{
    [&dtw](const std::vector<int>& a, const std::vector<int>& b) ->double
    {
      auto [wynik, przyrownanie]{dtw(a,b)}; 
      return wynik;
    }
    , gen, 3, 20};
  kmenoids.ustawCentroidy({{{1,1,1,1},0}, {{10,7},4}, {{100,100,100,100},5}});
  kmenoids.fit(zbior);
  auto centra = kmenoids.zwrocCentroidy();
  EXPECT_EQ(3, centra.size());
  EXPECT_THAT(zbior, testing::Contains(centra[0]));
  EXPECT_THAT(zbior, testing::Contains(centra[1]));
  EXPECT_THAT(zbior, testing::Contains(centra[2]));

  for(unsigned int i=0;i<centra.size();i++)
  {
    spdlog::debug("Pierwszy element dla centrum i: {} to {}", i, centra[i][0]);
    if (centra[i][0]==1)
      EXPECT_THAT((std::vector<std::vector<int>>{zbior[0], zbior[1]}), testing::Contains(centra[i]));
    else if (centra[i][0]==10)
      EXPECT_EQ(zbior[2], centra[i]);
    else
      EXPECT_EQ(zbior[5], centra[i]);
  }
}
