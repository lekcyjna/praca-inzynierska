#include "src/grupowanie/SoftDBAKmeans.tcc"
#include "src/miary/ZmienneFunkcjeDTW.tpp"
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <fenv.h>
int _feenableexcept_status = feenableexcept(FE_INVALID|FE_DIVBYZERO|FE_OVERFLOW);

TEST(SoftDBAKmeansTest, 01stalaDlugosc)
{
  std::vector<adept::Matrix> zbior = {
    {{1},{1},{1}}, {{1},{1},{2}}, {{10},{10},{10}}
  };
  std::mt19937 gen;
  gen.seed(14);

  SoftDBAKmeans<miary::EuklidesProstyBezWiezowMacierzy> model{1, 2, gen, 20};
  model.ustawMinimalnaLiczbeElementowWGrupie(1);
  model.ustawCentroidy({
      {{1},{1},{1}},
      {{1},{1},{2}}
      });

  model.fit(zbior);
  auto centra = model.zwrocCentroidy();
  EXPECT_EQ(2, centra.size());
//  std::cerr<<centra[0]<<"\n";
//  std::cerr<<centra[1]<<"\n";
  if(centra[0](0,0)<centra[1](0,0))
  {
    for (int i=0;i<3;i++)
    {
      EXPECT_LE(centra[0](i,0),2);
      EXPECT_GE(centra[1](i,0),9);
    }
  }
  else
  {
    for (int i=0;i<3;i++)
    {
      EXPECT_LE(centra[1](i,0),2);
      EXPECT_GE(centra[0](i,0),9);
    }
  }
}

TEST(SoftDBAKmeansTest, 02rozneDlugosci)
{
  std::vector<adept::Matrix> zbior = {
    {{1},{1},{1},{1}}, {{1},{1},{2}}, {{10},{10},{10}}, {{10},{11},{10},{10}}, {{10},{7}}, {{100},{100},{100},{100}}
  };
  std::mt19937 gen;
  gen.seed(14);

  SoftDBAKmeans<miary::EuklidesProstyBezWiezowMacierzy> model{1, 3, gen, 20};
  model.ustawMinimalnaLiczbeElementowWGrupie(1);
  model.ustawCentroidy({
      {{1},{1}},
      {{10},{1},{1}},
      {{10},{1},{20},{10}}
      });

  model.fit(zbior);
  auto centra = model.zwrocCentroidy();
  auto przyn = model.predict(zbior);
  for (const auto i : przyn)
    std::cerr<<i<<" ";
  std::cerr<<"\n";
  for (int i=0;i<3;i++)
    std::cerr<<centra[i]<<"\n";
  EXPECT_EQ(3, centra.size());
  for (int i=0;i<2;i++)
    EXPECT_LE(centra[0](i,0),2);
  for (int i=0;i<3;i++)
  {
    EXPECT_GE(centra[1](i,0),8);
    EXPECT_LE(centra[1](i,0),11);
  }
  for (int i=0;i<4;i++)
    EXPECT_GE(centra[2](i,0),98);
}


TEST(SoftDBAKmeansTest, 03normalizacjaPrzezDlugoscCentrum)
{
  std::vector<adept::Matrix> zbior = {
    {{1},{1},{1},{1}}, {{1},{1},{2}}, {{10},{10},{10}}, {{10},{11},{10},{10}}, {{10},{7}}, {{100},{100},{100},{100}}
  };
  std::mt19937 gen;
  gen.seed(14);

  SoftDBAKmeans<miary::EuklidesProstyBezWiezowMacierzy> model{1, 3, gen, 20};
  model.ustawTypNormalizacji((decltype(model)::TypNormalizacji)1);
  model.ustawMinimalnaLiczbeElementowWGrupie(1);
  model.ustawCentroidy({
      {{1},{1}},
      {{10},{1},{1}},
      {{10},{1},{20},{10}}
      });

  model.fit(zbior);
  auto centra = model.zwrocCentroidy();
  auto przyn = model.predict(zbior);
  for (const auto i : przyn)
    std::cerr<<i<<" ";
  std::cerr<<"\n";
  for (int i=0;i<3;i++)
    std::cerr<<centra[i]<<"\n";
  EXPECT_EQ(3, centra.size());
  for (int i=0;i<2;i++)
    EXPECT_LE(centra[0](i,0),2);
  for (int i=0;i<3;i++)
  {
    EXPECT_GE(centra[1](i,0),8);
    EXPECT_LE(centra[1](i,0),11);
  }
  for (int i=0;i<4;i++)
    EXPECT_GE(centra[2](i,0),98);
}



TEST(SoftDBAKmeansTest, 04rozneDlugosciExpDiv)
{
  std::vector<adept::Matrix> zbior = {
    {{1},{1},{1},{1}}, {{1},{1},{2}}, {{10},{10},{10}}, {{10},{11},{10},{10}}, {{10},{7}}, {{100},{100},{100},{100}}
  };
  std::mt19937 gen;
  gen.seed(14);

  SoftDBAKmeans<miary::EuklidesProstyBezWiezowMacierzy, PSmoothMinType::ExpDiv> model{1, 3, gen, 20};
  model.ustawMinimalnaLiczbeElementowWGrupie(1);
  model.ustawCentroidy({
      {{1},{1}},
      {{10},{1},{1}},
      {{10},{1},{20},{10}}
      });

  model.fit(zbior);
  auto centra = model.zwrocCentroidy();
  auto przyn = model.predict(zbior);
  for (const auto i : przyn)
    std::cerr<<i<<" ";
  std::cerr<<"\n";
  for (int i=0;i<3;i++)
    std::cerr<<centra[i]<<"\n";
  EXPECT_EQ(3, centra.size());
  for (int i=0;i<2;i++)
    EXPECT_LE(centra[0](i,0),2);
  for (int i=0;i<3;i++)
  {
    EXPECT_GE(centra[1](i,0),8);
    EXPECT_LE(centra[1](i,0),11);
  }
  for (int i=0;i<4;i++)
    EXPECT_GE(centra[2](i,0),98);
}

