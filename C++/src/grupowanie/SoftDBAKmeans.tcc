#pragma once
#include "SoftDBAKmeans.hpp"
#include "src/miary/ZmienneFunkcjeDTW.tpp"
#include <adept_arrays.h>
#include <adept_optimize.h>
#include <set>
#include <unordered_set>
#include "OptymalizatorUniwersalny.tcc"
#include "OptymalizatorEuklidesa.tcc"

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
SoftDBAKmeans<F, pSmoothMinType>::SoftDBAKmeans(double gamma, int liczbaGrup, std::mt19937& rnd, int iteracje)
  : liczbaGrup(liczbaGrup)
  , liczbaIteracji(iteracje)
  , liczbaReinicjalizacji(2)
  , softDtw(F<adept::Vector>(), gamma)
  , aSoftDtw(F<adept::aVector>(), gamma)
  , rnd(rnd)
  , mTypNormalizacji(TypNormalizacji::BRAK)
{ }

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::ustawTypNormalizacji(TypNormalizacji normal)
{
  mTypNormalizacji=normal;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::ustawLiczbeReinicjalizacji(int liczba)
{
  liczbaReinicjalizacji=liczba;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::ustawMinimalnaLiczbeElementowWGrupie(uint32_t liczba)
{
  minimalnaLiczbaElementowWGrupie=liczba;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::ustawLiczbeIterNaWczesnymEtapie(uint32_t liczba)
{
  liczbaIteracjiNaWstepnymEtapie=liczba;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::ustawMaksymalnaLiczbeLosowanReinicijalizacji(uint32_t liczba)
{
  maksymalnaLiczbaLosowanReinicjalizacji=liczba;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
std::vector<adept::Matrix> SoftDBAKmeans<F, pSmoothMinType>::zwrocCentroidy() const
{
  std::vector<adept::Matrix> vec;
  vec.reserve(centroidy.size());
  for (const auto& centrum : centroidy)
  {
    adept::Matrix M(centrum.dimensions());
    M=centrum;
    vec.push_back(M);
  }
  return vec;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::ustawCentroidy(std::vector<adept::Matrix> noweCentroidy)
{
  centroidy=std::move(noweCentroidy);
  czyCentroidyZainicjalizowane=true;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::inicjalizujCentroidyLosowo(const std::vector<adept::Matrix>& zbior)
{
  centroidy.clear();
  centroidy.reserve(liczbaGrup);
  std::uniform_int_distribution<unsigned long> U{0,zbior.size()-1};
  for (int i=0;i<liczbaGrup;i++)
  {
    int idx=U(rnd);
    adept::Matrix centrum (zbior[idx].dimensions());
    centrum=zbior[idx];
    centroidy.push_back(centrum);
  }
  czyCentroidyZainicjalizowane=true;
  return;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::wyliczMacierzOdl(const std::vector<adept::Matrix>& zbior, adept::Matrix& odl) const
{
  return wyliczMacierzOdl<adept::Matrix>(zbior, odl, centroidy, softDtw);
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::wyliczMacierzOdl(const std::vector<adept::Matrix>& zbior, adept::Matrix& odl, const std::vector<adept::Matrix>& centra) const
{
  return wyliczMacierzOdl<adept::Matrix>(zbior, odl, centra, softDtw);
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::wyliczMacierzOdl(const std::vector<adept::Matrix>& zbior, adept::aMatrix& odl, std::vector<adept::aMatrix>& centroidyA) const
{
  return wyliczMacierzOdl<adept::aMatrix>(zbior, odl, centroidyA, aSoftDtw);
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
template<typename T>
  requires requires{ std::same_as<T,adept::Matrix> or std::same_as<T, adept::aMatrix>;}
void SoftDBAKmeans<F, pSmoothMinType>::wyliczMacierzOdl(const std::vector<adept::Matrix>& zbior, T& odl, const std::vector<T>& centroidyA,
        const SoftDTW<T,F<std::conditional_t<std::is_same_v<T, adept::Matrix>, adept::Vector, adept::aVector>>, pSmoothMinType>& softDtw) const
{
  if (odl.dimension(0)!=(int)zbior.size() or odl.dimension(1)!=liczbaGrup)
    throw std::runtime_error("Błędna wymiarowość macierzy odległości.");
  for(unsigned int i=0;i<zbior.size();i++)
  {
    for(int j=0;j<liczbaGrup;j++)
    {
      auto koszt = softDtw(centroidyA[j], zbior[i]);
      switch (mTypNormalizacji)
      {
        case TypNormalizacji::BRAK:
          odl(i,j)=koszt;
          break;
        case TypNormalizacji::PRZEZ_DL_CENTRUM:
          odl(i,j)=koszt/centroidyA[j].dimension(0);
          break;
        default:
          throw std::runtime_error("Nieznany typ normalizacji: "+std::to_string((int)mTypNormalizacji));
      }
    }
  }
  return;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
template<typename T>
  requires requires{ std::same_as<T,adept::Matrix> or std::same_as<T, adept::aMatrix>;}
void SoftDBAKmeans<F, pSmoothMinType>::wyliczWektorOdl(const adept::Matrix& ciag,
    std::conditional_t<std::is_same_v<T, adept::Matrix>, adept::Vector, adept::aVector>& odl,
    const std::vector<T>& centroidyA,
    const SoftDTW<T,F<std::conditional_t<std::is_same_v<T, adept::Matrix>, adept::Vector, adept::aVector>>, pSmoothMinType>& softDtw) const
{
  if (odl.size()!=liczbaGrup)
    throw std::runtime_error("Błędna wymiarowość wektora odległości.");
  for(int j=0;j<liczbaGrup;j++)
  {
    auto koszt = softDtw(centroidyA[j], ciag);
    switch (mTypNormalizacji)
    {
      case TypNormalizacji::BRAK:
        odl(j)=koszt;
        break;
      case TypNormalizacji::PRZEZ_DL_CENTRUM:
        odl(j)=koszt/centroidyA[j].dimension(0);
        break;
      default:
        throw std::runtime_error("Nieznany typ normalizacji: "+std::to_string((int)mTypNormalizacji));
    }
  }
  return;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
template<typename T>
  requires requires{ std::same_as<T,adept::Matrix> or std::same_as<T, adept::aMatrix>;}
void SoftDBAKmeans<F, pSmoothMinType>::wyliczWektorOdlIMacierzeKosztu(const adept::Matrix& ciag,
    std::conditional_t<std::is_same_v<T, adept::Matrix>, adept::Vector, adept::aVector>& odl,
    std::vector<T>& macierzeKosztu,
    const std::vector<T>& centroidyA,
    const SoftDTW<T,F<std::conditional_t<std::is_same_v<T, adept::Matrix>, adept::Vector, adept::aVector>>, pSmoothMinType>& softDtw) const
{
  if (odl.size()!=liczbaGrup)
    throw std::runtime_error("Błędna wymiarowość wektora odległości.");
  macierzeKosztu.resize(0);
  macierzeKosztu.reserve(liczbaGrup);
  for(int j=0;j<liczbaGrup;j++)
  {
    T koszty = softDtw.wyliczMacierzDynamiczna(centroidyA[j], ciag);
    switch (mTypNormalizacji)
    {
      case TypNormalizacji::BRAK:
        odl(j)=koszty(adept::end, adept::end);
        macierzeKosztu.push_back(koszty);
        break;
      case TypNormalizacji::PRZEZ_DL_CENTRUM:
        macierzeKosztu.push_back(koszty);
        odl(j)=koszty(adept::end, adept::end)/centroidyA[j].dimension(0);
        break;
      default:
        throw std::runtime_error("Nieznany typ normalizacji: "+std::to_string((int)mTypNormalizacji));
    }
  }
  return;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
std::vector<short> SoftDBAKmeans<F, pSmoothMinType>::predict(const std::vector<adept::Matrix>& zbior) const
{
  return predict(zbior, centroidy);
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
std::vector<short> SoftDBAKmeans<F, pSmoothMinType>::predict(const std::vector<adept::Matrix>& zbior, const std::vector<adept::Matrix>& centra) const
{
  adept::Matrix odl(zbior.size(), liczbaGrup);
  wyliczMacierzOdl(zbior, odl, centra);

  std::vector<short> indeksy;
  indeksy.reserve(zbior.size());
  for(unsigned int i=0;i<zbior.size();i++)
  {
    indeksy.push_back(adept::minloc(odl[i]));
  }
  return indeksy;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
bool SoftDBAKmeans<F, pSmoothMinType>::reicijalizujCentroidyJesliTrzeba(const std::vector<adept::Matrix>& zbior)
{
  std::uniform_int_distribution<int> U(0,zbior.size()-1);

  bool czyPotrzebaBylo=false;

  auto wynikPredykcji = predict(zbior);
  std::multiset<short> indeksy(wynikPredykcji.begin(), wynikPredykcji.end());
  for (short grupa = 0; grupa<liczbaGrup;grupa++)
  {
    uint32_t licznikProb=0;
    while(indeksy.count(grupa)<minimalnaLiczbaElementowWGrupie)
    {
      licznikProb+=1;
      int indeksCiagu = U(rnd);
      centroidy[grupa].clear();
      centroidy[grupa]=zbior[indeksCiagu];
      czyPotrzebaBylo=true;
      wynikPredykcji = predict(zbior);
      indeksy.clear();
      indeksy.insert(wynikPredykcji.begin(), wynikPredykcji.end());
      if(licznikProb>=maksymalnaLiczbaLosowanReinicjalizacji)
        break;
    }
  }
  return czyPotrzebaBylo;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::wstepnaInicjalizacjaGrup(const std::vector<adept::Matrix>& zbior)
{
  for(int i=0;i<liczbaReinicjalizacji;i++)
  {
    std::cerr<<"Wstępna inicjalizacja. Iteracja: "<<i<<"\n";
    adept::Minimizer minimizer(algorytmOptymalizacji);

    minimizer.set_max_iterations(liczbaIteracjiNaWstepnymEtapie);

    //OptymalizatorUniwersalny optymInst(centroidy, zbior, this);
    OptymalizatorEuklidesa optymInst(centroidy, zbior, this);
    adept::Vector argumentyDoOptymalizacji(optymInst.zwrocRozmiarAdeptWektora());
    optymInst.centroidyDoAdeptWektora(centroidy, argumentyDoOptymalizacji);

    adept::MinimizerStatus minimizerStatus;

    minimizerStatus = minimizer.minimize(optymInst, argumentyDoOptymalizacji);
    std::cerr<<"DEBUG: Wynik minimalizacji: "<<adept::minimizer_status_string(minimizerStatus)<<" iter "<<minimizer.n_iterations()<<"\n";
    if (minimizerStatus!=adept::MINIMIZER_STATUS_SUCCESS)
      std::cerr<<"Wynik minimalizacji: "<<adept::minimizer_status_string(minimizerStatus)<<" iter "<<minimizer.n_iterations()<<"\n";

    centroidy=optymInst. template adeptWektorDoCentroidow<adept::Matrix>(argumentyDoOptymalizacji);
    if(not reicijalizujCentroidyJesliTrzeba(zbior))
      break;
  }
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::fit(const std::vector<adept::Matrix>& zbior)
{
  if (not czyCentroidyZainicjalizowane)
    inicjalizujCentroidyLosowo(zbior);

  wstepnaInicjalizacjaGrup(zbior);

  adept::Minimizer minimizer(algorytmOptymalizacji);
  minimizer.set_max_iterations(liczbaIteracji);

  //OptymalizatorUniwersalny optymInst(centroidy, zbior, this);
  OptymalizatorEuklidesa optymInst(centroidy, zbior, this);
  adept::Vector argumentyDoOptymalizacji(optymInst.zwrocRozmiarAdeptWektora());
  optymInst.centroidyDoAdeptWektora(centroidy, argumentyDoOptymalizacji);

  adept::MinimizerStatus minimizerStatus;

  minimizerStatus = minimizer.minimize(optymInst, argumentyDoOptymalizacji);
  std::cerr<<"DEBUG: Wynik minimalizacji: "<<adept::minimizer_status_string(minimizerStatus)<<" iter "<<minimizer.n_iterations()<<"\n";
  if (minimizerStatus!=adept::MINIMIZER_STATUS_SUCCESS)
    std::cerr<<"Wynik minimalizacji: "<<adept::minimizer_status_string(minimizerStatus)<<" iter "<<minimizer.n_iterations()<<"\n";

  centroidy=optymInst. template adeptWektorDoCentroidow<adept::Matrix>(argumentyDoOptymalizacji);
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType >
double SoftDBAKmeans<F, pSmoothMinType>::zwrocKoszt(const std::vector<adept::Matrix>& zbior) const
{
  //OptymalizatorUniwersalny optymInst(centroidy, zbior, this);
  OptymalizatorEuklidesa optymInst(centroidy, zbior, this);
  adept::Vector argumenty(optymInst.zwrocRozmiarAdeptWektora());
  optymInst.centroidyDoAdeptWektora(centroidy, argumenty);
  return optymInst.calc_cost_function(argumenty);
}
