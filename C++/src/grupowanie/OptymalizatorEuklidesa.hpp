#pragma once
#include <random>
#include <ranges>
#include "src/utils/wiezy.hpp"
#include <adept.h>
#include <adept_arrays.h>
#include <type_traits>
#include "src/miary/SoftDTW.tpp"
#include "IOptymalizator.tcc"

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
class SoftDBAKmeans<F, pSmoothMinType>::OptymalizatorEuklidesa : public SoftDBAKmeans<F, pSmoothMinType>::IOptymalizator
{
  public:
    OptymalizatorEuklidesa (const std::vector<adept::Matrix>& centroidy, const std::vector<adept::Matrix>& zbior,
        const SoftDBAKmeans<F, pSmoothMinType>* klasaNadrzedna);

    virtual adept::Real calc_cost_function_gradient(const adept::Vector& x, adept::Vector gradient) override;
  private:
    adept::Matrix obliczGradientCentroidu(const adept::Matrix& pochodne, 
        const adept::Matrix& centroid, const adept::Matrix ciag) const;
    void sprawdzCzyZapadlySieGrupy( const std::vector<adept::Matrix>& centroidy);
};
