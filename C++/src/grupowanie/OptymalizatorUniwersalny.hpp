#pragma once
#include <random>
#include <ranges>
#include "src/utils/wiezy.hpp"
#include <adept.h>
#include <adept_arrays.h>
#include <type_traits>
#include "src/miary/SoftDTW.tpp"
#include "IOptymalizator.tcc"

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
class SoftDBAKmeans<F, pSmoothMinType>::OptymalizatorUniwersalny : public SoftDBAKmeans<F, pSmoothMinType>::IOptymalizator
{
  public:
    OptymalizatorUniwersalny (const std::vector<adept::Matrix>& centroidy, const std::vector<adept::Matrix>& zbior,
        const SoftDBAKmeans<F, pSmoothMinType>* klasaNadrzedna);

    virtual adept::Real calc_cost_function_gradient(const adept::Vector& x, adept::Vector gradient) override;

  private:
    adept::Stack astack;
};
