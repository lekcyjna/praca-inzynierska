/// Założenia:
/// - odległości od szeregów są normalizowane przez długość wzorca (ponieważ dla danego szeregu 
///   jego długość jest stała, a my chcemy później dla danego szeregu znaleźć najbliższy mu wzorzec)
/// - długości wzorców losujemy z danych, losując po prostu początkowe wzorce
//
#pragma once
#include <random>
#include <ranges>
#include "src/utils/wiezy.hpp"
#include <adept.h>
#include <adept_arrays.h>
#include <type_traits>
#include "src/miary/SoftDTW.tpp"
#include <adept_optimize.h>

class ZapadniecieGrup : public std::exception
{ };

template<template <typename> typename F, PSmoothMinType pSmoothMinType = PSmoothMinType::LSE>
class SoftDBAKmeans {
  public:
    enum class TypNormalizacji : uint8_t
    {
      BRAK = 0,
      PRZEZ_DL_CENTRUM
    };

    SoftDBAKmeans(double gamma, int liczbaGrup, std::mt19937& rnd, int iter=20);

    void fit(const std::vector<adept::Matrix>&);
    std::vector<short> predict(const std::vector<adept::Matrix>&) const;
    std::vector<adept::Matrix> zwrocCentroidy() const;
    void ustawCentroidy(std::vector<adept::Matrix>);
    void ustawTypNormalizacji(TypNormalizacji);
    void ustawLiczbeReinicjalizacji(int);
    void ustawMinimalnaLiczbeElementowWGrupie(uint32_t);
    void ustawLiczbeIterNaWczesnymEtapie(uint32_t);
    void ustawMaksymalnaLiczbeLosowanReinicijalizacji(uint32_t);
    double zwrocKoszt(const std::vector<adept::Matrix>& zbior) const;
  
  protected:
    std::vector<short> predict(const std::vector<adept::Matrix>&, const std::vector<adept::Matrix>&) const;

    void inicjalizujCentroidyLosowo(const std::vector<adept::Matrix>&);
    void wyliczMacierzOdl(const std::vector<adept::Matrix>&, adept::Matrix& odl) const;
    void wyliczMacierzOdl(const std::vector<adept::Matrix>& zbior, adept::Matrix& odl, const std::vector<adept::Matrix>& centra) const;
    void wyliczMacierzOdl(const std::vector<adept::Matrix>&, adept::aMatrix& odl, std::vector<adept::aMatrix>&) const;

    template<typename T>
      requires requires{ std::same_as<T,adept::Matrix> or std::same_as<T, adept::aMatrix>;}
    void wyliczMacierzOdl(const std::vector<adept::Matrix>& zbior, T& odl, const std::vector<T>&,
        const SoftDTW<T,F<std::conditional_t<std::is_same_v<T, adept::Matrix>, adept::Vector, adept::aVector>>, pSmoothMinType>& softDtw) const;
    template<typename T>
      requires requires{ std::same_as<T,adept::Matrix> or std::same_as<T, adept::aMatrix>;}
    void wyliczWektorOdl(const adept::Matrix& ciag,
        std::conditional_t<std::is_same_v<T, adept::Matrix>, adept::Vector, adept::aVector>& odl,
        const std::vector<T>& centroidyA,
        const SoftDTW<T,F<std::conditional_t<std::is_same_v<T, adept::Matrix>, adept::Vector, adept::aVector>>, pSmoothMinType>& softDtw) const;
    template<typename T>
      requires requires{ std::same_as<T,adept::Matrix> or std::same_as<T, adept::aMatrix>;}
    void wyliczWektorOdlIMacierzeKosztu(const adept::Matrix& ciag,
        std::conditional_t<std::is_same_v<T, adept::Matrix>, adept::Vector, adept::aVector>& odl,
        std::vector<T>& macierzeKosztu,
        const std::vector<T>& centroidyA,
        const SoftDTW<T,F<std::conditional_t<std::is_same_v<T, adept::Matrix>, adept::Vector, adept::aVector>>, pSmoothMinType>& softDtw) const;

    bool reicijalizujCentroidyJesliTrzeba(const std::vector<adept::Matrix>& zbior);
    void wstepnaInicjalizacjaGrup(const std::vector<adept::Matrix>& zbior);

    const int liczbaGrup;
    int liczbaIteracji;
    int liczbaReinicjalizacji;
    uint32_t minimalnaLiczbaElementowWGrupie = 10;
    uint32_t liczbaIteracjiNaWstepnymEtapie=2;
    uint32_t maksymalnaLiczbaLosowanReinicjalizacji=500;
    SoftDTW<adept::Matrix, F<adept::Vector>, pSmoothMinType> softDtw;
    SoftDTW<adept::aMatrix, F<adept::aVector>, pSmoothMinType> aSoftDtw;

    std::vector<adept::Matrix> centroidy;

    std::mt19937 rnd;
    bool czyCentroidyZainicjalizowane=false;
    TypNormalizacji mTypNormalizacji;

    static constexpr auto algorytmOptymalizacji = adept::MINIMIZER_ALGORITHM_LIMITED_MEMORY_BFGS;

    class IOptymalizator;
    class OptymalizatorUniwersalny;
    class OptymalizatorEuklidesa;
};
