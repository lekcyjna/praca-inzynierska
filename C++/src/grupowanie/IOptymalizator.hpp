#pragma once
#include <random>
#include <ranges>
#include "src/utils/wiezy.hpp"
#include <adept.h>
#include <adept_arrays.h>
#include <type_traits>
#include "src/miary/SoftDTW.tpp"
#include <adept_optimize.h>
#include "SoftDBAKmeans.hpp"

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
class SoftDBAKmeans<F, pSmoothMinType>::IOptymalizator : public adept::Optimizable
{
  public:
    IOptymalizator(const std::vector<adept::Matrix>& centroidy, const std::vector<adept::Matrix>& zbior,
        const SoftDBAKmeans<F, pSmoothMinType>* klasaNadrzedna);

    virtual adept::Real calc_cost_function(const adept::Vector& x) override;
    virtual bool provides_derivative(int order) override
    { return (order >= 0 && order <= 1); }
    virtual void report_progress(int niter, const adept::Vector& x, adept::Real cost, adept::Real gnorm) override;

    template<typename T>
      requires requires{ std::same_as<T,adept::Matrix> or std::same_as<T, adept::aMatrix>;}
    void centroidyDoAdeptWektora(const std::vector<T>& centroidy, adept::Vector& v) const;

    template<typename T>
      requires requires{ std::same_as<T,adept::Matrix> or std::same_as<T, adept::aMatrix>;}
    std::vector<T> adeptWektorDoCentroidow(const adept::Vector& v) const;

    uint32_t zwrocRozmiarAdeptWektora() const {return rozmiarAdeptWektora;}
    void normalizujOdl(adept::Vector& odl, const std::vector<adept::Matrix>& centroidy) const;

  protected:
    const SoftDBAKmeans<F, pSmoothMinType>* klasaNadrzedna;
    uint32_t rozmiarAdeptWektora;
    std::vector<std::tuple<uint32_t,uint32_t>> rozmiaryCentroidow;
    const std::vector<adept::Matrix>& zbior;
};
