#pragma once
#include "IOptymalizator.hpp"

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
SoftDBAKmeans<F, pSmoothMinType>::IOptymalizator::IOptymalizator(const std::vector<adept::Matrix>& centroidy, const std::vector<adept::Matrix>& zbior,
    const SoftDBAKmeans<F, pSmoothMinType>* klasaNadrzedna)
  : klasaNadrzedna(klasaNadrzedna)
  , zbior(zbior)
{
  rozmiaryCentroidow.reserve(centroidy.size());
  rozmiarAdeptWektora=0;
  for(const auto & c : centroidy)
  {
    rozmiarAdeptWektora+=c.size();
    rozmiaryCentroidow.push_back(std::make_tuple(c.dimension(0), c.dimension(1)));
  }
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
template<typename T>
  requires requires{ std::same_as<T,adept::Matrix> or std::same_as<T, adept::aMatrix>;}
void SoftDBAKmeans<F, pSmoothMinType>::IOptymalizator::centroidyDoAdeptWektora(const std::vector<T>& centroidy, adept::Vector& v) const
{
  int licznik=0;
  uint32_t liczbaGrup=centroidy.size();
  for(unsigned int i=0;i<liczbaGrup;i++)
  {
    const auto& centrum=centroidy[i];
    unsigned int d0=centrum.dimension(0);
    unsigned int d1=centrum.dimension(1);
    for(unsigned int j=0;j<d0;j++)
    {
      for(unsigned int k=0;k<d1;k++)
      {
        v[licznik]=centrum(j,k);
        licznik++;
      }
    }
  }
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
template<typename T>
  requires requires{ std::same_as<T,adept::Matrix> or std::same_as<T, adept::aMatrix>;}
std::vector<T> SoftDBAKmeans<F, pSmoothMinType>::IOptymalizator::adeptWektorDoCentroidow(const adept::Vector& v) const
{
  std::vector<T> centroidy;
  centroidy.reserve(rozmiaryCentroidow.size());
  int licznik=0;
  uint32_t liczbaGrup=rozmiaryCentroidow.size();
  for(unsigned int i=0;i<liczbaGrup;i++)
  {
    auto [d0,d1]=rozmiaryCentroidow[i];
    T centrum(d0,d1);
    for(unsigned int j=0;j<d0;j++)
    {
      for(unsigned int k=0;k<d1;k++)
      {
        centrum(j,k)=v[licznik];
        licznik++;
      }
    }
    centroidy.push_back(std::move(centrum));
  }
  return centroidy;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::IOptymalizator::normalizujOdl(adept::Vector& odl, const std::vector<adept::Matrix>& centroidy) const
{
    switch (klasaNadrzedna->mTypNormalizacji)
    {
      case TypNormalizacji::BRAK:
        break;
      case TypNormalizacji::PRZEZ_DL_CENTRUM:
        for (unsigned int i=0;i<centroidy.size();i++)
        {
          odl(i)/=centroidy[i].dimension(0);
        }
        break;
      default:
        throw std::runtime_error("Nieznany typ normalizacji: "+std::to_string((int)klasaNadrzedna->mTypNormalizacji));
    }
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
adept::Real SoftDBAKmeans<F, pSmoothMinType>::IOptymalizator::calc_cost_function(const adept::Vector& v)
{
  std::vector<adept::Matrix> centroidy;
  centroidy=adeptWektorDoCentroidow<adept::Matrix>(v);

  adept::Matrix odl(zbior.size(), rozmiaryCentroidow.size());
  klasaNadrzedna->wyliczMacierzOdl(zbior, odl);

  adept::Vector najOdl=adept::minval(odl, 1);
  double wynik=adept::sum(najOdl);

  return wynik;
}


template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::IOptymalizator::report_progress(int niter, const adept::Vector&,
adept::Real cost, adept::Real) 
{
  std::cerr << "Iteration " << niter << ": cost function = " << cost << " liczba grup: " << klasaNadrzedna->liczbaGrup <<"\n";
}
