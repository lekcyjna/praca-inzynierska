#pragma once
#include "OptymalizatorUniwersalny.hpp"

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
SoftDBAKmeans<F, pSmoothMinType>::OptymalizatorUniwersalny::OptymalizatorUniwersalny (
    const std::vector<adept::Matrix>& centroidy, const std::vector<adept::Matrix>& zbior,
    const SoftDBAKmeans<F, pSmoothMinType>* klasaNadrzedna)
  : SoftDBAKmeans<F, pSmoothMinType>::IOptymalizator(centroidy, zbior, klasaNadrzedna)
{}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
adept::Real SoftDBAKmeans<F, pSmoothMinType>::OptymalizatorUniwersalny::calc_cost_function_gradient(const adept::Vector& v, adept::Vector gradient)
{
  std::vector<adept::aMatrix> centroidy;
  centroidy=this-> template adeptWektorDoCentroidow<adept::aMatrix>(v);

  std::vector<adept::Matrix> wektorGradientow;
  wektorGradientow.reserve(centroidy.size());

  for(const auto & c : centroidy)
  {
    adept::Matrix m(c.dimensions());
    m=0;
    wektorGradientow.push_back(m);
  }

  double wynikSumaryczny=0;
  for (const auto& ciag : this->zbior)
  {
    //ZZamiana w tym miejscu na sumę gradientów znacżaco wydłużyła czas zbiegania i trochę zmieniła wyniki
    astack.new_recording();
    adept::aVector odl(this->rozmiaryCentroidow.size());
    this->klasaNadrzedna-> template wyliczWektorOdl<adept::aMatrix>(ciag, odl, centroidy, this->klasaNadrzedna->aSoftDtw);

//    std::cerr<<odl<<"\n";

    //TODO poradzić sobie ze znikającymi grupami:
    // ? dodać reinicjalizację
    // ? dodać fuzzy (pamiętając o tym, że SoftDRW może mieć wartość ujemną)
    //this->normalizujOdl(odl, centroidy);
    adept::adouble najOdl=adept::minval(odl);
    adept::adouble wynik=najOdl;
    wynikSumaryczny+=adept::value(wynik);

    wynik.set_gradient(1.0);

    astack.reverse();

    for (unsigned int i=0;i<centroidy.size();i++)
    {
      wektorGradientow[i]+=centroidy[i].get_gradient();
    }
  }
  this->centroidyDoAdeptWektora(wektorGradientow, gradient);
  return adept::value(wynikSumaryczny);
}
