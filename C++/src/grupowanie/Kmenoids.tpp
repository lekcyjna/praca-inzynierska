#pragma once
#include "Kmenoids.hpp"
#include "spdlog/spdlog.h"


template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
Kmenoids<T>::Kmenoids(
    std::function<double(const typename T::value_type&, const typename T::value_type&)> funkcja,
    std::mt19937& gen, int nGrup,
    int n_iteracji)
  : funkcjaOdleglosci{funkcja}
  , liczbaGrup{nGrup}
  , n_iter{n_iteracji}
  , fitted{false}
  , centroidyZainicjalizowane{false}
  , wyliczonoOdleglosci{false}
  , randomGenerator{gen}
{} 


template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
std::vector<typename Kmenoids<T>::CiagZPozycja> Kmenoids<T>::inicjalizujCentroidyLosowo(const T& zbior) const
{
  std::vector<CiagZPozycja> stworzoneCentroidy;
  stworzoneCentroidy.reserve(liczbaGrup);
  std::uniform_int_distribution<int> U(0, zbior.size()-1);
  for (int i=0;i<liczbaGrup;i++)
  {
    int indeks = U(randomGenerator);
    spdlog::debug("{} wylosowany indeks: {}", __func__, indeks);
    stworzoneCentroidy.push_back(std::make_tuple(zbior[indeks], indeks));
    spdlog::info("Zainicjalizowane centrum {} {} {} {} {}", zbior[indeks][0], zbior[indeks][1], zbior[indeks][2], zbior[indeks][3], zbior[indeks][4]); 
  }
  spdlog::info("{} zainicjalizowano centroidy", __func__);
  return stworzoneCentroidy;
}

template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
int Kmenoids<T>::wyznaczCentroid(
    const std::vector<std::vector<float>>& przynaleznosc, int numerGrupy) const
{
  std::uint32_t liczbaCiagow=przynaleznosc.size();

  double najlepszaOdl=INF;
  int lokalnyNajIndeks=-1;

  for (std::uint32_t indTeoretycznegoCentroidu=0;indTeoretycznegoCentroidu<liczbaCiagow;indTeoretycznegoCentroidu++)
  {
    double odl=0;
    for(std::uint32_t j=0; j<liczbaCiagow; j++)
    {
      odl+=macierzOdleglosci[indTeoretycznegoCentroidu][j]*przynaleznosc[j][numerGrupy];
    }
    if(odl<najlepszaOdl)
    {
      najlepszaOdl=odl;
      lokalnyNajIndeks=indTeoretycznegoCentroidu;
    }
  }
  return lokalnyNajIndeks;
}

template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
std::vector<typename Kmenoids<T>::CiagZPozycja> Kmenoids<T>::wyznaczNoweCentroidy(const T& zbior,
    const std::vector<std::vector<float>>& przynaleznosc) const
{
  spdlog::info("==========================");
  std::vector<CiagZPozycja> noweCentra;
  noweCentra.reserve(liczbaGrup);
  for (int i=0;i<liczbaGrup; i++)
  {
    auto indeksNowegoCentroidu=wyznaczCentroid(przynaleznosc,i);
    noweCentra.emplace_back(std::make_tuple(zbior[indeksNowegoCentroidu], indeksNowegoCentroidu));
    auto [ciag, I] {noweCentra.back()};
    spdlog::info("Centrum {} {} {} {} {}", ciag[0], ciag[1], ciag[2], ciag[3], ciag[4]);
  }
  return noweCentra;
}


template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
void Kmenoids<T>::fit(const T& zbior) 
{
  if(not wyliczonoOdleglosci)
  {
    FabrykaMacierzOdleglosci<T> fabryka{funkcjaOdleglosci};
    macierzOdleglosci=fabryka(zbior);
    wyliczonoOdleglosci=true;
  }
  if (not centroidyZainicjalizowane)
  {
    centroidy = inicjalizujCentroidyLosowo(zbior);
    centroidyZainicjalizowane=true;
  }

  fitted=true;

  for(int iter=0;iter<n_iter;iter++)
  {
    std::vector<std::vector<float>> przynaleznosc = predict(zbior);
    centroidy = wyznaczNoweCentroidy(zbior,przynaleznosc);
  }
}

template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
std::vector<float> Kmenoids<T>::przynaleznoscCiagu(const int elementInd,
   const std::vector<int>& indeksyCentroidow) const
{
  std::vector<float> przynaleznosc;
  przynaleznosc.resize(liczbaGrup, 0);

  double odl=INF;
  int najblizszaGrupa=-1;

  for (int grupa=0;grupa<liczbaGrup;grupa++)
  {
    int indeksCentroidu=indeksyCentroidow[grupa];
    if(odl>macierzOdleglosci[indeksCentroidu][elementInd])
    {
      odl=macierzOdleglosci[indeksCentroidu][elementInd];
      najblizszaGrupa=grupa;
    }
  }
  przynaleznosc[najblizszaGrupa]=1;
  return przynaleznosc;
}

template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
std::vector<std::vector<float>> Kmenoids<T>::predict(const T& zbior) const
{
  if(not fitted)
  {
    spdlog::error("{} Kmenoids not fitted befor predict.", __func__);
    throw std::runtime_error("Kmenoids not fitted.");
  }
  int rozmiarZbioru = zbior.size();
  std::vector<int> indeksyCentroidow;
  for (const auto& i : centroidy)
  {
    auto [ciag, indeks] {i};
    indeksyCentroidow.push_back(indeks);
  }

  std::vector<std::vector<float>> przewidziane;
  przewidziane.reserve(rozmiarZbioru);

  for (int i=0;i<rozmiarZbioru;i++)
  {
    przewidziane.push_back(przynaleznoscCiagu(i, indeksyCentroidow));
  }
  spdlog::debug("{} zakonczono przewidywanie przynaleznosci do grup.", __func__);
  return przewidziane;
}

template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
std::vector<typename T::value_type> Kmenoids<T>::zwrocCentroidy() const
{
  std::vector<typename T::value_type> kopiaCentroidy;
  kopiaCentroidy.reserve(centroidy.size());
  for (auto centrum : centroidy)
  {
    auto [ciag, indeks] {centrum};
    kopiaCentroidy.emplace_back(ciag);
  }
  return kopiaCentroidy;
}


template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
void Kmenoids<T>::ustawCentroidy(std::vector<Kmenoids<T>::CiagZPozycja> centroidyDoUstawienia)
{
  centroidyZainicjalizowane=true;
  centroidy=centroidyDoUstawienia;
}


template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
MacierzOdleglosci Kmenoids<T>::zwrocMacierzOdleglosci() const
{
  if (not fitted)
    std::runtime_error(std::string(__func__)+"Kmenoids not fitted.");
  return macierzOdleglosci;
}


template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
void Kmenoids<T>::ustawMacierzOdleglosci(MacierzOdleglosci macierz)
{
  macierzOdleglosci=macierz;
  wyliczonoOdleglosci=true;
  return;
}
