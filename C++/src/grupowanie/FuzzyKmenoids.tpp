#pragma once

#include "FuzzyKmenoids.hpp"
#include "spdlog/spdlog.h"

#include <iostream>


template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
FuzzyKmenoids<T>::FuzzyKmenoids(
    std::function<double(const typename T::value_type&, const typename T::value_type&)> funkcja,
    std::mt19937& gen, int nGrup,
    int n_iteracji)
  : Kmenoids<T>{funkcja, gen, nGrup, n_iteracji}
{
  spdlog::info("Initializing FuzzyKmenoids");
} 


template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
std::vector<float> FuzzyKmenoids<T>::przynaleznoscCiagu(const int elementInd,
   const std::vector<int>& indeksyCentroidow) const
{
  std::vector<float> przynaleznosc;
  przynaleznosc.resize(this->liczbaGrup, 0);

  for (int i=0; i <this->liczbaGrup; i++)
  {
    if (indeksyCentroidow[i]==elementInd)
    {
      przynaleznosc[i]=1;
      return przynaleznosc;
    }
  }

  float sumaPrzynaleznosc=0;
  for (int grupa=0;grupa<this->liczbaGrup;grupa++)
  {
    int indeksCentroidu=indeksyCentroidow[grupa];
    float suma=0;
    for (int grupaWewnetrzna=0;grupaWewnetrzna<this->liczbaGrup;grupaWewnetrzna++)
    {
      float tmpVar=this->macierzOdleglosci[indeksCentroidu][elementInd]/this->macierzOdleglosci[indeksyCentroidow[grupaWewnetrzna]][elementInd];
      suma+=tmpVar*tmpVar;
    }
    przynaleznosc[grupa]=1.0/suma;
//    przynaleznosc[grupa]*=przynaleznosc[grupa];
    sumaPrzynaleznosc+=przynaleznosc[grupa];
  }


  for (int grupa=0;grupa<this->liczbaGrup;grupa++)
  {
    przynaleznosc[grupa]/=sumaPrzynaleznosc;
//    std::cerr<<przynaleznosc[grupa]<<"\t";
  }
  //std::cerr<<"\n";

  return przynaleznosc;
}
