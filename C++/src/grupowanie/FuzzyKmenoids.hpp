/// Wymagania odziedziczone:
/// - Przyjmuje zbior ciągów
/// - Grupuje dane
/// - Środkami grup mogą byc tylko istniejące ciągi
/// - Działa iteracyjnie
/// - Powinien obsługiwać różne miary odległości
/// - Powinien być wątko-bezpieczny
/// - Powinien oferować możliwość wielowątkowego liczenia odległości
/// - Inicjalizacja początkowych środków powinna być losowa
/// - Powinna być możliwość rozszerzenia algorytmu o podawanie początkowych centroidów
/// - Powinien sprawdzać, czy jakiekolwiek centrum się zmieniło, jeśli nie przerywać działanie
/// - Powinien mieć ograniczenie na liczbę iteracji
/// - Macierz odległości powinna być liczona jednokrotnie
/// - Powinna być możliwość wyboru miary jakości klastra, do późniejszego wyboru nowego centroidu
//
/// Wymagania specyficzne dla FuzzyKmenoids:
/// - Każdy ciąg należy w jakiejś części do każdej z grup, części sumują się do 1
/// - Powinna być możliwość nakładania funkcji na czesci w ktorych dany ciag nalezy do grup

#pragma once

#include "src/grupowanie/Kmenoids.tpp"
#include "src/miary/macierzOdleglosci.tpp"
#include <map>
#include <ranges>
#include <vector>
#include <functional>
#include <random>

template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
class FuzzyKmenoids : public Kmenoids<T>
{
  public:
    using typename Kmenoids<T>::CiagZPozycja;

    FuzzyKmenoids(std::function<double(const typename T::value_type&, const typename T::value_type&)>,
        std::mt19937&, int, int = 100);

  protected:
//    int wyznaczCentroid(const std::vector<std::vector<float>>&, int numerGrupy) const override;
    std::vector<float> przynaleznoscCiagu(const int elementInd, const std::vector<int>& indeksyCentroidow) const override;
};
