#pragma once
#include "OptymalizatorEuklidesa.hpp"
#include <unordered_set>

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
void SoftDBAKmeans<F, pSmoothMinType>::OptymalizatorEuklidesa::sprawdzCzyZapadlySieGrupy(
    const std::vector<adept::Matrix>& centroidy)
{
  auto wynikPredykcji = this->klasaNadrzedna->predict(this->zbior, centroidy);
  std::unordered_set<short> indeksy;
  indeksy.insert(wynikPredykcji.begin(), wynikPredykcji.end());
  if(indeksy.size()<(uint32_t)this->klasaNadrzedna->liczbaGrup)
    throw ZapadniecieGrup();
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
SoftDBAKmeans<F, pSmoothMinType>::OptymalizatorEuklidesa::OptymalizatorEuklidesa (const std::vector<adept::Matrix>& centroidy, const std::vector<adept::Matrix>& zbior,
    const SoftDBAKmeans<F, pSmoothMinType>* klasaNadrzedna)
  : SoftDBAKmeans<F, pSmoothMinType>::IOptymalizator(centroidy, zbior, klasaNadrzedna)
{}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
adept::Matrix SoftDBAKmeans<F, pSmoothMinType>::OptymalizatorEuklidesa::obliczGradientCentroidu(const adept::Matrix& pochodne, 
    const adept::Matrix& centroid, const adept::Matrix ciag) const
{
  adept::Matrix gradient(centroid.dimensions());
  gradient = 0;

  int dlWzorca = centroid.dimension(0);
  int dlPrzyrownywanego = ciag.dimension(0);

  for(int i=0;i<dlWzorca;i++)
  {
    for(int j=0;j<dlPrzyrownywanego;j++)
    {
      adept::Vector g=pochodne(i,j)*this->klasaNadrzedna->softDtw.funkcje.pochodna(centroid[i], ciag[j]);
      gradient[i]+=g;
    }
  }
  return gradient;
}

template<template <typename> typename F, PSmoothMinType pSmoothMinType>
adept::Real SoftDBAKmeans<F, pSmoothMinType>::OptymalizatorEuklidesa::calc_cost_function_gradient(const adept::Vector& v, adept::Vector gradient)
{
  std::vector<adept::Matrix> centroidy;
  centroidy=this-> template adeptWektorDoCentroidow<adept::Matrix>(v);

  std::vector<adept::Matrix> wektorGradientow;
  wektorGradientow.reserve(centroidy.size());

  for(const auto & c : centroidy)
  {
    adept::Matrix m(c.dimensions());
    m=0;
    wektorGradientow.push_back(m);
  }

  double wynikSumaryczny=0;
  for (const auto& ciag : this->zbior)
  {
    std::vector<adept::Matrix> macierzeKosztu;
    adept::Vector odl(this->rozmiaryCentroidow.size());
    this->klasaNadrzedna-> template wyliczWektorOdlIMacierzeKosztu<adept::Matrix>(
        ciag, odl, macierzeKosztu, centroidy, this->klasaNadrzedna->softDtw);

//    this->normalizujOdl(odl, centroidy);
    int najIdx = adept::minloc(odl);

    wynikSumaryczny+=adept::value(odl[najIdx]);
    auto pochodne = this->klasaNadrzedna->softDtw.backward(macierzeKosztu[najIdx], centroidy[najIdx], ciag);

    auto gradientCentroidu = obliczGradientCentroidu(pochodne, centroidy[najIdx], ciag);

    wektorGradientow[najIdx]+=gradientCentroidu;
  }
  this->centroidyDoAdeptWektora(wektorGradientow, gradient);
  return adept::value(wynikSumaryczny);
}
