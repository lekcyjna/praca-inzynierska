/// Wymagania dla DBA-Kmenoids:
/// - Przyjmuje zbior ciągów
/// - Grupuje dane
/// - Środkami grup mogą byc tylko istniejące ciągi
/// - Działa iteracyjnie
/// - Powinien obsługiwać różne miary odległości
/// - Powinien być wątko-bezpieczny
/// - Powinien oferować możliwość wielowątkowego liczenia odległości
/// - Inicjalizacja początkowych środków powinna być losowa
/// - Powinna być możliwość rozszerzenia algorytmu o podawanie początkowych centroidów
/// - Powinien sprawdzać, czy jakiekolwiek centrum się zmieniło, jeśli nie przerywać działanie - TODO
/// - Powinien mieć ograniczenie na liczbę iteracji
/// - Macierz odległości powinna być liczona jednokrotnie
/// - Powinna być możliwość wyboru miary jakości klastra, do późniejszego wyboru nowego centroidu
/// - Dodać wykrywanie czy mamy dwa identyczne centroidy i jeśli tak to jeden zamienić na losowy - TODO
//
//
/// Idea na implementację:
/// - Macierz odległości wyliczana na początku
/// - Wyliczenie macierzy odległości odbywać się będzie w innym module
/// - W jednej itaracji zostanie wyliczona miara jakosci klastra dla każdego możliwego środka, na podstawie wcześniejszego przypisania
/// - W następnym kroku zostaną uaktualnione centroidy
/// - Do nowych centroidów zostaną przypisane od nowa punkty

#pragma once

#include "src/miary/macierzOdleglosci.tpp"
#include "src/miary/DTW.tpp"
#include <map>
#include <ranges>
#include <vector>
#include <functional>
#include <random>

template<typename T>
  requires wiezy::kolekcja<T> and wiezy::Ciag<typename T::value_type> and std::ranges::random_access_range<T>
class Kmenoids 
{
  public:
    typedef std::tuple<typename T::value_type, int> CiagZPozycja;

    Kmenoids(std::function<double(const typename T::value_type&, const typename T::value_type&)>,
        std::mt19937&, int, int = 100);
    void fit(const T&);
    std::vector<std::vector<float>> predict(const T&) const;
    std::vector<typename T::value_type> zwrocCentroidy() const;
    void ustawCentroidy(std::vector<CiagZPozycja>);
    std::vector<CiagZPozycja> inicjalizujCentroidyLosowo(const T&) const;
    MacierzOdleglosci zwrocMacierzOdleglosci() const;
    void ustawMacierzOdleglosci(MacierzOdleglosci);

  protected:
    virtual int wyznaczCentroid(const std::vector<std::vector<float>>&, int numerGrupy) const;
    virtual std::vector<float> przynaleznoscCiagu(const int elementInd, const std::vector<int>& indeksyCentroidow) const;

    std::vector<CiagZPozycja> wyznaczNoweCentroidy(const T&, const std::vector<std::vector<float>>&) const;

    std::function<double(const typename T::value_type&, const typename T::value_type&)> funkcjaOdleglosci;
    MacierzOdleglosci macierzOdleglosci;
    std::vector<CiagZPozycja> centroidy;
    int liczbaGrup;
    int n_iter;
    bool fitted;
    bool centroidyZainicjalizowane;
    bool wyliczonoOdleglosci;
    mutable std::mt19937 randomGenerator;
    static constexpr double INF = 1e307;
};
