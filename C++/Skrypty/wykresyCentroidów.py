import argparse
import numpy as np
import matplotlib.pyplot as plt

parser=argparse.ArgumentParser()
parser.add_argument("sciezka")
parser.add_argument("-n", type=int, default=6)
parser.add_argument("--zapisz", action="store_true")
args=parser.parse_args()

X=[]
with open(args.sciezka, "r") as plik:
    for line in plik:
        linia=line[:-2].split("\t")
        linia=[float(i) for i in linia]
        X.append(np.array(linia))

plt.gcf().set_constrained_layout(True)
plt.gcf().set_size_inches((6,4))
for i in range(args.n):
    plt.subplot(2,4,i+1)
    plt.title(i+1)
    plt.axhline(c="black", lw=1)
    plt.ylim(-3,3)
    plt.plot(X[i])

if (args.zapisz):
    plt.savefig(args.sciezka+"-wykres.jpg")
plt.show()
