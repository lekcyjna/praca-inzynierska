import numpy as np
import argparse as arg

parser=arg.ArgumentParser()

parser.add_argument("sciezkaPrzynal", type=str, 
        help="Sciezka do pliku z przynaleznosciami")
parser.add_argument("sciezkaSekw", type=str,
        help="Sciezka do pliku z sekwencjami.")
parser.add_argument("N", type=int,
        help="Numer grupy, która ma zostać wyekstrachowana.")

args = parser.parse_args()

P=np.loadtxt(args.sciezkaPrzynal)
with open("noweDane.txt", "w") as zapis:
    with open(args.sciezkaSekw,"r") as plik:
        i=0
        for linia in plik:
            if P[i]==args.N:
                print(linia, end="", file=zapis)
            i+=1


