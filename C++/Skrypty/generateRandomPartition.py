import numpy as np
import argparse as arg

parser=arg.ArgumentParser()

parser.add_argument("sciezkaSekw", type=str,
        help="Sciezka do pliku z sekwencjami.")
parser.add_argument("N", type=int,
        help="Liczba grup do cross-validacji.")

args = parser.parse_args()

with open(args.sciezkaSekw,"r") as plik:
    dl=0
    for linia in plik:
        dl+=1
N=args.N
maska=np.random.randint(0, N, size=dl)
for i in range(N):
    with open(f"crossValDane{i}.txt", "w") as zapis:
        with open(args.sciezkaSekw,"r") as plik:
            j=0
            for linia in plik:
                if maska[j]!=i:
                    print(linia, end="", file=zapis)
                j+=1


