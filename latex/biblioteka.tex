\chapter{Biblioteka w C++}
\label{roz:biblioteczka}

W celu przeprowadzenia przedstawionych analiz przygotowano bibliotekę z podstawowymi algorytmami analizy danych sekwencyjnych. Biblioteka ta została napisana w standardzie C++ 20 w paradygmacie programowania generycznego, z użyciem konceptów w celu zabezpieczenia przed przekazaniem nieobsługiwanych typów. Zapewnia więc dużą elastyczność i szybkość działania, kosztem dłuższego czasu implementowania (np. względem bibliotek pythonowych), relatywnie długiego czasu kompilacji oraz kosztem słabej czytelności kodu. Elastyczność i możliwość wielokrotnego zastosowania była priorytetem podczas implementacji, w związku z tym część decyzji, które podjęto nie było optymalnych pod względem szybkości czasu działania, pozwoliły one za to uzyskać kod umożliwiający łatwą rozszerzalność biblioteki.


\section{Zależności}

Aby nie implementować wszystkich potrzebnych algorytmom elementów od podstaw, użyto kilku zewnętrznych bibliotek o otwartym kodzie źródłowym.

\begin{description}
\item [Adept-2] - \cite{Hogan2014-Adept2} --- wersja 2.1 --- biblioteka implementująca automatyczne wyliczanie pochodnych, a także algorytmy optymalizacji. Zapewnia szablonową implementację macierzy używającą expression templates, czyli wzorca projektowego, który pozwala na dodatkowe optymalizacje w czasie kompilacji. Między innymi można pominąć konieczność zapisywania zmiennych tymczasowych, co jest szczególnie istotne w algebrze liniowej, przykładowo w dodawaniu wektorów. Operator "+" w C++ jest binarny, więc wrażenie \(\overrightarrow{x} = \overrightarrow{a} + \overrightarrow{b} + \overrightarrow{c}\) zostałoby normalnie zaimplementowane jako:
\begin{gather*}
\overrightarrow{t}=\overrightarrow{a} + \overrightarrow{b} \\
\overrightarrow{x}=\overrightarrow{t} + \overrightarrow{c}
\end{gather*}
Czyli nastąpiłaby niepotrzebna alokacja wektora \(\overrightarrow{t}\). Natomiast dzięki expression templates można zbudować drzewo operacji, opisujące wyrażenie, które będzie zapisywało tylko raz do pamięci, będzie to ostateczny wynik. Przejście takiego drzewa w czasie działania programu byłoby kosztowne, ale dzięki temu, że jest ono zaimplementowane jako wyrażenia szablonowe, to rozwiązuje się ono na etapie kompilacji i nie spowalnia wykonania programu \parencite{Wikipedia-expressionTemplates}.

\item [spdlog] - wersja 1.9.2 --- umożliwia wygodne logowanie wiadomości z wykonania programu, ma możliwość wyłączenia go na etapie kompilacji, dzięki czemu nie spowalnia wykonania funkcji

\item [gtest/gmock] - wersja 1.11.0 --- biblioteki o otwartym kodzie źródłowym rozwijane przez firmę Google umożliwiające łatwe pisanie testów jednostkowych i moków.
\end{description}

Oprogramowanie potrzebne do zbudowania biblioteki, od którego jednakże nie zależy bezpośrednio kod źródłowy.
\begin{description}
\item[cmake] - wersja 3.22.1 --- pozwala na automatyczne generowanie plików Makefile, co ułatwia utrzymanie procesu budowania
\item [make] - wersja 4.3 --- ułatwia zarządzanie procesem budowania biblioteki
\item [g++] - wersja 11.1.0 --- kompilator języka C++
\item [git] - wersja 2.34.1 --- system kontroli wersji
\end{description}


\section {Funkcjonalności}

Biblioteka przygotowana w ramach tej pracy zawiera dwie główne typy algorytmów. Pierwszym z nich są algorytmy liczące podobieństwo między ciągami, czyli przede wszystkim DTW oraz SoftDTW. Drugim typem algorytmów są modele grupujące sekwencje, z pośród których zostały zaimplementowane takie funkcje jak Kmenoids, FuzzyKmenoids oraz SoftDBA--Kmeans.

\subsection{Miary podobieństwa}

\subsubsection {DTW}

Klasa szablonowa \texttt{DTW} jest parametryzowana przez dwa typy, pierwszym z nich jest struktura przechowująca sekwencję, przykładowo \texttt{std::vector}. Parametr ten musi spełniać więz \texttt{wiezy::Ciag<T>}.

\begin{lstlisting}[caption={Więz ograniczający typy, które można przekazać jako sekwencja do klasy \texttt{DTW}.},style=kod]
template <typename T>
concept Ciag = 
  requires (T x)
{
  typename T::value_type;
  requires std::ranges::range<T>;
  x.size();
};
\end{lstlisting}

Drugim parametrem szablonu jest klasa wykorzystywana do statycznego polimorfizmu, w której powinny zostać zaimplementowane statyczne metody, spełniająca koncept \texttt{wiezy::ZmienneFunkcjeDTW<T,F>}, gdzie \texttt{T} jest typem ciągu, a \texttt{F} typem klasy statycznego polimorfizmu.

\begin{lstlisting}[caption={Więz nakładający ograniczenia na klasę statycznego polimorfizmu.},style=kod,label=kod:wiez::Ciag]
template <typename T>
concept Xdouble = std::same_as<T, double> ||
	std::same_as<T, adept::adouble>;

template <typename T, typename F>
concept ZmienneFunkcjeDTW =
requires (typename T::value_type t, F f, int i)
{
  {f.funkcjaOdleglosci(t,t)} -> Xdouble;
  {f.wiezyMacierzyDynamicznej(i,i,i,i)}->std::same_as<bool>;
};
\end{lstlisting}

Dwie metody zdefiniowane w tej klasie są używane podczas wyliczania odległości DTW. \texttt{funkcjaOdleglosci} służy do wyliczania miary odległości miedzy elementem z \hfill jednego \hfill ciągu, \hfill a \hfill elementem \hfill z \hfill drugiego \hfill ciągu. \hfill Natomiast \hfill funkcje\\ \texttt{wiezyMacierzyDynamicznej}\footnote{Na podstawie definicji więzu z kodu \ref{kod:wiez::Ciag} można odnieść wrażenie, że funkcja ta przyjmuje 4 takie same argumenty \texttt{i}. Powtórzenie tej samej zmiennej jest jednak uzasadnione, ponieważ zmienne w definicji więzów nie posiadają wartości i służą one jedynie jako nośnik typu, dzięki któremu można sprawdzić, czy wyrażenie poprawnie się kompiluje.} stosuje się do nałożenia ograniczeń na macierz przyrównania w algorytmie dynamicznym, w celu ograniczenia złożoności obliczeniowej DTW. Jej argumentami są współrzędne \((x,y)\) aktualnie wyliczanego pola oraz wysokość i szerokość macierzy. Na podstawie tych czterech wartości określa się czy zadane pole ma zostać wyliczone, czy też nie.

Główną metodą klasy \texttt{DTW} jest przeciążony operator \texttt{operator()} pozwalający używać instancji klasy \texttt{DTW} jak funkcji. Do tego operatora przekazuje się dwa argumenty, będące ciągami, w rezultacie czego uruchamia się przyrównanie DTW, którego wynik jest zwracany jako rezultat działania tego operatora.

\subsubsection {SoftDTW}

W klasie \texttt{SoftDTW} został zaimplementowany algorytm o tej samej nazwie i strukturą przypomina klasę \texttt{DTW}. Klasa ta różni się jednak zastosowanymi więzami.

\begin{lstlisting}[caption={Nagłówek klasy \texttt{SoftDTW}.},style=kod]
template <typename T1, typename F>
requires (std::same_as<T1, adept::aMatrix> or
	std::same_as<T1, adept::Matrix>) and
	wiezy::ZmienneFunkcjeDTW<T1,F>
class SoftDTW 
{
...
\end{lstlisting}

Przede wszystkim typ ciągu został ograniczony do jednej z dwóch możliwości pierwszą z nich jest \texttt{adept::aMatrix}\footnote{W bibliotece adept przedrostek ,,\texttt{a}`` przed typem oznacza ,,active``, czyli typ podlegający automatycznemu różniczkowaniu}, włączający wyliczanie gradientu, a drugi \texttt{adept::Matrix} używany w przypadku gdy niepotrzebne jest wyliczanie pochodnej. Obie klasy implementujące macierze dwuwymiarowe pochodzą z biblioteki adept. Takie zawężenie możliwych typów wejściowych było podyktowane faktem, że SoftDTW można różniczkować, a w celu wsparcia automatycznego różniczkowania konieczne było użycie typu \texttt{adept::aMatrix}, w wyniku czego składnia w kodzie zmieniła się na tyle, że nie było możliwości dodania łatwej i czytelnej obsługi innych typów. Jednocześnie typy macierzowe z biblioteki adept pozwalają uzyskać ciągły układ danych w pamięci (co wspiera ich przechowywanie w pamięci podręcznej procesora), natomiast takiej gwarancji nie ma w przypadku użycia zagnieżdżonych wektorów (jak się często implementuje reprezentację macierzy danych w czystym C++).

Konstruktor \texttt{SoftDTW} oprócz klasy z funkcjami dla polimorfizmu statycznego przyjmuje zmienną \texttt{gamma}, opisującą stopień wygładzania podczas wyliczania gładkiego minimum.

Tak jak w przypadku \texttt{DTW} w \texttt{SoftDTW} został przeciążony operator \texttt{operator()}. Jednakże tym razem zwracany typ zależy od typu opisującego pierwszy ciąg przyjmowany przez funkcję. Jeśli do funkcji przekazywany jest \texttt{adept::aMatrix} to zwracanym typem jest \texttt{adept::adouble}, czyli obiekt opakowujący zwykły typ \texttt{double} w funkcjonalność umożliwiającą automatyczne różniczkowanie. Wpływ pierwszego typu na typ zwracany wynika z faktu, że pierwszy ciąg jest tym, względem którego będzie \hfill ewentualnie \hfill liczony \hfill gradient. \hfill Drugi \hfill natomiast \hfill będzie \hfill zawsze \hfill typu \\  \texttt{adept::Matrix}. \hfill W \hfill celu \hfill umożliwienia \hfill zwracania \hfill różnego \hfill typu \hfill wykorzystano\\ \texttt{std::conditional} umożliwiający warunkowy wybór typu w szablonach.

\begin{lstlisting}[caption={Deklaracja operatora \texttt{operator()} z klasy \texttt{SoftDTW}.},style=kod]
typedef typename std::conditional<
	std::is_same_v<T1, adept::Matrix>,
	double,
	adept::adouble>::type xdouble;

xdouble operator() (const T1&, const adept::Matrix&) const;
\end{lstlisting}


\subsubsection{Różniczkowalność SoftDTW}


Przy liczeniu gradientu z SoftDTW można zastosować dwa podejścia. Pierwsze to samodzielnie zaimplementować funkcje wyliczające pochodne i gradienty, natomiast drugie to wykorzystać bibliotekę do automatycznego różniczkowania. 

Pierwsze podejście będzie szybsze w działaniu, ponieważ nie ma narzutu silnika różniczkującego, jednak wymaga ręcznego wyliczenia wzoru na gradienty, co może stanowić barierę w użytkowaniu tej funkcji przez osoby nie mające doświadczenia matematycznego, a także stanowi pole, na którym można popełnić wiele trudnych do znalezienia błędów. Drugie podejście jest natomiast wolniejsze w działaniu (o ok. 40\% w przypadku SoftDTW z odległością euklidesową między elementami), jednak łatwiejsze w użytkowaniu, ponieważ wystarczy zaimplementować funkcję kosztu. W wersji algorytmu SoftDTW zaimplementowanej w bibliotece przygotowano wsparcie dla obu tych podejść. W przypadku gdy pożądane jest skorzystanie z biblioteki adept zapewniającej automatyczne liczenie pochodnej, wystarczy przekazać ciąg wejściowy w postaci typu \texttt{adept::aMatrix}. 



\subsection {Algorytmy grupujące}

\subsubsection {Kmenoids i FuzzyKmenoids}

Algorytmy Kmenoids i FuzzyKmenoids są używane do wyznaczania menoidów, czyli elementów z uczącego zbioru sekwencji, których średnia odległość do wszystkich pozostałych ciągów będzie najmniejsza. Metody te mają duże problemy ze zbieżnością i często utykają w lokalnych optimach, jednakże to że centra są faktycznymi ciągami ze zbioru uczącego nadaje wynikom dużą interpretowalność, dlatego zdecydowano się na ich zaimplementowanie w tej bibliotece.

Interfejs tych klas jest podobny do algorytmów z pythonowej biblioteki \texttt{sklearn}, czyli klasy te dysponują metodą \texttt{fit}, z pomocą której uczy się modele oraz metodą \texttt{predict} używanej do operowania na wyuczonym modelu w celu przyporządkowania nowych danych do jednej z otrzymanych podczas uczenia grup.

Jako że FuzzyKmenoids i Kmenoids są bardzo podobnymi algorytmami (różnią się tylko przypisaniem elementów do grup --- w wersji fuzzy przypisanie jest rozmyte), to zdecydowano się wykorzystać dziedziczenie by zminimalizować ilość zduplikowanego kodu. Dzięki temu dla funkcji FuzzyKmenoids wystarczyło nadpisać funkcję wirtualną wyliczająca przynależność ciągu do jednej z grup.

Pociągnęło to jednak za sobą kilka nieoptymalnych z punktu widzenia wydajności programu zmian w implementacji funkcji Kmenoids. Przykładowo operuje ona na pełnej macierzy przynależności (o wymiarach \((n,k)\), gdzie \(n\) to liczba szeregów, a \(k\) liczba grup), pomimo tego, że można by wykorzystać fakt, że w każdym wierszu tej macierzy jest dokładnie jedna jedynka i uprościć ją do tablicy rozmiaru \(n\). Taka optymalizacja przyśpieszyłaby działanie programu o około \(7\%\). Jednakże spowodowałaby też konieczność skopiowania dużej części logiki klasy \texttt{Kmenoids} by utworzyć klasę \texttt{FuzzyKmenoids} co utudniłoby utrzymanie kodu, a tak możliwe było zaimplementowanie wersji algorytmu z rozmytymi przypisaniami przy pomocy prostego dziedziczenia.

\subsubsection{SoftDBA--Kmeans}
SoftDBA--Kmenas jest aktualnie jednym z najnowszych i najskuteczniejszych modeli grupujących dane sekwencyjne i dlatego jego implementacja jest kluczową funkcją w tej bibliotece. Algorytm ten opiera się na wyznaczaniu centroidów przy pomocy gradientu uzyskiwanego z funkcji kosztu SoftDTW.

W implementacji stworzonej na potrzeby tej pracy wprowadzono inicjalizowanie centroidów poprzez wylosowanie ich z danych uczących, dzięki czemu --- w przeciwieństwie do wersji przedstawionej w artykule \cite{Cuturi2018-Softdtw} oraz zaimplementowanej w bibliotece \texttt{tslearn} \parencite{Tavenard2020-tslearn} --- centra mogą być różnej długości. 

Pozostałe funkcjonalności to:
\begin{itemize}
\item Opcjonalny wybór jednego z dwóch sposobów wyliczania gradientu SoftDTW. Przy pomocy ręcznie wyliczonych i zaimplementowanych wzorów, albo przy pomocy silnika adept do automatycznego różniczkowania. W tym drugim podejściu można łatwo podmienić funkcję kosztu między elementami z dwóch ciągów z odległości euklidesowej (jaka jest przedstawiona w oryginalnej wersji algorytmu) na dowolną inną.
\item Opcjonalna ręczna inicjalizacja centroidów
\item Dwa sposoby normalizacji SoftDTW
\begin{itemize}
\item Brak normalizacji
\item Normalizacja przez długość centroidu
\end{itemize}
\item Wsparcie dla różnych funkcji SmoothMinimum:
\begin{itemize}
\item Log--Sum--Exp (w skrócie LSE) - funkcja użyta w oryginalnej pracy \cite{Cuturi2018-Softdtw}
\item ExpDiv - funkcja aproksymująca minimum z góry
\end{itemize}
\item Reinicjalizacja centroidów w celu uzyskania grup początkowych o odpowiednio dużej liczności
\end{itemize}

Do optymalizacji centroidów wykorzystywany jest algorytm L--BFGS zaimplementowany w bibliotece \texttt{adept}.

Należy zwrócić uwagę, że SoftDTW używa funkcji wykładniczych i w związku z tym bardzo istotnym jest to, by dane przekazywane do SoftDBA--Kmeans były odpowiednio znormalizowane, tak by gradient nie przekroczył zakresu liczb zmiennopozycyjnych i nie pojawiły się błędy numeryczne.

\section {Uwagi implementacyjne}

W czasie pisania biblioteki przeprowadzano testy optymalności napisanego kodu. W tym celu zastosowano narzędzia do profilowania kodu, takie jak: \texttt{gprof} (wersja 2.36.1) \parencite{Graham2004-gprof}, \texttt{callgrind} (wersja 3.18.1) \parencite{Weidendorfer2004-callgrind}, \texttt{oprof} (wersja 1.4.0, wersja jądra Linuksa 5.15.12). Na podstawie tych testów wprowadzono między innymi następujące poprawki:

\subsubsection{Rozbicie pętli algorytmu DTW}
W oryginalnej wersji algorytmu DTW znajdowała się tylko jedna pętla, która wyliczała całą macierz przyrównania w algorytmie dynamicznym, także zerową kolumnę i zerowy wiersz, jednakże były one inaczej obliczane, niż reszta tabeli, ponieważ nie istnieje minus pierwszy wiersz i minus pierwsza kolumna. W związku z tym w pętli znajdowały się bloki warunkowe, które miały obsłużyć te przypadki. Okazało się jednak, że sprawdzanie warunków brzegowych było kosztowne ze względu na to, iż był to kod intensywnie wykonywany. Dlatego obliczenie pierwszej kolumny i pierwszego wiersza przeniesiono do dwóch osobnych pętli, a trzecia oblicza środek tabeli przyrównania. Pozwoliło to uzyskać około \(3\%-4\%\) szybsze działanie kodu.

\subsubsection {Optymalizacja polimorfizmu klasy DTW} 

Aby uzyskać uniwersalność biblioteki, część elementów algorytmów jest przekazywana w postaci parametrów. Przykładem tego może być klasa \texttt{DTW} obliczająca stosowną miarę podobieństwa, która ma parametr pozwalający przekazać dowolną funkcję odległości, przy pomocy której zostanie wyliczona odległość między dwoma elementami ciągów. 

Początkowo \hfill implementacja \hfill ta \hfill opierała \hfill się \hfill na \hfill funkcjach \hfill lambda \hfill i \hfill klasie \\ \texttt{std::function} z biblioteki standardowej. Użytkownik podczas instancjonowania klasy \texttt{DTW} podawał w parametrach konstruktora funkcję lambda, a ta była następnie używana by wyznaczyć odległość. Jednakże w związku z tym, że funkcja wyliczania odległości między dwoma elementami była bardzo często wywoływana (w przybliżeniu \(O(n^2m^2)\) razy, gdzie \(n\) to liczba ciągów, a \(m\) średnia długość jednego ciągu), to nawet minimalny narzut związany z warstwami abstrakcji powodował znaczący wzrost czasu wykonania całego programu. Stało się tak w przypadku tej implementacji, ponieważ w \texttt{std::function} przed wywołaniem właściwej funkcji sprawdza się, czy jest ona poprawna (czyli, czy nie spróbujemy wywołać \texttt{nullptr}), a jeśli nie to zwraca się wyjątek \texttt{std::bad\_function\_call}. Sprawdzenie to powoduje, że kompilator nie może prosto zoptymalizować kodu i podstawić za instancję klasy \texttt{std::function} funkcji, którą ta instancja przechowuje. Dodatkowym narzutem było wywołanie funkcji lambda, który łącznie ze wcześniejszym zajmował ok. \(13\%\) czasu wykonania kodu według pomiarów wykonanych z użyciem \texttt{oprof}.

Aby sprawdzić, czy można obniżyć koszt działania DTW przeprowadzono test, w którym zamiast wywoływać funkcję lambda, użyto bezpośredniego wywołania funkcji wyliczającej odległość euklidesową. Taki zabieg spowodował przyśpieszenie działania programu o ok. \(25\%\). W związku z tym, że przy spodziewanym czasie wyliczania macierzy odległości DTW rzędu minut lub godzin \(25\%\) przyśpieszenia jest znaczące, to podjęto próbę uzyskania go bez straty elastyczności funkcji.

W tym celu przygotowano implementację algorytmu DTW rozbitą na dwie części połączone dynamicznym polimorfizmem. Pierwsza część, czyli klasa \texttt{DTWBase} zawierała większość implementacji i czysto wirtualną metodę \texttt{funkcjaOdleglosci}, w której powinna zostać wyliczona odległość między dwoma elementami z ciągów. Po klasie \texttt{DTWBase} dziedziczyły klasy potomne z zaimplementowaną brakującą metodą. Stworzono dla porównania dwie klasy--dzieci: \texttt{DTWEuklides} implementującą odległość euklidesową, taką jak w teście przeprowadzanym wcześniej i \texttt{DTWLambda} pozwalającą przekazać dowolną funkcję lambda, która była wywoływana w celu wyliczenia odległości. Klasa ta działała więc na podobnej zasadzie jak pierwotna implementacja DTW i służyła jako punkt odniesienia, czy dziedziczenie i związany z nim narzut na odwołanie do vtable (tablica z adresami funkcji wirtualnych) klasy jest zauważalny.

Po przeprowadzeniu pomiarów okazało się, że \texttt{DTWLambda} jest wolniejsza o ok. \(3\%\) od oryginalnej implementacji, natomiast \texttt{DTWEuklides} o ok. \(4\%\) szybsza. Pojawiło się więc przyśpieszenie, jednakże było ono znacząco mniejsze niż wynikające ze wcześniejszych testów. Było to wynikiem dwóch rzeczy. Po pierwsze aby, wywołać funkcję wirtualną, trzeba w pierwszej kolejności odpytać vtable o adres tej funkcji. Powoduje to jeden dodatkowy skok w pamięci podczas wywołania funkcji, który oprócz tego, że zwiększa prawdopodobieństwo nietrafienia w dane przechowywane w pamięci podręcznej procesora, to także może zablokować spekulatywne wykonanie instrukcji w procesorach out--of--order\footnote{Czyli w większości dzisiejszych procesorów używanych komputerach.} ze względu na skok zależny od adresu przechowywanego w pamięci RAM. Dodatkowo użycie funkcji wirtualnych powoduje, że kompilator nie może zoptymalizować łącznie funkcji wołającej i wołanej, ponieważ dopiero w czasie działania programu jest określana funkcja wołana. (Klasa bazowa wywołuje jakąś metodę z vtable, jednakże nie zna ona definicji tej metody, ponieważ w zależności od tego jaka klasa dziedziczy po klasie bazowej definicja będzie inna, w związku z tym w wygenerowanym asemblerze musi wystąpić \textit{explicite} wywołanie funkcji).

Aby obejść ten problem zdecydowano się na zastosowanie statycznego polimorfizmu. W tym celu powrócono do koncepcji jednej klasy \texttt{DTW}, jednakże dodano nowy parametr szablonowy, który jest obiektem zawierającym funkcję \texttt{funkcjaOdleglosci}. Zamiast więc wołać \texttt{funkcjaOdleglosci} będącą metodą klasy \texttt{DTW}, wołana jest funkcja z klasy podawanej w szablonie. Powoduje to, że dla danej instancji klasy \texttt{DTW} już na etapie kompilacji wiadome jest, jaka dokładnie funkcja odległości zostanie wywołana, a dwie różne funkcje odległości (czyli dwie różne klasy podawane jako argument szablonu) spowodują wygenerowanie się dwóch różnych typów w programie. Jednocześnie przekazywanie w parametrze szablonu klasy zawierającej funkcję odległości pozwala na dodanie do tej klasy innych metod, które wcześniej były wirtualne, przykładowo funkcji sprawdzającej więzy macierzy przyrównania dynamicznego.

Ponownie przygotowano dwie klasy implementujące funkcję odległości, tyle że tym razem przy pomocy statycznego polimorfizmu. W pierwszej z tych funkcji wyliczano \textit{explicite} odległość euklidesową, a w drugiej wywoływano podaną w czasie konstrukcji funkcję lambdę w celu wyliczenia tej odległości. Zbadano czas wykonania kodu i tym razem zaobserwowano, że o ile wersja z funkcją lambdą cały czas działała ok. \(3\%\) wolniej, to DTW używające wprost zdefiniowanej odległości euklidesowej działało ok. \(36\%\) szybciej. W tym przypadku większe przyśpieszenie niż to uzyskane w testach początkowych wynikało z tego, że oprócz \texttt{funkcjaOdleglosci} do statycznego polimorfizmu przeniesiono także funkcję określającą więzy macierzy dynamicznej, co pozwoliło kompilatorowi lepiej zoptymalizować program.


\section {Porównanie z tslearn}

Część z funkcji zaimplementowanych w tej bibliotece (DTW, SoftDTW, SoftDBA--Kmeans) posiada już implementacje w pythonowej bibliotece \texttt{tslearn} (wersja 0.5.2) \parencite{Tavenard2020-tslearn}. Istnieje jednakże kilka różnic, które uzasadniają stworzenie własnej implementacji:
\begin{itemize}
\item \texttt{tslearn} jest biblioteką do pythona implementowaną częściowo w cythonie. Powoduje to, że zazwyczaj równoważna implementacja z tej biblioteki będzie wolniejsza od takiej zaimplementowanej w C++. Widać to w przypadku wyliczania miary DTW, gdzie czas wyliczania macierzy odległości \((100,100)\) dla ciągów o długości \(256\) na jednym wątku wynosi dla \texttt{tslearn} \(27.5\) sekundy natomiast dla implementacji w C++ \(15.1\) sekundy.

\item W bibliotece \texttt{tslearn} występują problemy ze wsparciem dla wielowątkowości\footnote{W ramach wstępnych analiz wykonanych do tej pracy podjęto próbę dodania zrównoleglania SoftDBA--Kmeans w bibliotece \texttt{tslearn}. Jednakże narzut obliczeń wielowątkowych był tak duży, że skutecznie niwelował większość zysków z jednoczesnego wykonania kodu na kilku procesach (udało się przyśpieszyć kod o 40\%, gdy był wykonywany na 4 wątkach procesora) \url{https://github.com/tslearn-team/tslearn/issues/310\#issuecomment-886232685}} (co wynika między innymi z mechanizmu Global Interpreter Lock\footnote{Jest to globalna blokada w pythonie pozwalająca by tylko jeden wątek miał na raz dostęp do wykonywanego kodu.}
) i nie udostępnia ona w związku z tym zrównoleglania. W nowych wersjach C++ można łatwo zrównoleglić kod, dzięki czemu wyliczenie macierzy z poprzedniego punktu na czterech wątkach zajmuje ok. \(3,85\) sekundy.

\item Przedstawiona tutaj implementacja SoftDBA--Kmeans wspiera centroidy różnej długości, natomiast w \texttt{tslearn} wszystkim centroidom jest ustawiana długość równa długości najdłuższego ciągu w danych uczących

\item W bibliotece \texttt{tslearn} zaimplementowano tylko jeden sposób normalizacji, wyliczania odległości między elementami ciągów i wyliczania gładkiego minimum, natomiast autorska implementacja pozwala sparametryzować te elementy.
\end{itemize}

