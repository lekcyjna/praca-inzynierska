\graphicspath{{Obrazy/mozliweModele/}}

\chapter{Wybór modelu do przeprowadzenia analiz}

Przeprowadzenie analiz w celu potwierdzenia lub zaprzeczenia hipotezy o różnej używalności kodonów stop w sekwencjach prokariotycznych jest z punktu widzenia informatyki problemem grupowania danych. Celem jest znalezienie podziału sekwencji na grupy zawierające geny, które będą podobne w ramach jednej, a różne między różnymi grupami. Jednocześnie reprezentacja genów powinna być tak dobrana, by ich pogrupowanie pozwalało na stwierdzenie czy występują w danym klastrze regiony z podwyższonym udziałem kodonów podatnych na mutację nonsensowną.

Modele grupujące dzielą się na co najmniej dwie grupy, w zależności od tego co stanowi podstawę ich działania:
\begin{itemize}
\item Klasyczne --- których zasada działania opiera się o modele matematyczne wykrywające ustalone wzorce w danych, które są następnie wykorzystywane do grupowania. Cechują się zazwyczaj dużą interpretowalnością wyników, ponieważ wiadome jest jakie wzorce są wykrywane przez dany model.
\item Bazujące na sieciach neuronowych --- gdzie wykorzystywana jest optymalizacja gradientowa do zoptymalizowania funkcji celu określającej jakość grupowania. Modele tego typu mogą czasami osiągać bardzo wysoką skuteczność niedostępną dla innych sposobów grupowania, jednakże zazwyczaj nie jest możliwe zinterpretowanie wyników i stwierdzenie jakie wzorce w danych powodują daną odpowiedź sieci neuronowej.
\end{itemize}
Można także wyszczególnić grupy modeli ze względu na dane na jakich operują, przykładowe kategorie to:
\begin{itemize}
\item numeryczne --- służące do grupowania danych, w których jeden element jest opisany przez zbiór cech numerycznych, różne cechy są od siebie niezależne, ale na każdej z nich można ustanowić porządek
\item kategoryczne --- punkty danych grupowane przez modele tego typu mają cechy na których nie da się ustanowić porządku (np. cecha kolor)
\item analizujące szeregi czasowe (dane sekwencyjne) --- operują na danych, których podstawę stanowi założenie, że obserwowane wartości opisują zmienną zmieniającą się w czasie, co pozwala wykorzystywać zależności między sąsiednimi punktami do otrzymania dodatkowej wiedzy poprawiającej grupowanie
\item operujące na tekstach --- metody przystosowane do grupowania dokumentów tekstowych
\end{itemize}

Wybór każdej metody łączy się z pewnymi wadami i zaletami, które mogą znacząco wpłynąć na jakość uzyskanych wyników. W związku z tym przeanalizowano różne sposoby grupowania w celu wybrania modelu najlepiej dopasowanego do problemu hipotezy o kodonach niebezpiecznych.

\section{Modele klasyczne grupujące dane numeryczne}

Metody tego typu są jednymi z najstarszych, które powstały do grupowania danych, a zarazem jednymi z najbardziej uniwersalnych, ze względu na to, że zazwyczaj jedynym założeniem potrzebnym do ich użycia jest to, że każdy element danych wejściowych daje się opisać przy pomocy zbioru niezależnych cech numerycznych, które za każdym razem pojawiają się na tej samej pozycji w danych. Najpopularniejsze z nich to Kmeans\footnote{Używany do dzielenia przestrzeni danych wejściowych na obszary Voronoya poprzez iteracyjne wyszukiwanie środków grup, które minimalizują sumę odległości euklidesowych między centrum grupy, a punktami należącymi do tej grupy, połączone z naprzemiennym poprawianiem przypisania punktów danych do grup, tak aby każdy punkt danych był przypisany zawsze do najbliższej grupy.}
, DB--Scan\footnote{Służący do przeprowadzania grupowania gęstościowego poprzez badanie najbliższego otoczenia każdego punktu. Punkty mające odpowiednio wiele bliskich sąsiadów tworzą rdzeń, a wszystkie punkty ze rdzenia, a także punkty brzegowe będące sąsiadami rdzenia, tworzą jedną grupę. Liczba punktów sąsiednich potrzebna do uznania danego punktu za rdzeń, a także odległość do której przyjmujemy, że punkty są sąsiadami to parametry algorytmu.}
\parencite{Ester96-DBscan}
, Birch\footnote{Którego użycie umożliwia stworzenie grupowania hierarchicznego, poprzez skonstruowanie drzewa cech grupujących (ang. Clustering Feature Tree) z użyciem takich własności jak suma liniowa i suma kwadratów współrzędnych punktów, poprzez dodawanie do tego drzewa kolejnych punktów tak, że każdy nowy punkt jest dodawany do najbliższego liścia pod względem cech grupujących. Następnie drzewo jest reorganizowane, tak by w liściu nie było za dużo elementów oraz by zbytnio się nie rozgałęziało, na podstawie parametrów podanych do algorytmu. Punkty będące w tym samym liściu będą do siebie podobne, a im bardziej liście będą od siebie oddalone, tym bardziej punkty z tych liści będą różne. Poprzez analizę drzewa na danej wybranej wysokości, a także odpowiedni dobór parametrów, można otrzymać różną liczbę grup.}
\parencite{Zhang1996-Birch} 
czy Gath--Geva\footnote{Którym można się posłużyć, w celu rozszerzenia algorytmu Kmeans o użycie miary odległości wykorzystującej funkcję wykładniczą. Najpierw przeprowadza się wstępne grupowanie zwykłym Kmeans by wstępnie dobrać odpowiednie centra (by zmniejszyć szansę, że drugi etap nie utknie w lokalnym optimum), a następnie przełącza się na wykładniczą funkcję odległości normalizowaną poprzez odchylenie standardowe grupy, która jest dużo bardziej czuła, co pozwala na lepsze dostrojenie właściwości grupy do danych i tym samym uzyskanie lepszego wykrywania kształtów grup. Dzięki temu etapowi, w przeciwieństwie do Kmeans, algorytm ten jest niepodatny na duże różnice w średnicy i gęstości grup w danych.}
\parencite{GathGeva1989}. Jednakże to jedyne założenie potrzebne do działania tych modeli powoduje, że algorytmy te są nieefektywne dla danych sekwencyjnych analizowanych w tym problemie (ponieważ zazwyczaj występują w nich niepomijalne zależności między sąsiednimi punktami w sekwencji).

Pewnym obejściem tego problemu jest ekstrahowanie określonych cech z danych sekwencyjnych i opisywanie przy ich pomocy ciągu\footnote{Przykładowo można losować fragmenty ciągu i następnie liczyć dla nich takie własności jak średnia, odchylenie standardowe, nachylenie ciągu, maksimum, minimum itp..}
\parencite{Deng2013}. Podejście to także ma swoje wady, ponieważ należy znać \textit{a priori} cechy, które po wyekstrahowaniu stworzą sensowną reprezentację. Wymaga to więc z jednej strony wiedzy eksperckiej, a z drugiej powoduje, że model może być uprzedzony i otrzymywane z jego użyciem wyniki będą zgodne z wizją eksperta.

W ramach testów modeli klasycznych w rozwiązaniu problemu używalności kodonów niebezpiecznych przeanalizowano możliwość użycia algorytmów Kmenoids\footnote{Grupującego dane na podobnej zasadzie jak Kmeans, tyle że Kmenoids zakłada, że centroidami mogą być wyłącznie punkty z danych uczących, podczas gdy w Kmeans centroidami mogą być punkty z całej przestrzeni danych.}
oraz FuzzyKmenoids\footnote{Będącego wersją Kmenoids z rozmytym przypisaniem do grup, czyli jeden punkt danych może należeć na raz do kilku grup, jednak przynależności do grup muszą sumować się do 1 i nie mogą być ujemne.}, 
w połączeniu z miarą DTW\footnote{Wyliczającą koszt optymalnego przyrównania dwóch sekwencji, który jest wyrażany jako suma odległości euklidesowych między sparowanymi elementami.} (ang. Dynamic Time Warping). Takie połączenie umożliwia zastosowanie tych algorytmów grupujących pomimo analizowania danych sekwencyjnych, bowiem założeniem wymaganym do ich użycia jest zdefiniowanie miary, która określa podobieństwo dowolnej pary elementów grupowanych --- w tym przypadku pary sekwencji --- czyli dokładnie to co zapewnia DTW. Jednakże słabe założenia powodują, że część zależności w danych może być pomijana, co znajdzie negatywne odzwierciedlenie w uzyskiwanych wynikach. Właśnie coś takiego zostało zaobserwowane w przypadku analiz tych modeli, bowiem uzyskane z ich pomocą rezultaty były niestabilne i regularnie utykały w lokalnych optimach.


\section{Sieci neuronowe}
\label{roz:modeleSieciNeur}

Inną grupą modeli są głębokie sieci neuronowe (ang. Deep Neural Nets), posiadające zazwyczaj własność uniwersalnej aproksymacji (choć nie wszystkie, wyjątkiem mogą być przykładowo sieci RealNVP (ang. Real--valued non--volume preserving) \parencite{Wehenkel2020}). Modele tego typu są stworzone z tysięcy pojedynczych komórek (tzw. neuronów), które reprezentują różniczkowalne funkcje. Parametry tych funkcji dobierane są przy pomocy algorytmów optymalizacji opartych na funkcji gradientu, gdzie optymalizowana jest zadana przez programistę funkcja kosztu.

Podobnie jak w przypadku klasycznych metod analizy danych, tak i w przypadku sieci neuronowych wyewoluowała grupa modeli służących do analizy danych sekwencyjnych (ang. Recurrent Neural Nets, w skrócie RNN). Podstawowymi typami neuronów w RNN są aktualnie LSTM\footnote{Będąca komórką w której może być zapamiętana jakaś wartość. Wartość tę można zastąpić nową wartością, usunąć lub odczytać. Taka struktura pomaga zapobiegać zanikaniu oraz eksplodowaniu gradientu.}
(ang. Long Short-Term Memory) \parencite{Hochreiter1997-LSTM} oraz GRU\footnote{Która jest uproszczeniem komórki LSTM umożliwiającym jedynie usunięcie wartości lub jej aktualizację.}
(ang. Gated Recurent Unit) \parencite{Cho2014-GRU}, które są następnie łączone w bardziej skomplikowane topologie, takie jak Dilated--RNN\footnote{Na kolejnych poziomach tej sieci neuronowej znajduje się coraz mniej neuronów, które służą do analizowania coraz większych fragmentów sekwencji. Przykładowo z użyciem neuronów z pierwszego poziomu przetwarzany jest każdy element sekwencji, komórki na drugim poziomie aplikowane są na wyniki tylko co drugiego neuronu z pierwszego poziomu, efektywnie więc na raz przetwarzane są dwa elementy z sekwencji, neurony na trzecim poziomie otrzymują wyniki z co drugiego neuronu z drugiego poziomu, więc efektywnie służą do wyliczenia wartości na podstawie czterech elementów z sekwencji itd.}
\parencite{Chang2017-DilatedRNN}.

Pierwotnie sieci neuronowe były wykorzystywane do uczenia z nadzorem, gdzie każdej próbce danych wejściowych odpowiadała poprawna etykieta jaką powinien przyznać model. Jednak wraz z postępem prac nad sieciami zaczęły być tworzone modele do uczenia bez nadzoru. Najpopularniejszymi z nich są autoenkodery, służące do znalezienia mniej wymiarowej reprezentacji danych, co do której można mieć nadzieję, że będzie zawierała wyekstrahowaną, interesującą nas wiedzę \parencite{Weng2019}. Zazwyczaj struktura takich modeli składa się z co najmniej dwóch części. Pierwsza poprzez przekształcenia danych wejściowych z użyciem funkcji zakodowanych w neuronach, koduje te dane do niskowymiarowej reprezentacji i nazywana jest enkoderem. Druga część ma działanie odwrotne i służy do dekompresji zakodowanych enkoderem danych, przez co nazywana jest dekoderem.

\subsection{Zastosowanie modelu DTCR}

Dzięki połączeniu dwóch powyższych technik, czyli rekurencyjnych sieci neuronowych i autoenkoderów możliwym wydaje się stworzenie sieci, która umożliwi zakodowanie sekwencji z wejścia programu, do jakiejś mniej wymiarowej reprezentacji. Przykładowym modelem może być DTCR (ang. Deep Temporal Clustering Representation) \parencite{Qianli2019}, który wprawdzie można zastosować do analiz hipotezy o regionach podatnych na mutacje nonsensowne, jednakże wyniki otrzymywane po jego użyciu nie są zadowalające\footnote{Analizy te odbyły się w ramach projektu niebędącego częścią tej pracy, dlatego są tu tylko pokrótce przytoczone. Odnośnik do pełnych wyników: \url{https://gitlab.com/lekcyjna/dl-project}}.

DTCR składa się z trzech części. W pierwszej będącej enkoderem zastosowana jest rekurencyjna sieć neuronowa umożliwiająca zakodowanie sekwencji długości 256 do reprezentacji zawierającej 32 liczby rzeczywiste. Na podstawie tej reprezentacji wyliczane są trzy składowe funkcji kosztu:
\begin{itemize}
\item koszt odtworzenia --- jednocześnie z enkoderem uczona jest druga część modelu czyli dekoder także będący rekurencyjną siecią neuronową. Jest on używany w celu zdekodowania otrzymanej trzydziestodwu liczbowej repreztencji z powrotem do ciągu o długości 256 liczb, który następnie porównywany jest z oryginalnym i na tej podstawie wyliczany jest błąd średniokwadratowy
\item koszt grupowania --- składowa określająca jak dobrze w sensie kmeans grupują się zakodowane dane
\item koszt klasyfikacji --- umożliwiający sprawdzenie czy kodowanie pozwala na odróżnienie prawdziwych sekwencji z danych od danych losowych. Klasyfikacja odbywa się z użyciem trzeciej części modelu DTCR będącej prostą siecią klasyfikującą.
\end{itemize}

Po wyliczeniu składowe te są sumowane z wagami będącymi parametrem algorytmu, a ich suma jest następnie łącznie minimalizowana metodami gradientowymi. 

Podczas testów powiodło się stworzenie modelu do redukcji sekwencji z długości 256 liczb do długości 32 liczb i pozornie osiągnięto bardzo dobre wyniki, bowiem w odkodowywaniu wcześniej zakodowanych sekwencji błąd średniokwadratowy wynosił mniej niż 5\%. Sukces ten był jednakże złudny, ponieważ z użyciem tego modelu nie dało się ekstrahować żadnej wiedzy co zostało pokazane w dalszych testach.

Podobnie jak to zostało przedstawione w pracy \cite{Qianli2019} sekwencje ze zbioru uczącego były przeskalowane tak by miały tę samą długość, jednakże w ramach testu po wyuczeniu modelu spróbowano użyć go do kodowania i dekodowania sekwencji innej długości. Okazało się, że wydłużenie sekwencji nawet o jeden punkt powodowało, że wynik był błędny. Co więcej także prefix długości 256, na którym teoretycznie model powinien działać poprawnie nie był po zdekodowaniu nawet w przybliżeniu poprawną sekwencją. Można stąd wnioskować, że na etapie optymalizacji współczynników neuronów doszło do wykrycia pewnych zasad kompresji dla sekwencji o długości dokładnie 256, które jednakowoż nie były uniwersalnymi zależnościami (na jakich powinny operować analizy z użyciem RNN) albowiem te mogą być różnej długości i pojawiać się w różnych miejscach w danych.

Podjęto także próby uczenia sieci od początku na sekwencjach różnej długości, które się jednak nie powiodły i zdekodowanie wcześniej zakodowanej sekwencji zwracało za każdym razem sekwencję stałą o wartości będącej w przybliżeniu średnią wszystkich punktów z sekwencji, zamiast orginalnej sekwencji. Było to zapewne wynikiem zbyt krótkiego czasu uczenia, zbyt małego użytego modelu lub niedobranych dobrze parametrów algorytmu uczącego.

\subsection{Zastosowania modeli generycznych i grupujących}

Z autoenkoderami związane są modele generyczne i grupujące. Pierwsze, operując na zmiennych losowych, pozwalają generować zupełnie nowe dane o podobnej charakterystyce jak te, na których model się uczył. W ramach tych modeli wyróżnia się aktualnie trzy podgrupy: VAE\footnote{Modele do rzutowania danych wejściowych na wybrany rozkład prawdopodobieństwa (najczęściej na rozkład normalny) i następnie do dekodowania punktów z tego rozkładu w rzeczywiste dane.} (ang. Variational Auto--Encoders) \parencite{Kingma2014-VAE}, 
GAN\footnote{Modele składające się z dwóch części uczonych przeciwstawnie. Pierwsza (generator) na podstawie ziarna losowego służy do wygenerowania jakiś danych i jest premiowana za to, że nie będzie możliwe ich odróżnienie od rzeczywistych danych z użyciem drugiej część (dyskryminator). Natomiast dyskryminator jest premiowany tak by możliwe było odróżnienie danych prawdziwych od wygenerowanych.}
(ang. Generative Adversarial Network) \parencite{Goodfellow2014-GAN} oraz 
NF\footnote{Modele do przeprowadzania danych wejściowych w rozkład prawdopodobieństwa z użyciem przekształceń odwracalnych.}
(ang. Normalized Flow) \parencite{Kobyzev2020}. Drugi typ modeli, czyli sieci grupujące są relatywnie nowym typem sieci uczonych bez nadzoru. Przykładowymi modelami są DEC (ang. Deep Embedded Clustering) \parencite{Xie2015-DEC} oraz DCC (ang. Deep Continous Clustering) \parencite{Shah2018-DCC}. Działają one podobnie jak autoenkodery i służą do znalezienia sensownej, mniej wymiarowej reprezentacji, która jednocześnie będzie powodowała dobre różnicowanie się grup.

Przykładowo dzięki DEC optymalizowana jest Dywergencja Kullbacka--Leiblera między aktualnym rozkładem prawdopodobieństwa należenia próbki do danej grupy, a rozkładem w którym to prawdopodobieństwo jest bardziej jednoznaczne. Jednakże ze względu na fakt iż sieci neuronowe są uniwersalnymi aproksymatorami i mogą tym samym zakodować dane w sposób wskazujący na dowolne grupowanie, potrzebne jest wprowadzenie pewnych ograniczeń. Przykładowym ograniczeniem wykorzystanym w DEC i DCC jest wymóg, by z zakodowanej reprezentacji móc otrzymać z powrotem dane wejściowe. Dodatkowo modele te są wrażliwe na reprezentację początkową danych, ponieważ grupująca funkcja kosztu powoduje zwiększanie odległości między już dalekimi od siebie punktami i zmniejszanie odległości między punktami znajdującymi się blisko siebie. W związku z tym, jeśli na skutek inicjalizacji początkowej parametrów sieci punkty z dwóch grup wymieszają się w przestrzeni reprezentacji, to jest marginalna szansa, by na skutek uczenia modelu udało się te grupy rozdzielić.

Z powyższych modeli wybrano do analiz wstępnych model RealNVP\footnote{Na każdym poziomie sieci neuronowej dane wejściowe są dzielone na dwie części. Jedną z nich używa się jako wejście do normalnej, dosyć płytkiej sieci neuronowej, przy pomocy której wylicza się współczynniki (\(s,t\)). Te natomiast są używane następnie do przekształcenia drugiej części danych wejściowych (\(x\)) poprzez nałożenie funkcji \(f(x)=x*\exp(s)+t\).}
\footnote{\url{https://gitlab.com/lekcyjna/praca-inzynierska/-/blob/main/Notatniki/Sieci/Analiza\%20NF.ipynb}}
jako przykład sieci generycznej typu NF oraz DEC\footnote{\hbox{\url{https://gitlab.com/lekcyjna/praca-inzynierska/-/blob/main/Notatniki/Sieci/DEC.ipynb}}}
jako sieć grupującą. Model DEC jest bardzo podobny do DTCR, różni się tylko nieznacznie optymalizowaną funkcją kosztu, w związku z tym otrzymane wyniki były podobne do tych z DTCR. Z użyciem DEC możliwym było dobre zrekonstruowanie sekwencji wejściowe, ale nie powiodło się wyekstrahowanie wiedzy i znalezienie rozsądnego grupowania. Natomiast z użyciem modelu opartego na RealNVP udało się stworzyć modele do poprawnego generowania sekwencji podobnych do już istniejących. Nie powiodło się jednak wykorzystanie tego osiągnięcia do zgrupowania danych lub do znalezienia w nich jakiś zależności. Wynika to z tego, że modele NF operują na losowych danych wejściowych o takiej samej wymiarowości jaką ma mieć wyprodukowany wynik. Powoduje to, że nie da się ich użyć do zmniejszenia wymiarowości wejścia i tym samym znalezienia prawidłowości opisujących dane.

Podsumowując, sieci neuronowe są interesującymi modelami do wykorzystania w analizach, ponieważ często osiągają znacząco lepsze wyniki niż modele klasyczne \parencite{Shah2018-DCC, Goodfellow2014-GAN}. Jednakże mają też sporo wad. Podstawową z nich jest fakt, że potrzebują bardzo dużo mocy obliczeniowej by możliwe było dobranie sensownych parametrów. Aktualnie najlepsze modele są uczone miesiącami na setkach kart graficznych\footnote{Przykładowo szacuje się, że uczenie modelu GPT-3 (ang. Generative Pretrained Transformer 3) zużyło \(190\)MWh \parencite{Quach2020}, co przy założeniu, że jedna karta graficzna zużywa \(190\)W odpowiada uczeniu modelu na jednej karcie przez milion godzin, czyli około 114 lat.}, jednak nie ma pewności, że zbiegną do jakiegokolwiek przydatnego rozwiązania ze względu na to, że uczone są metodami gradientowymi podatnymi na utykanie w optimach lokalnych. Dodatkowo rezultaty sieci neuronowych będą zazwyczaj trudno interpretowalne, ponieważ jak na razie nie są znane uniwersalne metody do uzasadnienia wyników otrzymywanych z ich użyciem (choć dla niektórych sieci istnieją metody dla nich specyficzne \parencite{Zeiler2013}).




\section{Sekwencyjne modele klasyczne}

Trzecia z przedstawianych grup modeli jest dostosowana do analizy danych sekwencyjnych, takich jak szeregi czasowe czy nagrania muzyczne, dzięki czemu modele tego typu powinny się dobrze sprawdzić w analizach związanych z problemem hipotezy o kodonach niebezpiecznych, gdzie danymi są sekwencje genetyczne.

\subsection{Analiza wolnych składowych}

Analiza wolnych składowych (ang. Slow Features Analysis - SFA) została początkowo wymyślona do wyszukiwania niezależnych składowych w ciągach czasowych i używana jest do analizy sygnału \(X \in \mathbb{R}^{T \times d}\) --- o którym wiadomo że jest wynikiem działania pewnej dowolnej funkcji \(f\), która na wejściu przyjęła \(d\) niezależnych szeregów czasowych o wymiarze \(T \times 1\) --- w celu odtworzenia \(d\) oryginalnych sygnałów składających się na sygnał \(X\). Funkcja \(f\) może mieszać sygnały w potencjalnie nieliniowy sposób. Z użyciem SFA odnajduje się więc funkcję \(f^{-1}\) (lub jej aproksymację) i poprzez jej zaaplikowanie na sygnał \(X\) uzyskuje się oryginalne szeregi czasowe \parencite{Sprekeler2014a}.

Znajdowanie tych składowych odbywa się przy założeniu, że są one wolno zmieniające się, czyli mając punkt \(x_t\), można przypuszczać, że następny punkt w czasie \(x_{t+1}\) ma podobną wartość jak \(x_t\). Wynika to z obserwacji, że zazwyczaj funkcje ,,przyśpieszają'' szeregi czasowe (czyli zwiększają różnice między sąsiednimi punktami), więc najwolniejsze znalezione składowe będą pierwotnymi szeregami \parencite{Sprekeler2014a}.

Aby zilustrować powyższą obserwację przykładem, można rozważyć dwa szeregi czasowe \(A \in \mathbb{R}^T \) oraz \(B \in \mathbb{R}^T \), których elementy są generowane z rozkładu standardowego normalnego \( P(a_t)=P(b_t) \sim N(0,1) \). W takiej sytuacji gdy \( C = A + B \) to \(c_t\) jest sumą dwóch niezależnych zmiennych losowych podlegających standardowemu rozkładowi normalnemu, więc \( P(c_t) \sim N(0,2)\) co oznacza, że odchylenie standardowe zwiększyło się, więc szereg czasowy ,,przyśpieszył''.

Problem znajdowania wolnych składowych można opisać jako próbę znalezienia macierzy \(Z \in \mathbb{R}^{T \times d} \), w której każda z \(d\) kolumn reprezentuje jeden ciąg wejściowy  będących wejściem funkcji \(f\). Warunek określający to, że ciągi te mają się wolno zmieniać można natomiast zapisać jako:
\begin{gather}
\min_{f^{-1}: Z=f^{-1}(X)} 
\sum_{i=0}^{d} \sum_{t=0}^{T} \Delta Z_{t,i}^{2}
\quad \text{ gdzie } \quad
\Delta Z_{t,i} = Z_{t,i} - Z_{t-1,i}
\end{gather}
Przy założeniach że:
\begin{gather}
\forall_{i \in \{0,1,...,d\} }
\left( \sum_{t=0}^{T} Z_{t,i}  = 0 \right) 
\quad \text{ oraz } \quad ZZ^T=I
\label{wzr:ogrniczeniaSFA}
\end{gather}
Ograniczenia te mają wykluczyć trywialne rozwiązania takie jak ciąg stały (który jest bardzo wolny, ponieważ różnica między dowolnymi dwoma sąsiednimi punktami wynosi 0), a także zapobiec minimalizowaniu funkcji celu, poprzez proste dzielenie wszystkich elementów przez stałą. Pierwszy warunek implikuje, że średnia z elementów każdego znalezionego ciągu będzie 0, natomiast drugi powoduje, że wariancja elementów w każdym ciągu będzie równa 1, a każda para ciągów będzie niezależna.


Mając tak zdefiniowany problem można podjąć próbę jego rozwiązania, co ze względu na to, że w ogólności istnieje nieskończenie wiele rozwiązań \parencite{Pineau2020} robi się dodając pewne założenia (przykładowo, że funkcja mieszająca \(f\) jest liniowa), albo korzystając z metod heurystycznych. W tym drugim przypadku przygotowuje się zbiór funkcji pomocniczych \(\mathcal{H} = \{h_1,h_2,...,h_k\}\), gdzie \(h_i: \mathbb{R}^d \rightarrow \mathbb{R} \). Funkcje te są następnie aplikowane do danych wejściowych, a ich wyniki tworzą macierz \(W \in \mathbb{R}^{T \times k}\), w której wartości kolumny \(i\) są rezultatem zaaplikowania \(h_i\) na wierszach \(X\). Zbiór \(\mathcal{H}\) jest tworzony przez użytkownika algorytmu i zazwyczaj składa się z prostych funkcji nieliniowych np: \(x^2, x^3, xy, \sin{x}\) itp. Aplikując funkcje te na macierz \(X\) zakłada się, że z ich pomocą możliwe będzie usunięcie zależności nieliniowych wprowadzonych przez funkcje \(f\) i tym samym stworzenie funkcji \(f^{-1}\) poprzez liniowe złożenie funkcji ze zbioru \(\mathcal{H}\). Do wyliczenia współczynników do złożenia liniowego używa się algorytmu PCA \parencite{Pineau2020}.

Metoda ta wydaje się być obiecująca dla problemu znajdowania regionów podatnych na mutacje nonsensowne, ponieważ wyszukując najwolniej zmieniające się składowe zostaną efektywnie znalezione trendy w danych, których złożenie spowodowało otrzymanie właściwej sekwencji. Wolne składowe będąc trendami były by łatwo interpretowalne w sensie biologicznym, a fakt że sekwencje były by liniowym złożeniem trendów pozwalałby stwierdzić jak ważne jest dane źródło sygnału w danej sekwencji, co z kolei można by było wykorzystać do dalszych analiz.

Jednakże metoda ta ma także pewne wady, przede wszystkim jej założeniem jest że istnieje tyle samo wolnych składowych, co danych wejściowych, które nie jest spełnione przez problem hipotezy o kodonach niebezpiecznych. Dodatkowo SFA wymaga dużo pamięci RAM, ponieważ zbiór \(\mathcal{H}\) musi być odpowiednio duży, tak by była duża szansa na znalezienie odpowiedniej kombinacji funkcji nieliniowych. Zawiera on więc zazwyczaj kilkaset różnych funkcji, co przekłada się na rozmiar macierzy \(W\). Trzecim elementem, który może wpłynąć negatywnie na otrzymane wyniki są błędy numeryczne, ponieważ PCA korzysta z algorytmu do rozkładu macierzy na wartości osobliwe, który jest na takowe błędy podatny.

Przeprowadzono w związku z tym wstępne analizy\footnote{\href{https://gitlab.com/lekcyjna/praca-inzynierska/-/blob/main/Notatniki/SFA/PierwszyTest.ipynb}{\texttt{https://gitlab.com/lekcyjna/praca-inzynierska/-/blob/main/Notatniki/SFA}}} z użyciem SFA w celu określenia, czy pomimo powyższych problemów możliwym jest zastosowanie SFA do wyszukiwania regionów podatnych na mutacje nonsensowe. W tym celu użyto biblioteki MDP w wersji 3.6 \parencite{Zito2009}, która implementuje analizy wolnych składowych. W trakcie wykonywanych testów wykorzystano fakt, że na ostatnim etapie SFA wykonywane jest PCA i przeprowadzono z jego pomocą redukcję wymiarowości, by otrzymać spodziewaną liczbę wolnych składowych rzędu kilku, kilkunastu, reprezentujących trendy w danych.

Niestety wyników zazwyczaj nie udawało się otrzymać, ponieważ dane pośrednie nie mieściły się w pamięci albo występowały na tyle znaczące błędy numeryczne, że niemożliwym było wyznaczenie wartości własnych z użyciem metody SVD (np. otrzymywano ujemne wartości własne). Natomiast w tych niewielu testach, w których udało się otrzymać jakieś rezultaty, nie było żadnej wiedzy, ponieważ przykładowo pierwsza wolna składowa była badaną sekwencją, a pozostałe składowe były szumem. Zilustrowano to na rycinie \ref{fig:wynikiSFA}.

\begin{figure}
\includegraphics[width=\linewidth]{wynikiSFA.png}
\caption{Rozkład na wolne składowe przykładowych czterech sekwencji E. Coli. Oryginalne sekwencje zobrazowano na pomarańczowo, natomiast wolne składowe są zaznaczone na niebiesko i uszeregowane od lewej do prawej wraz ze wzrastającą prędkością tych składowych.}
\label{fig:wynikiSFA}
\end{figure}






\subsection{DBA--Kmeans}
\label{roz:modeleDTW}

Najpopularniejszym z modeli grupujących dane sekwencyjnie jest aktualnie DBA--Kmeans (ang. DTW Barycenter Averaging Kmeans) \parencite{Petitjean2011-DBAKmeans}. Opiera się on na mierze DTW (ang. Dynamic Time Warping), która jak wcześniej wspomniano, służy do wyliczania optymalnego przyrównania między dwoma szeregami (przy pomocy algorytmu dynamicznego), dla którego suma odległości euklidesowych między każdą parą sparowanych punktów jest minimalna. Taka definicja powoduje, że z użyciem DTW uwzględnia się przede wszystkim kolejność wzorców w sekwencji, a mniejszą wagę przykłada się do tego w którym momencie te wzorce się rozpoczynają.

Niestety tak duża elastyczność w poszukiwaniu wzorców może być jednocześnie obciążeniem (zwłaszcza gdy w danych jest niski stosunek sygnału do szumu) ponieważ może się okazać, że najlepiej dopasowane będą bardzo odległe od siebie fragmenty, przykładowo początek pierwszej sekwencji z końcem drugiej. Natomiast intuicyjnie w problemie wyszukiwania regionów podatnych na mutacje nonsensowne chciałoby się powiedzieć, że ,,dopasowania mogą być przesunięte ale nie za bardzo'' i być w stanie tym samym odróżnić wystąpienie pewnego wzorca na początku sekwencji od wystąpienia tego samego wzorca na jej końcu.

Dodatkowo taka elastyczność w przesuwaniu sprawia problemy przy wyliczaniu centroidów w DBA--Kmeans, ponieważ miejscowe odchylenia/zakłócenia w sekwencji oryginalnej są przyrównywane do pików w centroidzie jeszcze bardziej je amplifikując, natomiast pozostałe punkty z centroidu dążą do średniej by zminimalizować koszt przyrównania. Widać to dobrze na  ilustracji \ref{fig:problemZDTW}, gdzie środkowa część centroidu jest w zasadzie linią prostą, która się przyrównuje do jednego punktu w sekwencji, natomiast na początku i na końcu centroidu istnieje sporo pików, które mają umożliwić dobre przyrównanie się wszelkich możliwych wzorców odchyleń w sekwencji.

\begin{figure}
\includegraphics[width=\textwidth]{problemCentroiduDTW.png}
\caption{Przyrównanie przykładowej (przeskalowanej do długości 256) sekwencji (niebieski) z centroidem będącym środkiem grupy, do której została ta sekwencja zakwalifikowana przez DBA--Kmenas. Jak widać około połowa centroidu (od pozycji 100 do pozycji 225) została przyrównana do jednego punktu sekwencji znajdującego się w okolicy pozycji 95.}
\label{fig:problemZDTW}
\end{figure}

\subsection{SoftDBA--Kmeans}
Rozszerzeniem DBA--Kmeans jest SoftDBA--Kmeans, oparte na mierze SoftDTW \parencite{Cuturi2018-Softdtw}. Jest to rozwinięcie DTW, które zamiast zwykłego minimum w algorytmie dynamicznym używa funkcji gładkiego minimum (ang. smooth minimum) definiowanej jako:
\begin{gather}
Smoothmin_\gamma(x_1,x_2,...,x_n)=-\gamma \log \sum_{i=1}^n \exp  \left( \frac{-x_i}{\gamma} \right) \text{, gdzie}\quad  \gamma > 0
\label{wzr:LSE}
\end{gather}
gdzie \(x_1,x_2,...,x_n\) to liczby rzeczywiste, z których zostanie wyznaczone gładkie minimum.

Powoduje to, że Soft--DTW jest różniczkowalne i można zastosować algorytmy optymalizacji z wykorzystaniem gradientu, co jest wykorzystywane przez SoftDBA--Kmeans. Dodatkowo miara ta ogranicza swobodę przesuwania przyrównywania bez nakładania sztywnych ograniczeń, a także zapobiega powstawaniu punktowych pików, których jedynym celem będzie przyrównanie się do ewentualnych szumów/odchyleń w sekwencji. Dzieje się tak dzięki temu, że poprzez użycie Smoothmin uwzględnia się w wyniku wartości wszystkich pól \(x_1, x_2, ..., x_n\), a nie tak jak w przypadku zwykłego minimum, tylko tego najmniejszego. Dzięki temu wprowadza się karę, jeśli przesunięcie lokalnego przyrównania o \(\pm 1\) prowadziłoby do dużych zmian w wyniku, czyli pośrednio wprowadza powolność zmian znaną z SFA.

Oczywiście metoda ta nie jest bez wad, wśród przykładowych można wymienić to, że SoftDTW nie jest metryką. W szczególności tak, jak została zdefiniowana przez \cite{Cuturi2018-Softdtw} może przyjąć wartości ujemne, ponieważ zastosowana w tej pracy funkcja smooth minimum zbiega do minimum od dołu. Jednakże pomimo tych wad daje ona najlepsze wyniki z pośród przedstawionych tutaj sposobów grupowania, została więc zastosowana w dalszej części pracy.

