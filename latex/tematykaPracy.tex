\graphicspath{{Obrazy/tematykaPracy/}}
\chapter{Wstęp}


Pomimo odkrycia struktury DNA w roku 1953 i tego, że od ponad 20 lat jest możliwe sekwencjonowanie kodu genetycznego relatywnie małym kosztem, cały czas istnieje dużo niewiadomych związanych z ewolucją i optymalizacją kodu genetycznego \parencite{Blazej2018,Nowak2020}.

Takim problemem jest hipoteza o różnej używalności kodonów podatnych na mutacje nonsensowne w różnych częściach sekwencji genetycznej. Ze względu na specyfikę organizmów dzieli się ona na dwa pod problemy w zależności od rozważanych organizmów (eukariotów bądź prokariotów). O ile wśród eukariotów hipoteza ta została dokładnie zbadana \parencite{Cusack2011, Schmid2011}, to prokariotom zostało poświęcone mniej uwagi. Jednocześnie ze względu na mnogość możliwych podejść i fakt, że jest to problem biologiczny, wykorzystywano w tych analizach jedynie proste modele informatyczne i statystyczne. Ostatnio ukazał się cykl prac \parencite{Mackiewicz2021, Muniak2021, Grzybowska2021}, w których podjęto próbę zbadania tej hipotezy dla prokariotów. Jednak ze względu na prostotę przyjętego podejścia, zawierają one dużo uproszczeń, z których można zrezygnować przy zastosowaniu odpowiednich modeli informatycznych. W związku z tym w niniejszej pracy przeanalizowano różne modele informatyczne do grupowania danych i na tej podstawie wybrano algorytm SoftDBA--Kmeans, przy pomocy którego przeprowadzono analizy tej hipotezy. Pokazano w nich, że tylko część zaobserwowanych prawidłowości jest zgodna z tą hipotezą.


\section[Wybór podstawowych pojęć biologicznych]{Wybór podstawowych pojęć biologicznych używanych w pracy\protect\footnote{Dokładne objaśnienia procesów biologicznych można znaleźć w książce \citetitle{Weglenski2006} pod redakcją Piotra Węgleńskiego \parencite{Weglenski2006}.}}
\subsection{Prokarioty}



Prokarioty to grupa nieskomplikowanych organizmów żywych --- zazwyczaj jednokomórkowych, choć zdarzają się gatunki tworzące skupiska wielokomórkowe \parencite{Kaiser2003} --- w ramach aktualnej klasyfikacji jest to domena i znajduje się powyżej tradycyjnie najwyższej grupy organizmów jaką jest królestwo \parencite{Woese1990}. Organizmy będące w tej domenie cechują się przede wszystkim brakiem jądra komórkowego. Materiał genetyczny jest przechowywany w prostej postaci jaką jest długa pojedyncza nić. Dzięki temu cechują się one prostszym (w porównaniu do innych domen) mechanizmem tłumaczenia materiału genetycznego na białko\footnote{Przykładowo prokarioty nie posiadają intronów, co ułatwia składanie transkryptu.}. Najpopularniejszą grupą organizmów w ramach tej domeny są bakterie. 



\subsection{Kodowanie informacji genetycznej}

Wszystkie informacje, które komórka potrzebuje do życia, są przechowywane w materiale genetycznym. Materiałem tym jest DNA (kwas deoksyrybonukleinowy). Ma on strukturę podwójnej, skręconej w helisę, nici. Nić ta jest tworzona w uproszczeniu\footnote{Oprócz tych cząsteczek występuje jeszcze szkielet fosforanowo-deoksyrybozynowy.} przez 4 różne cząsteczki zwane nukleotydami. Cząsteczkami tymi są adenina, cytozyna, guanina i tymina, w skrócie oznaczane przez pierwsze litery tych związków, czyli ACGT. Kodują one informację przy pomocy różnych sekwencji w jakich się powtarzają. Sekwencje genetyczne są powielane w procesie nazywanym replikacją i ich kompletny zbiór w organizmie tworzy genom.

DNA odczytywane jest w postaci trójkowej, tzn. pojedynczym fragmentem niosącym pewną informację są trzy kolejne nukleotydy, zwane kodonem. Kodony są w relacji wiele -- do jednego z aminokwasami, czyli fragmentami składowymi białek (relacja ta w postaci standardowej tabeli kodu genetycznego jest zobrazowana na rys. \ref{fig:standardowaTabKoduGen}).

\begin{figure}
\includegraphics[width=\linewidth]{standardowaTabelaKoduGenetycznego.png}
\caption{Standardowa tabela kodu genetycznego. Obrazuje ona przypisanie 20 aminokwasów oraz 3 sygnałów STOP do poszczególnych kodonów. Przykładowym kodonem niebezpiecznym jest tryptofan (Trp) kodowany przez TGG, który może zmutować przy pomocy jednej substytucji zarówno w kodon stop TAG jak i TGA.}
\source{\url{https://en.wikipedia.org/wiki/DNA_and_RNA_codon_tables}}
\label{fig:standardowaTabKoduGen}
\end{figure}


\subsection{Proces tłumaczenia DNA na białka}
Proces tłumaczenia DNA na białko odbywa się w dwóch częściach. W pierwszej DNA jest transkrybowane do mRNA --- jednoniciowej cząsteczki o strukturze podobnej do DNA. Największą różnicą poza liczbą nici jest zamiana tyminy na uracyl, który podobnie jak pozostałe nukleotydy oznaczany jest skrótowo przy użyciu pierwszej litery z nazwy czyli U.

W drugim etapie RNA jest translatowane na aminokwasy, które połączone jeden za drugim tworzą peptyd, fałdujący się następnie w białko\footnote{Oczywiście w uproszczeniu, ponieważ oprócz fałdowania może występować łączenie różnych podjednostek białkowych, czy też dodawanie fragmentów nie będących peptydami, jak to się dzieje na przykład w przypadku Hemoglobiny, składającej się z jednostek \(\alpha\) i \(\beta\) (w przypadku hemoglobiny typu A) oraz z 4 dodatkowych cząsteczek hemu będących metaloporfirynami.}. Translacja ta zachodzi w rybosomach\footnote{Które także są białkami}. Rybosom przyłącza się do mRNA w miejscach inicjacji translacji i odczytuje RNA kodon po kodonie, aż wykryje kodon startu (najczęściej jest to AUG). Po wykryciu tego kodonu zaczyna tworzyć peptyd, do którego dołącza aminokwasy odpowiadające kolejnym odczytanym przez niego kodonom, bazując na tabeli kodu genetycznego. Odczyt połączony z przedłużaniem peptydu prowadzi tak długo, aż nie napotka jednego z kodonów końca replikacji (w standardowej tabeli kodu genetycznego są nimi UAG, UAA, UGA). Po odczytaniu kodonu stop rybosom uwalnia peptyd i odłącza się od mRNA.



\subsection{Mutacje}
Procesy transkrypcji oraz translacji nie są doskonałe i w ich trakcie pojawiają się spontaniczne zmiany w materiale genetycznym. Przykładowymi typami mutacji mogą być:
\begin{description}
\item[Substytucje] polegające na zamianie jednego nukleotydu na inny.
\item [Insercje/delecje] na skutek których wstawiane lub usuwane są nukleotydy, choć w minimalnym przypadku może być wstawiony lub usunięty tylko jeden nukleotyd.
\item [Rewersje] w których wycinany jest fragment genu i następnie jest on wklejany w to samo miejsce, jednakże obrócony o 180 stopni.
\item [Duplikacje] gdzie fragment sekwencji jest wielokrotnie powielany i wstawiany jeden za drugim. Jest to specyficzny typ insercji.
\end{description}


Mutacje substytucji są najczęściej występującym typem, ponieważ mogą one zajść na etapie replikacji, transkrypcji lub translacji\footnote{W przypadku translacji rybosom może odczytać inny nukleotyd niż jest w rzeczywistości na mRNA, co powoduje, że produkt powstaje taki sam, jak gdyby mutacja substytucji oryginalnego nukleotydu w odczytany zaszła na etapie transkrypcji} jednakże są także zazwyczaj niegroźnie, ponieważ najczęściej powodują one substytucję jednego aminokwasu w peptydzie, a w niektórych przypadkach mogą w ogóle nie powodować żadnych zmian w aminokwasach tworzących peptyd ze względu na zdegenerowanie\footnote{Nadmiarowości liczby kodonów nad liczbą aminokwasów.} kodu genetycznego\footnote{Ale może już wpływać na proces fałdowania się białka ze względu na różną prędkość translacji dla poszczególnych kodonów}. Czasami jednak może się zdarzyć tak, że zamiast kodonu kodującego aminokwas zostanie odczytany kodon stop (mutacja typu nonsens)\footnote{Kodony podatne na przejście na skutek substytucji w kodony STOP, nazywane są kodonami podatnymi na mutacje nonsensowne lub kodonami niebezpiecznymi.}. Taki typ mutacji substytucji jest bardzo szkodliwy, ponieważ translacja jest przerywana i powstaje zbyt krótki peptyd, który może nie uformować się w poprawne białko, a nawet może stać się dla komórki toksyczny i łączyć się z poprawnymi białkami upośledzając ich funkcję.

Podczas rozważania mutacji substytucji bierze się za zwyczaj poprawkę na fakt, że nukleotydy dzieli się na dwie podgrupy w zależności od tego do jakiej rodziny związków chemicznych należą. Istnieją więc dwie puryny (adenina i guanina) oraz dwie pirymidyny (cytozyna i tymina). Podział ten jest ważny w kontekście mutacji, ponieważ puryny są znacząco większe niż pirymidyny (wzory obrazujące rozmiar tych związków przedstawiono na rys. \ref{fig:zasady}). W związku z powstającym w ten sposób niedopasowaniem przestrzennym prawdopodobieństwo zmutowania nukleotydów z jednej grupy w drugą jest znacząco mniejsze od prawdopodobieństwa zmutowania w ramach tej samej grupy. Substytucje w ramach jednej rodziny związków chemicznych określa się mianem tranzycji (np. mutacja adeniny w guaninę), natomiast te w ramach dwóch grup mianem transwersji (np. mutacja adeniny w cytozynę). Dla uogólnienia przyjmuje się, że tranzycje są dwa razy częstsze niż transwersje \parencite{Aloqalaa2019, Mackiewicz2021}, jednak rzeczywiste wartości mogą się zmieniać w zależności od analizowanego gatunku \parencite{Blazej2017}.


\begin{figure}
\includegraphics[width=\linewidth]{Main_nucleobases.png}
\caption{Wzory szkieletowe zasad azotowych tworzących nukleotydy. Kolejno: adenina, guanina, cytozyna, tymina, uracyl. (Uracyl jest zasadą azotową, która zastępuje tyminę w RNA.)}
\source{Michał Sobkowski, domena publiczna, \url{https://commons.wikimedia.org/w/index.php?curid=54257380}}
\label{fig:zasady}
\end{figure}



Występowanie mutacji jest w pewnym stopniu korzystne, ponieważ umożliwia adaptację do nowych warunków środowiskowych. Z drugiej strony mutacje mogą doprowadzić do śmierci organizmu, a w przypadku gdy występują w całej populacji, to do jej wymarcia. W związku z tym powstały w komórkach różne mechanizmy wykrywania mutacji i ich naprawy. Przykładowo na etapie translacji wykorzystywany jest fakt, że metylacja nici występuje z kilkusekundowym opóźnieniem, by odróżnić nić oryginalną od kopii i móc naprawić błędy kopiowania \parencite{Mackiewicz2019-wyklad6-genetyka}. Oprócz tego występują mechanizmy zmniejszające szansę na pojawienie się mutacji, przykładowo odpowiednia konstrukcja tabeli kodu genetycznego może zmniejszać prawdopodobieństwo powstania mutacji niesynonimicznych \parencite{Blazej2019a, Nowak2020}.









