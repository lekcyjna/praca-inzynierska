

\section{Dotychczasowe badania}
\label{roz:dotychczasoweBadania}

Wspomniana we wstępie hipoteza mówi, że ze względu na dużą szkodliwość mutacji polegających na substytucji aminokwasu w kodon stop, sekwencje genetyczne będą ewoluowały tak, by zminimalizować prawdopodobieństwo powstania takich mutacji. Jednocześnie komórki nie mogą całkowicie zaprzestać używania kodonów o zwiększonym prawdopodobieństwie zmutowania w kodon stop (ponieważ cześć aminokwasów jest kodowana tylko przez takie kodony, np. tryptofan). W związku z tym przypuszcza się, że będzie zachodziła optymalizacja i w sekwencjach genetycznych takich kodonów będzie mniej w środkowej części genu, a więcej w początkowej i końcowej części sekwencji. Co wynika z faktu, że w przypadku pojawienia się kodonu stop na brzegach genu powstały peptyd będzie na tyle krótki, że łatwo będzie go rozłożyć, albo na tyle długi, że jest duża szansa, iż pomimo skrócenia będzie funkcjonalny. Hipoteza ta była badana w kilku pracach, lecz analizy przeprowadzone do tej pory wykorzystywały przede wszystkim różne metody statystyczne oraz korzystały z wiedzy domenowej, koncentrując się na tezach o różnym stopniu szczegółowości.

W pracy \cite{Schmid2011} przeanalizowano różne grupy organizmów eukariotycznych. Dla każdego genomu wyliczono współczynnik \(F\) opisujący czy prawdopodobieństwo zmutowania w kodon stop jest większe  (\(F>1\)), czy też mniejsze (\(F<1\)) niż oczekiwane. Współczynnik ten został zdefiniowany jako:
\begin{gather}
F=\frac{\sum_k n_k * \omega_k}
{\sum_k \frac{aa_k}{n_{syn}(k)}*\omega_k}
\end{gather}
gdzie:
\begin{description}
\item [\(n_k\)] - liczba kodonów \textit{k} w sekwencji
\item [\(\omega_k\)] - waga kodonu \textit{k} opisująca szansę zmutowania tego kodonu w stop
\item [\(aa_k\)] - liczba aminokwasów w sekwencji, takich samych jak aminokwas kodowany przez kodon \textit{k}
\item [\(n_{syn}(k)\)] - liczba kodonów kodujących ten sam aminokwas co kodon \textit{k}
\end{description}
W rezultacie pokazano, że w bardziej zaawansowanych organizmach (np. kręgowce) jest mniejsze prawdopodobieństwo uzyskania mutacji nonsensownej, a w mniej zaawansowanych (np. protisty) większe. Potwierdzono także istnienie korelacji między zawartością GC, a szansą nabycia mutacji nonsensownej. 

Bardziej szczegółowe badania przeprowadzono w pracy \cite{Cusack2011}, gdzie zbadano genomy człowieka, myszy, muszki owocowej i drożdży w celu sprawdzenia, czy istnienie mechanizmów naprawczych mutacji typu nonsens powoduje zmianę w używalności kodonów niebezpiecznych. W tym celu wyliczono współczynniki opisujące statystycznie daną sekwencję lub jej fragment, z użyciem których pokazano, że w sekwencjach posiadających tylko jeden ekson (takie sekwencje nie podlegają mechanizmom naprawczym), organizmy preferują kodony bezpieczniejsze, czyli mniej podatne na mutacje typu nonsens. Natomiast w przypadku genów wieloeksonowych zaobserwowano, iż mają zazwyczaj wysoką używalność kodonów niebezpiecznych, ponieważ podlegają one mechanizmom naprawczym. Jednakże reguła ta nie dotyczy ostatniego eksonu, który podobnie jak geny pojedynczo eksonowe tym mechanizmom nie podlega i w związku z tym kodony niebezpieczne występują w nim rzadziej.



Dla organizmów prokariotycznych powstały do tej pory mniej szczegółowe analizy. Przykładowo w pracy \cite{Barkovsky2009} zbadano korelację między frakcją kodonów łatwo mutujących w kodon stop (tzw. kodony niebezpieczne), a zawartością GC\footnote{Czyli zawartością guaniny i cytozyny.} w genomach bakteryjnych, która okazała się być bardzo znacząca (współczynnik korelacji \(-0,97\)). Wynika to z faktu, że kodony stop są kodowane głównie przez adeninę i tyminę, więc im mniej jest w genomie tych nukleotydów, tym trudniej jest znaleźć kodony, które łatwo zmutują w kodon stop.


Praca \cite{Schmid2011} została rozwinięta na organizmy prokariotyczne przez \cite{Muniak2021} oraz \cite{Grzybowska2021} gdzie sekwencje genetyczne zostały podzielone na trzy równe części, dla których także wyliczono współczynnik \(F\). Został on jednak odmiennie zdefiniowany:
\begin{gather}
F=\frac{\sum_k n_k * \omega_k}{\sum_k aa_k * \omega_k * f_k} \qquad \text{gdzie } 
f_k=\frac{n_{exp}(k)}{\sum_{i \in syn(k)} n_{exp}(i)}
\end{gather}
gdzie:
\begin{description}
\item [\(n_k\)] - liczba kodonów \textit{k} w sekwencji
\item [\(n_{exp}(k)\)] - oczekiwana liczba kodonów \textit{k} w sekwencji przy założeniu, że kolejne kodony w sekwencji są losowane z rozkłady dyskretnego i ich prawdopodobieństwo wylosowania jest identyczne na całej długości sekwencji. W takim przypadku:
\begin{gather}
n_{exp}(k) = L * P(k_1) * P(k_2) * P(k_3)
\end{gather}
gdzie \(L\) do długość sekwencji w kodonach, \(P(k_m)\) to prawdopodobieństwo wylosowania \(m\)-tego nukleotydu z kodonu \textit{k}
\item [\(\omega_k\)] - waga kodonu \textit{k} opisująca szansę zmutowania tego kodonu w stop
\item [\(aa_k\)] - liczba aminokwasów w sekwencji, takich samych jak aminokwas kodowany przez kodon \textit{k}
\item [\(syn(k)\)] - zbiór kodonów synonimicznych do \textit{k} wraz z kodonem \textit{k}
\end{description}

Podobnie jak w \cite{Schmid2011} parametr \(F\) zastosowany w pracach \cite{Muniak2021,Grzybowska2021}, opisuje czy prawdopodobieństwo zmutowania w kodon stop jest większe, czy mniejsze niż oczekiwane. Różnice między współczynnikami \(F\) dla różnych regionów zostały następnie sprawdzone pod względem istotności statystycznej i przeanalizowano czy te regiony, w których w myśl hipotezy powinno występować większe prawdopodobieństwo zmutowania w kodon stop, rzeczywiście wykazują taką właściwość.

Podejście to jest bardziej ogólne niż we wcześniejszych pracach, w szczególności:
\begin{itemize}
\item Nie zostały dobrane arbitralne wagi opisujące szansę zmutowania kodonu w kodon stop, zamiast tego wyliczono je na podstawie macierzy mutacji nukleotydów. Wprawdzie założono, że macierz ta jest taka sama dla wszystkich organizmów, jednak istnieje możliwość wykorzystania tej metody z macierzami dopasowanymi do poszczególnych genomów.
\item Założono, że kodony synonimiczne mogą występować z różnymi częstościami, co odpowiada rzeczywistości.
\item Sekwencje były analizowane z podziałem na regiony, a nie w ujęciu całościowym.
\end{itemize}

\section{Temat badawczy}

Powyższa metoda ma jednak także kilka wad, które można wyeliminować przy pomocy bardziej zaawansowanych algorytmów. Główną wadą jest to, że zakłada się iż są dokładnie trzy regiony i nie są uwzględniane inne możliwości, przykładowo takie jak ta, że część sekwencji będzie miała mniejszą lub większą liczbę regionów, co może być szczególnie istotne dla bardzo krótkich i bardzo długich sekwencji. 

Dodatkowym mankamentem jest założenie, że regiony są równej długości, będącej jedną trzecią długości całej sekwencji. W oczywisty sposób takie podejście nie uwzględnia tego, że regiony niebezpieczne na końcu i na początku sekwencji mogą mieć stałą długość liczoną w kodonach i w takim przypadku region środkowy wykazywałby dużą zmienność w swojej wielkości.

Powyższe problemy mogły wprowadzić znaczącą liczbę zniekształceń do wyników otrzymanych w pracach \cite{Muniak2021, Grzybowska2021} i utrudnić tym samym wyszukiwanie prawidłowości w danych oraz wyciąganie wniosków. W związku z tym poniżej przedstawiono wybrane algorytmy eksploracji danych i przeanalizowano możliwość ich zastosowania w celu analizy hipotezy o kodonach podatnych na mutacje typu nonsense.
